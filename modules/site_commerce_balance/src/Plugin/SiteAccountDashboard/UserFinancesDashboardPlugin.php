<?php

namespace Drupal\site_commerce_balance\Plugin\SiteAccountDashboard;

use Drupal\site_account\Annotation\DashboardPlugin;
use Drupal\site_account\DashboardPluginBase;

/**
 * Плагин отображения баланса текущего пользователя.
 * @package Drupal\site_commerce_balance\Plugin\SiteAccountDashboard\UserFinancesDashboardPlugin
 *
 * @DashboardPlugin(
 *   id = "site_commerce_balance_user_finances_dashboard",
 *   label = @Translation("My finances"),
 *   view_label = TRUE
 * )
 */
class UserFinancesDashboardPlugin extends DashboardPluginBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return [
      '#theme' => 'site_commerce_balance_user_finances_dashboard',
      '#attached' => [
        'library' => [
          'site_commerce_balance/user_finances_dashboard',
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
