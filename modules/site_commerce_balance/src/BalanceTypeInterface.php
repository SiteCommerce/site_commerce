<?php

namespace Drupal\site_commerce_balance;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Balance type entities.
 */
interface BalanceTypeInterface extends ConfigEntityInterface {

}
