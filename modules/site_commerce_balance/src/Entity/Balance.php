<?php

namespace Drupal\site_commerce_balance\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;
use Drupal\site_commerce_balance\BalanceInterface;

/**
 * Defines the Balance entity.
 *
 * @ingroup site_commerce_balance
 *
 * @ContentEntityType(
 *   id = "site_commerce_balance",
 *   label = @Translation("User balance"),
 *   bundle_label = @Translation("Type the user's balance"),
 *   bundle_entity_type = "site_commerce_balance_type",
 *   handlers = {
 *     "views_data" = "Drupal\site_commerce_balance\BalanceViewsData",
 *     "access" = "Drupal\site_commerce_balance\BalanceAccessControlHandler",
 *   },
 *   base_table = "site_commerce_balance",
 *   admin_permission = "administer site_commerce_balance",
 *   permission_granularity = "bundle",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "balance_id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   }
 * )
 */
class Balance extends ContentEntityBase implements BalanceInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getBalanceNumber() {
    return $this->get('balance')->number;
  }

  /**
   * {@inheritdoc}
   */
  public function setBalanceNumber($number) {
    $this->balance->number = $number;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBalanceСurrencyСode() {
    return $this->get('balance')->currency_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('User'))
      ->setSettings([
        'target_type' => 'user',
        'default_value' => 0,
      ])
      ->setDescription(new TranslatableMarkup('User, owner of the balance account.'));

    $fields['balance'] = BaseFieldDefinition::create('site_commerce_price')
      ->setLabel(new TranslatableMarkup('Balance'))
      ->setRequired(FALSE)
      ->setDescription(new TranslatableMarkup('The current balance of user.'));

    $fields['status'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Status'))
      ->setDefaultValue(1)
      ->setDescription(new TranslatableMarkup('The current status of the balance of the account.'));

    $fields['comment'] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Comment'))
      ->setRequired(FALSE)
      ->setDescription(new TranslatableMarkup('The comment from administrator about current status of balance.'));

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(new TranslatableMarkup('Data'))
      ->setRequired(FALSE)
      ->setDescription(new TranslatableMarkup('A serialized array of additional data.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time when the balance was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time when the balance was last edited.'));

    return $fields;
  }

}
