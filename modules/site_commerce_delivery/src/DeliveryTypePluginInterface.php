<?php

namespace Drupal\site_commerce_delivery;

interface DeliveryTypePluginInterface {

  /**
   * Метод, который возвращает объект с настройками плагина.
   */
  public function getSettings();

  /**
   * Метод, который возвращает машинный код способа доставки.
   */
  public function getCode();

  /**
   * Метод, который возвращает описание способа доставки.
   */
  public function getDescription();

  /**
   * Метод, который возвращает TRUE - если способ доставки доступен для выбора пользователю.
   */
  public function getAvailableStatus();

}
