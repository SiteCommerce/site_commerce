<?php

namespace Drupal\site_commerce_delivery\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Плагин DeliveryType, который определяет тип доставки товаров.
 *
 * @Annotation
 */
class DeliveryType extends Plugin {

  /**
   * ID плагина.
   */
  public $id;

  /**
   * Название плагина.
   */
  public $label;

  /**
   * Код способа доставки - машинное имя.
   */
  public $code;

}
