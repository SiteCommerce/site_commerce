<?php

namespace Drupal\site_commerce_delivery;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Менеджер плагина DeliveryPluginManager.
 */
class DeliveryTypePluginManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/SiteCommerce/DeliveryType', $namespaces, $module_handler, 'Drupal\site_commerce_delivery\DeliveryTypePluginInterface', 'Drupal\site_commerce_delivery\Annotation\DeliveryType');
    $this->alterInfo('site_commerce_delivery_type_info');
    $this->setCacheBackend($cache_backend, 'site_commerce_delivery_type');
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'site_commerce_delivery_type_pickup';
  }

}
