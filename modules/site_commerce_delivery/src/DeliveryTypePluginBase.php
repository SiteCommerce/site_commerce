<?php

namespace Drupal\site_commerce_delivery;

use Drupal\Component\Plugin\PluginBase;

abstract class DeliveryTypePluginBase extends PluginBase implements DeliveryTypePluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCode() {
    return $this->pluginDefinition['code'];
  }

}
