<?php

namespace Drupal\site_commerce_import;

/**
 * Class CreateQueue.
 */
class CreateQueue {

  /**
   * Instance of ImportProducts plugin.
   */
  private $importPlugin = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $config = \Drupal::config('site_commerce_import.settings');
    $plugin_id = $config->get('import_plugin');

    if ($plugin_id) {
      $this->importPlugin = \Drupal::service('plugin.manager.site_commerce_import.import_products')->createInstance($plugin_id);
    }
  }

  /**
   * Импорт каталога товаров.
   */
  public function createQueueCatalog() {
    if ($this->importPlugin) {
      $this->importPlugin->createQueueCatalog();
    }
  }

  /**
   * Импорт товаров.
   */
  public function createQueueProducts() {
    if ($this->importPlugin) {
      $this->importPlugin->createQueueProducts();
    }
  }

  /**
   * Импорт URL ссылок каталога товаров.
   */
  public function createQueueCatalogUrl() {
    if ($this->importPlugin) {
      $this->importPlugin->createQueueCatalogUrl();
    }
  }

  /**
   * Импорт URL ссылок товаров.
   */
  public function createQueueProductsUrl() {
    if ($this->importPlugin) {
      $this->importPlugin->createQueueProductsUrl();
    }
  }

}
