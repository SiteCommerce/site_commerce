<?php

namespace Drupal\site_commerce_import;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\site_commerce_product\ProductInterface;

interface ImportProductsInterface extends ContainerFactoryPluginInterface {

  /**
   * Реализует получение пути на файл Excel с категориями товаров.
   * @return string
   */
  public function getFileCatalog();

  /**
   * Реализует вычисление HASH суммы файла Excel с категориями товаров.
   * @return string
   */
  public function getFileCatalogHash();

  /**
   * Реализует получение пути на файл Excel с товарами.
   * @return string
   */
  public function getFileProducts();

  /**
   * Реализует вычисление HASH суммы файла Excel с товарами.
   * @return string
   */
  public function getFileProductsHash();

  /**
   * Реализует создание очереди импорта категорий товаров.
   */
  public function createQueueCatalog();

  /**
   * Реализует создание очереди импорта товаров.
   */
  public function createQueueProducts();

  /**
   * Реализует создание категории товара в БД.
   * @param array $data
   */
  public function createItemCatalog(array $data);

  /**
   * Реализует проверку на заполнение минимально необходимых полей при импорте категории товара.
   * @param array $data
   * @return bool
   */
  public function checkDataCatalog(array $data);

  /**
   * Реализует создание товара в БД.
   * @param array $data
   */
  public function createItemProduct(array $data);

  /**
   * Реализует проверку на заполнение минимально необходимых полей при импорте товара.
   * @param array $data
   * @return bool
   */
  public function checkDataProducts(array $data);

  /**
   * Реализует очистку спецсимволов у заголовка товара или категории перед занесением в БД.
   * @param string $title
   * @return string
   */
  public function cleanTitle(string $title);

  /**
   * Реализует вычисление HASH суммы файла Excel с URL ссылками каталога товаров.
   * @return string
   */
  public function getFileCatalogUrlHash();

  /**
   * Реализует вычисление HASH суммы файла Excel с URL ссылками товаров.
   * @return string
   */
  public function getFileProductsUrlHash();

  /**
   * Реализует получение пути на файл Excel с URL ссылками каталога товаров.
   * @return string
   */
  public function getFileCatalogUrl();

  /**
   * Реализует создание очереди импорта URL ссылок каталога товаров.
   */
  public function createQueueCatalogUrl();

  /**
   * Реализует создание URL ссылки категории каталога товаров в БД.
   * @param array $data
   * @return string
   */
  public function createItemCatalogUrl(array $data);

  /**
   * Реализует получение пути на файл Excel с URL ссылками товаров.
   * @return string
   */
  public function getFileProductsUrl();

  /**
   * Реализует создание очереди импорта URL ссылок товаров.
   */
  public function createQueueProductsUrl();

  /**
   * Реализует создание URL ссылки товара в БД.
   * @param array $data
   * @return string
   */
  public function createItemProductUrl(array $data);

  /**
   * Реализует удаление файлов с категориями товаров и перечнем товаров после импорта.
   *
   * @param string $type
   *   Тип файла 'catalog' или 'products'.
   */
  public function deleteImportedFiles(string $type);

  /**
   * Sets media objects to entity.
   *
   * @param \Drupal\site_commerce_product\Entity\Product $entity
   * @param array $data
   *
   * @return \Drupal\site_commerce_product\Entity\Product
   */
  public function createMediaImages(ProductInterface &$entity, array $data);

  /**
   * Возвращает идентификатор категории по названию.
   *
   * @param string $name
   *   The entity name.
   * @return int|null
   *   The entity identifier, or NULL if the object does not yet have an identifier.
   */
  public function getCatalogIDByName($name);

}
