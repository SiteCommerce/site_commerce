<?php

namespace Drupal\site_commerce_import\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for ImportProducts plugin type.
 *
 * @Annotation
 */
class ImportProducts extends Plugin {

  /**
   * The plugin ID.
   */
  public $id;

  /**
   * The human-readable name of plugin type.
   */
  public $label;

}
