<?php

namespace Drupal\site_commerce_import\Plugin\QueueWorker;

use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes products import.
 *
 * @QueueWorker(
 *   id = "site_commerce_import_excel_product_url_import",
 *   title = @Translation("SiteCommerce: import links of products from Excel"),
 *   cron = {"time" = 60}
 * )
 */
class ExcelProductsUrlImport extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $importPlugin = \Drupal::service('plugin.manager.site_commerce_import.import_products')->createInstance($data['plugin_id']);
    $importPlugin->createItemProductUrl($data);
  }

}
