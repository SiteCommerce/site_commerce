<?php

namespace Drupal\site_commerce_import\Plugin\QueueWorker;

use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes catalog import.
 *
 * @QueueWorker(
 *   id = "site_commerce_import_excel_catalog_import",
 *   title = @Translation("SiteCommerce: import catalog from Excel"),
 *   cron = {"time" = 60}
 * )
 */
class ExcelCatalogImport extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $importPlugin = \Drupal::service('plugin.manager.site_commerce_import.import_products')->createInstance($data['plugin_id']);
    $importPlugin->createItemCatalog($data);
  }

}
