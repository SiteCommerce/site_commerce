<?php

namespace Drupal\site_commerce_import\Plugin\SiteCommerce\Import\Products;


use Drupal\path_alias\PathAliasInterface;
use Drupal\site_commerce_import\Annotation\ImportProducts;
use Drupal\site_commerce_import\ImportProductsBase;
use Drupal\site_commerce_product\ProductInterface;
use Drupal\taxonomy\TermInterface;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use Drupal\redirect\Entity\Redirect;
use Drupal\path_alias\Entity\PathAlias;

/**
 * Class DefaultImportProducts
 *
 * @ImportProducts(
 *   id = "site_commerce_import_default_import_products",
 *   label = @Translation("Default")
 * )
 */
class DefaultImportProducts extends ImportProductsBase {

  /**
   * {@inheritdoc}
   */
  public function createQueueCatalog() {
    $reader = new Xls();
    $spreadsheet = $reader->load($this->getFileCatalog());
    $sheet = $spreadsheet->getActiveSheet();
    $cells = $sheet->getCellCollection();
    $row_max = $cells->getHighestRow();

    // Строка с которой начнется импорт из файла.
    if (empty($this->config->get('start_catalog_row'))) {
      $row = 2;
      $this->config->set('start_catalog_row', $row)->save();
    } else {
      $row = (int) $this->config->get('start_catalog_row');
    }

    // Максимальное кол-во очередей, которые создаются при импорте из Excel.
    $row_chank_size = (int) $this->config->get('queues_limit');
    $row_chank_size_max = $row_chank_size + $row;

    $queue = \Drupal::queue('site_commerce_import_excel_catalog_import');
    $queue->createQueue();

    $data = [];
    $queue_created = TRUE;
    $count = 0;
    for ($row; $row <= $row_max; $row++) {
      if ($count > $row_chank_size_max) {
        $queue_created = FALSE;
        $this->config->set('start_catalog_row', $row)->save();
        \Drupal::logger('import_catalog')->notice('Import categories completed in row: @row.', ['@row' => $row]);

        $this->messenger()->addMessage(
          $this->t('Import categories completed in row: @row.', ['@row' => $row])
        );

        break;
      }

      // Создаем очередь задания.
      $data = [
        'plugin_id' => $this->getPluginId(),
        'name' => $sheet->getCell([1, $row])->getCalculatedValue(),
        'code' => $sheet->getCell([2, $row])->getCalculatedValue(),
        'parent' => $sheet->getCell([3, $row])->getCalculatedValue(),
        'published' => $sheet->getCell([4, $row])->getCalculatedValue(),
      ];
      $queue->createItem($data);

      $count++;
    }

    // Записываем текущее значение md5. Считаем, что файл полностью обработан.
    if ($this->config->get('start_catalog_row') && $queue_created) {
      $this->config->set('file_catalog_md5', $this->getFileCatalogHash())->save();
      $this->config->set('start_catalog_row', 0)->save();

      $this->deleteImportedFiles('catalog');
      \Drupal::logger('import_catalog')->notice('Import categories completed.');

      $this->messenger()->addMessage(
        $this->t('Import categories completed.')
      );
    } else {
      $this->messenger()->addMessage(
        $this->t('A queue has been created for importing categories: @value.', ['@value' => $queue->numberOfItems()])
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createItemCatalog(array $data) {
    // Выполняем проверку обязательных ключей.
    if (!$this->checkDataCatalog($data)) {
      $message = $this->t('The category was not imported. Required keys were not checked. @value', [
        '@value' => json_encode($data, JSON_UNESCAPED_UNICODE),
      ]);
      \Drupal::logger('site_commerce_import')->error($message);
      return FALSE;
    }

    // Получаем данные и выполняем проверку.
    $name = $this->cleanTitle($data['name']);
    $field_code = trim($data['code']);
    $field_code_parent = trim($data['parent']);
    $status = (bool) trim($data['published']);

    // Разрешаем отображение в каталоге.
    $field_view = TRUE;

    // Если указан артикул.
    if (!empty($field_code)) {
      $result = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties(['field_code' => $field_code]);
      $entity = reset($result);

      if ($entity instanceof TermInterface) {
        $entity->name->setValue($name);
        $entity->status->setValue($status);
        $entity->field_view->setValue(['value' => $field_view]);

        // Определяем термин родитель по артикулу.
        if (!empty($field_code_parent)) {
          $result = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties(['field_code' => $field_code_parent]);
          $term_parent = reset($result);
          if ($term_parent) {
            $entity->parent->setValue([$term_parent->id()]);
          }
        }

        $entity->save();
      } else {
        // Создаем новый термин.
        $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $term = [
          'name' => $name,
          'vid' => 'site_commerce_catalog',
          'langcode' => $langcode,
          'parent' => [0],
          'status' => $status,
          'field_view' => [
            'value' => $field_view,
          ],
          'field_code' => [
            'value' => $field_code,
          ],
        ];

        // Определяем термин родитель.
        // TODO: Важно в файле импорта соблюдать порядок следования категорий, дочерние должны быть ниже!!!
        if (!empty($field_code_parent)) {
          $result = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties(['field_code' => $field_code_parent]);
          $term_parent = reset($result);
          if ($term_parent) {
            $term['parent'] = [$term_parent->id()];
          }
        }

        $this->entityTypeManager->getStorage('taxonomy_term')->create($term)->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createQueueProducts() {
    $reader = new Xls();
    $spreadsheet = $reader->load($this->getFileProducts());
    $sheet = $spreadsheet->getActiveSheet();
    $cells = $sheet->getCellCollection();
    $row_max = $cells->getHighestRow();

    // Строка с которой начнется импорт из файла.
    if (empty($this->config->get('start_products_row'))) {
      $row = 2;
      $this->config->set('start_products_row', $row)->save();
    } else {
      $row = (int) $this->config->get('start_products_row');
    }

    // Максимальное кол-во очередей, которые создаются при импорте из Excel.
    $row_chank_size = (int) $this->config->get('queues_limit');
    $row_chank_size_max = $row_chank_size + $row;

    $queue = \Drupal::queue('site_commerce_import_excel_products_import');
    $queue->createQueue();

    $data = [];
    $queue_created = TRUE;
    $count = 0;
    for ($row; $row <= $row_max; $row++) {
      if ($count > $row_chank_size_max) {
        $queue_created = FALSE;
        $this->config->set('start_products_row', $row)->save();
        \Drupal::logger('import_products')->notice('Import products completed in row: @row.', ['@row' => $row]);

        $this->messenger()->addMessage(
          $this->t('Import products completed in row: @row.', ['@row' => $row])
        );

        break;
      }

      // Создаем очередь задания.
      $data = [
        'plugin_id' => $this->getPluginId(),
        'type' => 'default',
        'id_external' => $sheet->getCell([1, $row])->getCalculatedValue(),
        'code' => $sheet->getCell([2, $row])->getCalculatedValue(),
        'code_manufacturer' => $sheet->getCell([3, $row])->getCalculatedValue(),
        'category' => $sheet->getCell([4, $row])->getCalculatedValue(),
        'categories' => $sheet->getCell([5, $row])->getCalculatedValue(),
        'title' => $sheet->getCell([6, $row])->getCalculatedValue(),
        'summary' => $sheet->getCell([7, $row])->getCalculatedValue(),
        'quantity' => $sheet->getCell([8, $row])->getCalculatedValue(),
        'price' => $sheet->getCell([9, $row])->getCalculatedValue(),
        'currency_code' => $sheet->getCell([10, $row])->getCalculatedValue(),
        'quantity_unit' => $sheet->getCell([11, $row])->getCalculatedValue(),
        'status' => $sheet->getCell([12, $row])->getCalculatedValue(),
        'status_catalog' => $sheet->getCell([13, $row])->getCalculatedValue(),
        'images' => $sheet->getCell([14, $row])->getCalculatedValue(),
      ];
      $queue->createItem($data);

      $count++;
    }

    // Записываем текущее значение md5. Считаем, что файл полностью обработан.
    if ($this->config->get('start_products_row') && $queue_created) {
      $this->config->set('file_products_md5', $this->getFileProductsHash())->save();
      $this->config->set('start_products_row', 0)->save();

      $this->deleteImportedFiles('products');
      \Drupal::logger('import_products')->notice('Import products processing completed.');

      $this->messenger()->addMessage(
        $this->t('Import products processing completed.')
      );
    } else {
      // Системное уведомление о создании очереди.
      $this->messenger()->addMessage(
        $this->t('A queue has been created for importing products: @value.', ['@value' => $queue->numberOfItems()])
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createItemProduct(array $data) {
    // Создаваемая сущность и ее тип.
    // @param \Drupal\site_commerce_product\Entity\Product $entity
    $entity = NULL;
    $entity_type = 'site_commerce_product';
    $bundle = trim($data['type']);

    // Выполняем проверку обязательных ключей.
    if (!$this->checkDataProducts($data)) {
      $message = $this->t('The product was not imported. Required keys were not checked. @value', [
        '@value' => json_encode($data, JSON_UNESCAPED_UNICODE),
      ]);
      \Drupal::logger('site_commerce_import')->error($message);
      return FALSE;
    }

    // Получаем переменные.
    $field_id_external = empty($data['id_external']) ? 0 : trim($data['id_external']);
    $field_code = trim($data['code']);
    $field_code_manufacturer = trim($data['code_manufacturer']);
    $category = trim($data['category']);
    $title = $this->cleanTitle($data['title']);
    $field_summary_value = trim($data['summary']);

    // Цена товара и валюта.
    $field_price_group_number = (float) round(trim($data['price']), 2);
    $field_price_group_currency_code = trim($data['currency_code']);
    if (!preg_match('/^\d{3}$/i', $field_price_group_currency_code)) {
      $config = $this->configFactoryStorage->get('site_commerce_order.settings');
      $field_price_group_currency_code = $config->get('default_currency_code') ? $config->get('default_currency_code') : 'RUB';
    }

    // Количество на складе.
    $field_settings_quantity_stock_value = (int) trim($data['quantity']);
    $field_settings_quantity_unit = trim($data['quantity_unit']);

    // Статус публикации.
    $status = (int) trim($data['status']);
    $status_catalog = (int) trim($data['status_catalog']);

    // Параметры SEO.
    $metatags = [];
    if (!empty($data['seo_title'])) {
      $metatags['title'] = trim($data['seo_title']);
    }
    if (!empty($data['seo_description'])) {
      $metatags['description'] = trim($data['seo_description']);
    }
    if (!empty($data['seo_keywords'])) {
      $metatags['keywords'] = trim($data['seo_keywords']);
    }

    // Если удалось определить категорию создаем товар.
    $tid = $this->getCatalogIDByName($category);

    if ($tid && ($field_id_external || $field_code)) {
      if ($field_id_external) {
        $result = $this->entityTypeManager->getStorage($entity_type)->loadByProperties(['field_id_external' => $field_id_external]);
      } else {
        $result = $this->entityTypeManager->getStorage($entity_type)->loadByProperties(['field_code' => $field_code]);
      }
      $entity = reset($result);

      // Если товар не существует.
      if (!$entity instanceof ProductInterface) {
        $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $site_commerce_product = [
          'type' => $bundle,
          'title' => $title,
          'langcode' => $langcode,
          'uid' => 1,
          'category' => $tid,
          'code' =>  $field_code,
          'code_external' => $field_id_external
        ];

        $entity = $this->entityTypeManager->getStorage($entity_type)->create($site_commerce_product);
        $entity->save();
      }

      // Если товар существует.
      if ($entity instanceof ProductInterface) {
        $entity->title->setValue($title);
        $entity->status->setValue($status);
        $entity->status_catalog->setValue($status_catalog);
        $entity->category->setValue(['target_id' => $tid]);
        $entity->field_summary->setValue(['value' => $field_summary_value, 'format' => 'basic_html']);
        $entity->code_external->setValue(['value' => $field_id_external]);
        $entity->code->setValue(['value' => $field_code]);
        $entity->code_manufacturer->setValue(['value' => $field_code_manufacturer]);
        $entity->price->setValue([
          'group' => 'retail',
          'prefix' => '',
          'suffix' => '',
          'number_from' => 0,
          'number' => $field_price_group_number,
          'currency_code' => $field_price_group_currency_code,
        ]);
        $entity->settings->setValue([
          'status_new' => 0,
          'status_appearance' => 0,
          'status_stock' => 0,
          'status_type_sale' => 1,
          'quantity_stock_allow' => $field_settings_quantity_stock_value ? $field_settings_quantity_stock_value : 1,
          'quantity_stock_value' => $field_settings_quantity_stock_value ? $field_settings_quantity_stock_value : 0,
          'quantity_order_min' => 1,
          'quantity_order_max' => 0,
          'quantity_unit' => $field_settings_quantity_unit,
          'cart_form_allow' => 1,
          'cart_quantity_allow' => 1,
          'cart_after_adding_event' => 'load_goto_order_checkout_form',
          'cart_label_button_add' => '',
          'cart_label_button_click' => '',
          'cart_description' => '',
        ]);

        // Если переданы метатеги.
        if ($metatags && $entity->hasField('field_meta_tags')) {
          $entity->set('field_meta_tags', serialize($metatags));
        }

        $entity->save();

        // Привязка media.
        $this->createMediaImages($entity, $data);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createQueueCatalogUrl() {
    $reader = new Xls();
    $spreadsheet = $reader->load($this->getFileCatalogUrl());
    $sheet = $spreadsheet->getActiveSheet();
    $cells = $sheet->getCellCollection();
    $row_max = $cells->getHighestRow();

    // Строка с которой начнется импорт из файла.
    if (empty($this->config->get('start_catalog_url_row'))) {
      $row = 2;
      $this->config->set('start_catalog_url_row', $row)->save();
    } else {
      $row = (int) $this->config->get('start_catalog_url_row');
    }

    // Максимальное кол-во очередей, которые создаются при импорте из Excel.
    $row_chank_size = (int) $this->config->get('queues_limit');
    $row_chank_size_max = $row_chank_size + $row;

    $queue = \Drupal::queue('site_commerce_import_excel_catalog_url_import');
    $queue->createQueue();

    $data = [];
    $queue_created = TRUE;
    $count = 0;
    for ($row; $row <= $row_max; $row++) {
      if ($count > $row_chank_size_max) {
        $queue_created = FALSE;
        $this->config->set('start_catalog_url_row', $row)->save();

        $this->messenger()->addMessage(
          $this->t('Import URL links of categories completed in row: @row.', ['@row' => $row])
        );

        break;
      }

      // Создаем очередь задания.
      $data = [
        'plugin_id' => $this->getPluginId(),
        'code' => $sheet->getCell([1, $row])->getCalculatedValue(),
        'alias' => $sheet->getCell([2, $row])->getCalculatedValue(),
      ];
      $queue->createItem($data);

      $count++;
    }

    // Выполняем если файл полностью обработан.
    if ($this->config->get('start_catalog_url_row') && $queue_created) {
      $this->config->set('file_catalog_url_md5', $this->getFileCatalogUrlHash())->save();
      $this->config->set('start_catalog_url_row', 0)->save();

      $this->messenger()->addMessage(
        $this->t('Import URL links of categories completed.')
      );
    } else {
      $this->messenger()->addMessage(
        $this->t('A queue has been created for importing URL links of categories: @value.', ['@value' => $queue->numberOfItems()])
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createItemCatalogURL(array $data) {
    // Получаем данные и выполняем проверку.
    $field_code = trim($data['code']);
    $field_alias = trim($data['alias'], '/');

    // Если указан артикул и алиас.
    if (!empty($field_code) && !empty($field_alias)) {
      $result = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties(['field_code' => $field_code]);
      $term = reset($result);
      if ($term instanceof TermInterface) {
        $path = '/taxonomy/term/' . $term->id();

        // Создавать редиректы вместо URL.
        $allow_create_redirects = (bool) $this->config->get('allow_create_redirects');
        if ($allow_create_redirects) {
          // Проверяем наличие редиректа.
          $query = \Drupal::entityTypeManager()->getStorage('redirect')->getQuery();
          $query->condition('redirect_redirect.uri', 'internal:' . $path);
          $query->accessCheck(TRUE);
          $redirect_ids = $query->execute();
          if (!$redirect_ids) {
            Redirect::create([
              'redirect_source' => $field_alias,
              'redirect_redirect' => 'internal:' . $path,
              'language' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
              'status_code' => 301,
            ])->save();
          }
        } else {
          $alias = '/' . $field_alias;
          $pathStorage = $this->entityTypeManager->getStorage('path_alias');
          $result = $pathStorage->loadByProperties(['path' => $path]);
          $path_alias = reset($result);
          if ($path_alias instanceof PathAliasInterface) {
            $path_alias->setAlias($alias);
          } else {
            $path_alias = PathAlias::create([
              'path' => $path,
              'alias' => $alias,
            ]);
          }

          $path_alias->save();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createQueueProductsUrl() {
    $reader = new Xls();
    $spreadsheet = $reader->load($this->getFileProductsUrl());
    $sheet = $spreadsheet->getActiveSheet();
    $cells = $sheet->getCellCollection();
    $row_max = $cells->getHighestRow();

    // Строка с которой начнется импорт из файла.
    if (empty($this->config->get('start_product_url_row'))) {
      $row = 2;
      $this->config->set('start_product_url_row', $row)->save();
    } else {
      $row = (int) $this->config->get('start_product_url_row');
    }

    // Максимальное кол-во очередей, которые создаются при импорте из Excel.
    $row_chank_size = (int) $this->config->get('queues_limit');
    $row_chank_size_max = $row_chank_size + $row;

    $queue = \Drupal::queue('site_commerce_import_excel_product_url_import');
    $queue->createQueue();

    $data = [];
    $queue_created = TRUE;
    $count = 0;
    for ($row; $row <= $row_max; $row++) {
      if ($count > $row_chank_size_max) {
        $queue_created = FALSE;
        $this->config->set('start_product_url_row', $row)->save();

        $this->messenger()->addMessage(
          $this->t('Import URL links of products completed in row: @row.', ['@row' => $row])
        );

        break;
      }

      // Создаем очередь задания.
      $data = [
        'plugin_id' => $this->getPluginId(),
        'code' => $sheet->getCell([1, $row])->getCalculatedValue(),
        'alias' => $sheet->getCell([2, $row])->getCalculatedValue(),
      ];
      $queue->createItem($data);

      $count++;
    }

    // Выполняем если файл полностью обработан.
    if ($this->config->get('start_product_url_row') && $queue_created) {
      $this->config->set('file_products_url_md5', $this->getFileProductsUrlHash())->save();
      $this->config->set('start_product_url_row', 0)->save();

      $this->messenger()->addMessage(
        $this->t('Import URL links of products completed.')
      );
    } else {
      $this->messenger()->addMessage(
        $this->t('A queue has been created for importing URL links of products: @value.', ['@value' => $queue->numberOfItems()])
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createItemProductUrl(array $data) {
    // Получаем данные и выполняем проверку.
    $field_code = trim($data['code']);
    $field_alias = trim($data['alias'], '/');

    // Если указан артикул и алиас.
    if (!empty($field_code) && !empty($field_alias)) {
      $result = $this->entityTypeManager->getStorage('site_commerce_product')->loadByProperties(['field_code' => $field_code]);
      $product = reset($result);
      if ($product instanceof ProductInterface) {
        $path = '/product/' . $product->id();

        // Создавать редиректы вместо URL.
        $allow_create_redirects = (bool) $this->config->get('allow_create_redirects');
        if ($allow_create_redirects) {
          // Проверяем наличие редиректа.
          $query = \Drupal::entityTypeManager()->getStorage('redirect')->getQuery();
          $query->condition('redirect_redirect.uri', 'internal:' . $path);
          $query->accessCheck(TRUE);
          $redirect_ids = $query->execute();
          if (!$redirect_ids) {
            Redirect::create([
              'redirect_source' => $field_alias,
              'redirect_redirect' => 'internal:' . $path,
              'language' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
              'status_code' => 301,
            ])->save();
          }
        } else {
          $alias = '/' . $field_alias;
          $pathStorage = $this->entityTypeManager->getStorage('path_alias');
          $result = $pathStorage->loadByProperties(['path' => $path]);
          $path_alias = reset($result);
          if ($path_alias instanceof PathAliasInterface) {
            $path_alias->setAlias($alias);
          } else {
            $path_alias = PathAlias::create([
              'path' => $path,
              'alias' => $alias,
            ]);
          }

          $path_alias->save();
        }
      }
    }
  }
}
