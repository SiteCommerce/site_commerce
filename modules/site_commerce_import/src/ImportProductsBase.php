<?php

namespace Drupal\site_commerce_import;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\site_commerce_product\ProductInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\taxonomy\TermInterface;

/**
 * ImportProductsBase class.
 */
abstract class ImportProductsBase extends PluginBase implements ImportProductsInterface {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Import settings configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->config = $container->get('config.factory')->getEditable('site_commerce_import.settings');
    $instance->logger = $container->get('logger.factory')->get('site_commerce_import');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFileCatalog() {
    $file = $this->config->get('file_catalog_path');
    return $file;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileCatalogUrl() {
    $file = $this->config->get('file_catalog_url_path');
    return $file;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileCatalogHash() {
    return md5_file($this->getFileCatalog());
  }

  /**
   * {@inheritdoc}
   */
  public function getFileCatalogUrlHash() {
    return md5_file($this->getFileCatalogUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getFileProducts() {
    $file = $this->config->get('file_products_path');
    return $file;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileProductsUrl() {
    $file = $this->config->get('file_products_url_path');
    return $file;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileProductsHash() {
    return md5_file($this->getFileProducts());
  }

  /**
   * {@inheritdoc}
   */
  public function getFileProductsUrlHash() {
    return md5_file($this->getFileProductsUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function checkDataCatalog(array $data) {
    $fields = [
      'plugin_id',
      'name',
      'code',
    ];

    foreach ($fields as $field) {
      if (empty($data[$field])) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function checkDataProducts(array $data) {
    $fields = [
      'plugin_id',
      'code',
      'category',
      'title',
    ];

    foreach ($fields as $field) {
      if (empty($data[$field])) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function cleanTitle(string $title) {
    $title = htmlspecialchars_decode($title, ENT_QUOTES);
    $title = strip_tags($title);
    $title = preg_replace('/(?:"([^>]*)")(?!>)/', '«$1»', $title);
    return trim($title);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteImportedFiles(string $type) {
    $allow_delete = (bool) $this->config->get('allow_delete_excel_files');
    if ($allow_delete) {
      if ($type == 'catalog') {
        $path = $this->getFileCatalog();
      }

      if ($type == 'products') {
        $path = $this->getFileProducts();
      }

      \Drupal::service('file_system')->delete($path);
    }
  }

  /**
   * Sets media objects to entity.
   *
   * @param ProductInterface $entity
   * @param array $data
   * @return ProductInterface
   */
  public function createMediaImages(ProductInterface &$entity, array $data) {
    // Существующие изображения товара.
    $field_media_image = $entity->get('field_media_image')->getValue();
    $field_media_image_default = $field_media_image;

    // Если переданы изображения для импорта.
    $images = empty($data['images']) ? NULL : trim($data['images']);
    if ($images) {
      // Путь к папке с изображениями товаров для импорта.
      $file_images_path = $this->config->get('file_images_path');

      $title = $entity->label();
      $langcode = $entity->language()->getId();

      // Создаем изображения товаров в виде медиа объектов.
      $images_array = explode("|", $images);
      foreach ($images_array as $image_name) {
        $temp_image_path = "{$file_images_path}/{$image_name}";
        if (file_exists($temp_image_path)) {
          $file_contents = file_get_contents($temp_image_path);
          if ($file_contents) {
            $pathinfo = pathinfo($temp_image_path);

            // Формирует имя файла.
            $path = \Drupal::service('pathauto.alias_cleaner')->cleanstring($title);
            $path = \Drupal::transliteration()->transliterate($path, $langcode);
            $filename = $path . '.' . $pathinfo['extension'];

            // Удаление пробелов.
            $filename = str_replace(' ', '-', $filename);

            // Удаление не безопасных символов.
            $filename = preg_replace('![^0-9A-Za-z_.-]!', '', $filename);

            // Удаление символов не алфавита.
            $filename = preg_replace('/(_)_+|(\.)\.+|(-)-+/', '\\1\\2\\3', $filename);

            // Преобразование.
            $filename = mb_strtolower($filename);

            $directory = 'public://images/products/' . date('Y') . '-' . date('m');
            $destination = $directory . '/' . $filename;

            // Проверка длины пути файла не более 255 символов.
            if (mb_strlen($destination) > 255) {
              $pathinfo = pathinfo($destination);
              $destination = mb_substr($destination, 0, 254 - mb_strlen($pathinfo['extension'])) . ".{$pathinfo['extension']}";
            }

            \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
            $file = \Drupal::service('file.repository')->writeData($file_contents, $destination, FileSystemInterface::EXISTS_REPLACE);
            if ($file instanceof File) {
              // Выполняем проверку на поиск дубликата файла.
              // Проверяем есть ли дубликаты медиаобьекта, чтобы не создавать его повторно
              // с одним и тем же файлом.
              $file = \Drupal::service('kvantstudio.validator')->fileHasDuplicate($file);
              $media = NULL;
              if ($file->has_dublicate) {
                $result = $this->entityTypeManager->getStorage('media')->loadByProperties(['field_media_image.target_id' => $file->id()]);
                $media = reset($result);
              }

              // Создаем медиа объект и прикрепляем к нему файл.
              if (!$media) {
                $media = Media::create([
                  'bundle' => 'product_image',
                  'uid' => '1',
                  'langcode' => $langcode,
                  'status' => 1,
                  'field_media_image' => [
                    'target_id' => $file->id(),
                    'alt' => $title,
                    'title' => $title,
                  ],
                ]);
                $media->setName($title);
                $media->save();
              }

              // Добавляем медиа-объект в массив
              // для привязки к объекту товара к полю field_media_image.
              // Исключаем повторное добавление медиа-объектов.
              if ($media instanceof Media) {
                $media_exist = FALSE;
                $referenced_entities = $entity->referencedEntities();
                foreach ($referenced_entities as $referenced_entity) {
                  if ($referenced_entity instanceof Media && $referenced_entity->id() == $media->id()) {
                    $media_exist = TRUE;
                  }
                }

                // Если не существует связь товара с медиа-объектом.
                if (!$media_exist) {
                  $field_media_image[] = [
                    'target_id' => $media->id(),
                  ];
                }
              }
            }
          }

          // Удаляем импортированный файл из папки предварительной загрузки.
          $allow_delete_images_files = (bool) $this->config->get('allow_delete_images_files');
          if ($allow_delete_images_files) {
            \Drupal::service('file_system')->delete($temp_image_path);
          }
        }
      }

      // Удаляем привязку товара к изображению по умолчанию (заглушка нет фотографии),
      // если существуют другие изображения товара.
      if (count($field_media_image) > 1) {
        // Идентификатор медиа-объекта по умолчанию.
        $default_media_target_id = \Drupal::service('kvantstudio.helper')->getDefaultMedia($entity, 'field_media_image', TRUE);
        foreach ($field_media_image as $key => $value) {
          $target_id = (int) $value['target_id'];
          if ($target_id == $default_media_target_id) {
            unset($field_media_image[$key]);
          }
        }
      }

      // Если были изменения в массиве с изображениями, обновляем товар.
      $field_media_image_serialize = serialize($field_media_image);
      $field_media_image_default_serialize = serialize($field_media_image_default);
      if ($field_media_image_default_serialize != $field_media_image_serialize) {
        $entity->set('field_media_image', $field_media_image);
        $entity->save();
      }
    }

    // Если не удалось определить изображение в карточке товара.
    if (empty($field_media_image)) {
      $entity = \Drupal::service('kvantstudio.helper')->setDefaultMedia($entity, 'field_media_image');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCatalogIDByName($name) {
    $categories_array = explode("|", $name);
    $name = array_pop($categories_array);
    $entities = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties(['name' => $name, 'vid' => 'site_commerce_catalog']);
    $entity = $entities ? reset($entities) : NULL;
    if ($entity instanceof TermInterface) {
      return (int) $entity->id();
    }

    $message = $this->t('The @value product category was not created on the site.', ['@value' => $name]);
    $this->logger->error($message);

    return NULL;
  }
}
