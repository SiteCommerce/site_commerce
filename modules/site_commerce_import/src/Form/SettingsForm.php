<?php

namespace Drupal\site_commerce_import\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'site_commerce_import.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_commerce_import_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Реализует получения списка доступных плагинов импорта товаров и категорий.
   * @return array
   */
  protected function getPluginList() {
    $definitions = \Drupal::service('plugin.manager.site_commerce_import.import_products')->getDefinitions();
    $plugin_list = [];
    foreach ($definitions as $plugin_id => $plugin) {
      $plugin_list[$plugin_id] = $this->t($plugin['label']->render());
    }

    return $plugin_list;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config(static::SETTINGS);

    $form['paths'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Import settings from Excel'),
    ];

    $form['paths']['import_plugin'] = [
      '#title' => $this->t('Select the plugin for importing'),
      '#type' => 'select',
      '#options' => $this->getPluginList(),
      '#empty_option' => $this->t('None'),
      '#default_value' => $config->get('import_plugin'),
    ];

    // Путь до файла импорта с категориями каталога.
    $form['paths']['file_catalog_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path of catalog file'),
      '#default_value' => $config->get('file_catalog_path'),
    ];

    $queue = \Drupal::queue('site_commerce_import_catalog_import');
    if ($number_of_items = $queue->numberOfItems()) {
      $form['paths']['info_text_catalog_import'] = [
        '#type' => 'markup',
        '#markup' => $this->t('In the queue for processing: @number.', [
          '@number' => $number_of_items,
        ]),
      ];
    }

    // Путь до файла импорта с товарами.
    $form['paths']['file_products_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path of products file'),
      '#default_value' => $config->get('file_products_path'),
    ];

    $queue = \Drupal::queue('site_commerce_import_products_import');
    if ($number_of_items = $queue->numberOfItems()) {
      $form['paths']['info_text_products_import'] = [
        '#type' => 'markup',
        '#markup' => $this->t('In the queue for processing: @number.', [
          '@number' => $number_of_items,
        ]),
      ];
    }

    // Разрешить импорт категорий каталога.
    $form['paths']['allow_import_catalog'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow import of catalog categories'),
      '#default_value' => $config->get('allow_import_catalog'),
    ];

    // Разрешить импорт товаров.
    $form['paths']['allow_import_products'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow import of products'),
      '#default_value' => $config->get('allow_import_products'),
    ];

    // Разрешить удалять файлы после импорта.
    $form['paths']['allow_delete_excel_files'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow to delete files after import'),
      '#default_value' => $config->get('allow_delete_excel_files'),
    ];

    // Настройка импорта изображений товаров.
    $form['images'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Importing product images'),
    ];

    // Путь к папке с изображениями товаров.
    $form['images']['file_images_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to the folder with product images'),
      '#default_value' => $config->get('file_images_path'),
    ];

    // Разрешить удалять файлы после импорта.
    $form['images']['allow_delete_images_files'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow to delete files after import'),
      '#default_value' => $config->get('allow_delete_images_files'),
    ];

    // Настройки импорта ссылок.
    $form['url'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Link import settings'),
    ];
    // Путь до файла импорта с URL ссылками на категории каталога.
    $form['url']['file_catalog_url_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path of catalog file'),
      '#default_value' => $config->get('file_catalog_url_path'),
    ];
    // Путь до файла импорта с URL ссылками на товары.
    $form['url']['file_products_url_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path of products file'),
      '#default_value' => $config->get('file_products_url_path'),
    ];
    // Разрешить импорт ссылок.
    $form['url']['allow_import_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow import of links'),
      '#default_value' => $config->get('allow_import_links'),
    ];
    // Создавать редиректы вместо синонимов.
    $form['url']['allow_create_redirects'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create redirects instead of URLs'),
      '#default_value' => $config->get('allow_create_redirects'),
    ];

    // Максимальное кол-во очередей, которые создаются при импорте из Excel.
    $form['queues_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum number of queues that are created when importing from Excel'),
      '#default_value' => $config->get('queues_limit'),
      '#size' => 20,
      '#required' => TRUE,
      '#min' => 1,
    ];

    // Разрешить повторный импорт файлов.
    $form['allow_re_importing_files'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow re-importing files'),
      '#default_value' => $config->get('allow_re_importing_files'),
      '#description' => $this->t('This option should be disabled by default. If this option is selected, files will be imported every time this form is saved, when CRON or web service requests are made to start the import.'),
    ];

    // Создать очередь для импорта.
    $form['create_import_queue'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create an import queue now'),
      '#default_value' => 0,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Записывает значения в конфигурацию.
    $config = $this->configFactory->getEditable(static::SETTINGS);

    // Сохраняем название плагина для импорта.
    $import_plugin = trim($form_state->getValue('import_plugin'));
    $config->set('import_plugin', $import_plugin);

    // Путь до файла импорта с категориями каталога.
    $file_catalog_path = trim($form_state->getValue('file_catalog_path'));
    $config->set('file_catalog_path', $file_catalog_path);

    // Путь до файла импорта с товарами.
    $file_products_path = trim($form_state->getValue('file_products_path'));
    $config->set('file_products_path', $file_products_path);

    // Разрешить импорт категорий каталога.
    $allow_import_catalog = (int) $form_state->getValue('allow_import_catalog');
    $config->set('allow_import_catalog', $allow_import_catalog);

    // Разрешить импорт товаров.
    $allow_import_products = (int) $form_state->getValue('allow_import_products');
    $config->set('allow_import_products', $allow_import_products);

    // Разрешить повторный импорт файлов.
    $allow_re_importing_files = (int) $form_state->getValue('allow_re_importing_files');
    $config->set('allow_re_importing_files', $allow_re_importing_files);

    // Разрешить удалять файлы после импорта.
    $allow_delete_excel_files = (int) $form_state->getValue('allow_delete_excel_files');
    $config->set('allow_delete_excel_files', $allow_delete_excel_files);

    // Путь к папке с изображениями товаров.
    $file_images_path = trim($form_state->getValue('file_images_path'));
    $config->set('file_images_path', $file_images_path);

    // Разрешить удалять файлы после импорта.
    $allow_delete_images_files = (int) $form_state->getValue('allow_delete_images_files');
    $config->set('allow_delete_images_files', $allow_delete_images_files);

    // Путь до файла импорта с URL ссылками на категории каталога.
    $file_catalog_url_path = trim($form_state->getValue('file_catalog_url_path'));
    $config->set('file_catalog_url_path', $file_catalog_url_path);

    // Путь до файла импорта с URL ссылками на товары.
    $file_products_url_path = trim($form_state->getValue('file_products_url_path'));
    $config->set('file_products_url_path', $file_products_url_path);

    // Создавать редиректы вместо URL.
    $allow_create_redirects = (int) $form_state->getValue('allow_create_redirects');
    $config->set('allow_create_redirects', $allow_create_redirects);

    // Максимальное кол-во очередей, которые создаются при импорте из Excel.
    $queues_limit = (int) $form_state->getValue('queues_limit');
    $config->set('queues_limit', $queues_limit);

    // Разрешить импорт ссылок.
    $allow_import_links = (int) $form_state->getValue('allow_import_links');
    $config->set('allow_import_links', $allow_import_links);

    $config->save();

    $create_import_queue = (int) $form_state->getValue('create_import_queue');
    if ($create_import_queue) {
      \site_commerce_import_start_import();
    }
  }

}
