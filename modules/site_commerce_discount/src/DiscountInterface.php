<?php

namespace Drupal\site_commerce_discount;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Discount entities.
 *
 * @ingroup site_commerce_discount
 */
interface DiscountInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  const COMPATIBLE_ANY = 'any';
  const COMPATIBLE_NONE = 'none';

  /**
   * Gets the discount name.
   *
   * @return string
   *   Name of the discount.
   */
  public function getTitle();

  /**
   * Sets the discount name.
   *
   * @param string $title
   *   Name of the discount.
   *
   * @return \Drupal\site_commerce_discount\DiscountInterface
   *   The called Discount entity.
   */
  public function setTitle($title);

  /**
   * Gets the description of discount.
   *
   * @return string
   *   Description of the discount.
   */
  public function getDescription();

  /**
   * Gets the discount plugin id.
   *
   * @return string
   *   Discount plugin id.
   */
  public function getDiscountPluginId();

  /**
   * Gets the Discount creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Discount.
   */
  public function getCreatedTime();

  /**
   * Sets the Discount creation timestamp.
   *
   * @param int $timestamp
   *   The Discount creation timestamp.
   *
   * @return \Drupal\site_commerce_discount\DiscountInterface
   *   The called Discount entity.
   */
  public function setCreatedTime($timestamp);

}
