<?php

namespace Drupal\site_commerce_discount;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Discount entity.
 *
 * @see \Drupal\site_commerce_discount\Entity\Discount.
 */
class DiscountAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      // TODO: нужно добавить проверку прав доступа на просмотр не опубликованных сущностей.
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'site commerce view published discount entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'site commerce update discount entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'site commerce delete discount entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'site commerce add discount entities');
  }
}
