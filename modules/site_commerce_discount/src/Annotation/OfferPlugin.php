<?php

namespace Drupal\site_commerce_discount\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the discount offer plugin annotation object.
 *
 * Plugin namespace: Plugin\SiteCommerce\Discount.
 *
 * @Annotation
 */
class OfferPlugin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The offer entity type ID.
   *
   * This is the entity type ID of the entity passed to the plugin during execution.
   * For example: 'site_commerce_product'.
   *
   * @var string
   */
  public $entity_type;

}
