<?php

namespace Drupal\site_commerce_discount;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Discount entities.
 *
 * @ingroup site_commerce_discount
 */
class DiscountListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['title'] = $this->t('Title');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\site_commerce_discount\Entity\Discount $entity */
    $row['id'] = $entity->id();
    $row['title'] = Link::createFromRoute(
      $entity->label(),
      'entity.site_commerce_discount.edit_form',
      ['site_commerce_discount' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
