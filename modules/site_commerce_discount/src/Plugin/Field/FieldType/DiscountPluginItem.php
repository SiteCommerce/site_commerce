<?php

namespace Drupal\site_commerce_discount\Plugin\Field\FieldType;

use Drupal\kvantstudio\Plugin\Field\FieldType\PluginItem;

/**
 * Plugin implementation of the select discount plugin field type.
 *
 * @FieldType(
 *   id = "site_commerce_discount_plugin_item",
 *   label = @Translation("The method of calculating discount"),
 *   description = @Translation("Saves the association of the discount object with the plugin that calculates it."),
 *   category = @Translation("SiteCommerce"),
 *   deriver = "\Drupal\site_commerce_discount\Plugin\Field\FieldType\DiscountPluginItemDeriver"
 * )
 */
class DiscountPluginItem extends PluginItem {

}
