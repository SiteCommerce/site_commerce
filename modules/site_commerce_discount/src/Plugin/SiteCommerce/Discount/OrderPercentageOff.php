<?php

namespace Drupal\site_commerce_discount\Plugin\SiteCommerce\Discount;

use Drupal\site_commerce_discount\DiscountInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_commerce_discount\Annotation\OfferPlugin;

/**
 * Provides the percentage off offer for orders.
 *
 * The discount is split between order items, to simplify VAT taxes and refunds.
 *
 * @OfferPlugin(
 *   id = "site_commerce_discount_order_percentage_off",
 *   label = @Translation("Percentage off the order subtotal"),
 *   entity_type = "site_commerce_order",
 * )
 */
class OrderPercentageOff extends OfferPluginBase {

  /**
   * {@inheritdoc}
   */
  public function apply(EntityInterface $entity, DiscountInterface $discount) {

  }

}
