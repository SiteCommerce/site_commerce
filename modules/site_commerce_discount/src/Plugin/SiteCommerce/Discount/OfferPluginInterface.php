<?php

namespace Drupal\site_commerce_discount\Plugin\SiteCommerce\Discount;

use Drupal\site_commerce_discount\DiscountInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the base interface for discounts.
 */
interface OfferPluginInterface extends ConfigurableInterface, PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * Gets the offer entity type ID.
   *
   * This is the entity type ID of the entity passed to apply().
   *
   * @return string
   *   The offer's entity type ID.
   */
  public function getEntityTypeId();

  /**
   * Applies the offer to the given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\site_commerce_discount\DiscountInterface $discount
   *   THe parent discount.
   */
  public function apply(EntityInterface $entity, DiscountInterface $discount);

}
