<?php

namespace Drupal\site_commerce_discount\Plugin\SiteCommerce\Discount;

use Drupal\site_commerce_discount\DiscountInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_commerce_discount\Annotation\OfferPlugin;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the percentage off offer for orders.
 *
 * The discount is split between order items, to simplify VAT taxes and refunds.
 *
 * @OfferPlugin(
 *   id = "site_commerce_discount_product_percentage_off",
 *   label = @Translation("Percentage off the product subtotal"),
 *   entity_type = "site_commerce_product",
 * )
 */
class ProductPercentageOff extends OfferPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form += parent::buildConfigurationForm($form, $form_state);

    // Wrap the offer configuration in a fieldset by default.
    $form['#type'] = 'fieldset';
    $form['#title'] = $this->t('Offer 2');
    $form['#collapsible'] = FALSE;

    // Разрешить выбор способа оплаты.
    $form['allow_select'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow selection of payment method'),
      '#default_value' => $this->configuration['allow_select'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['allow_select'] = $values['allow_select'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function apply(EntityInterface $entity, DiscountInterface $discount) {

  }

}
