<?php

namespace Drupal\site_commerce_discount;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Discount entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class DiscountHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    if ($settings_form_route = $this->getSettingsFormRoute($entity_type)) {
      $collection->add("site_commerce_discount.settings", $settings_form_route);
    }

    return $collection;
  }

  /**
   * Gets the settings form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getSettingsFormRoute(EntityTypeInterface $entity_type) {

    $route = new Route("/admin/site-commerce/config/discounts/settings");
    $route
      ->setDefaults([
        '_form' => 'Drupal\site_commerce_discount\Form\DiscountSettingsForm',
        '_title' => 'Settings',
      ])
      ->setRequirement('_permission', 'site commerce access configuration pages')
      ->setOption('_admin_route', TRUE);

    return $route;
  }
}
