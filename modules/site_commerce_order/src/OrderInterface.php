<?php

namespace Drupal\site_commerce_order;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface for defining Order entities.
 *
 * @ingroup site_commerce_order
 */
interface OrderInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the order number.
   *
   * @return string
   *   Name of the order number.
   */
  public function getNumber();

  /**
   * Sets the order number.
   *
   * @param string $number
   *   The order number.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setNumber($number);

  /**
   * Gets the order customer.
   *
   * @return string
   *   Name of the customer.
   */
  public function getCustomerName();

  /**
   * Gets the telephone of customer.
   *
   * @return string
   *   The telephone of customer.
   */
  public function getCustomerPhone();

  /**
   * Gets the mail of customer.
   *
   * @return string
   *   The mail of customer.
   */
  public function getCustomerMail();

  /**
   * Gets the delivery address of customer.
   *
   * @return string
   *   The delivery address of customer.
   */
  public function getCustomerDeliveryAddress();

  /**
   * Gets the order price value.
   *
   * @return string
   *   Price value.
   */
  public function getPrice(bool $format = FALSE);

  /**
   * Gets the order paid currency code value.
   *
   * @return string
   *   Paid currency code value.
   */
  public function getPriceСurrencyСode();

  /**
   * Set the order price value.
   *
   * @param float $value
   *   The order price value.
   * @param string $currency_code
   *   The currency code.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setPrice(float $value, string $currency_code = NULL);

  /**
   * Gets the order paid cost value.
   *
   * @return float|string
   *   Paid cost value.
   */
  public function getPaid(bool $format = FALSE);

  /**
   * Gets the order paid currency code value.
   *
   * @return string
   *   Paid currency code value.
   */
  public function getPaidСurrencyСode();

  /**
   * Gets the order paid currency value.
   *
   * @return string
   *   Paid currency value.
   */
  public function getPaidCurrencySymbol();

  /**
   * Set the order paid cost value.
   *
   * @param float $value
   *   The order paid cost value.
   * @param string $currency_code
   *   The currency code.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setPaid(float $value, string $currency_code = NULL);

  /**
   * Gets the discount.
   *
   * @return array
   *   Cost value.
   *   Percent value.
   */
  public function getDiscount();

  /**
   * Set the discount.
   *
   * @param int $percent
   *   The discount value in percents.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setDiscountByPercent(int $percent);

  /**
   * Gets the order status.
   *
   * @return string
   *   Order status name.
   */
  public function getStatus();

  /**
   * Gets the order status code value.
   *
   * @return string
   *   Order status code value.
   */
  public function getStatusCode();

  /**
   * Sets the order status code.
   *
   * @param int $code
   *   The order order status code.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setStatusCode($code);

  /**
   * Gets the order creation timestamp.
   *
   * @return int
   *   Creation timestamp of the order.
   */
  public function getCreatedTime();

  /**
   * Sets the order creation timestamp.
   *
   * @param int $timestamp
   *   The order creation timestamp.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Sets the order creator.
   *
   * @param UserInterface $account
   *   The order creator.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setCreator(UserInterface $account);

  /**
   * Gets the order customer status.
   *
   * @return bool
   *   Customer status TRUE if is new user.
   */
  public function isNewClient();

  /**
   * Gets the delivery type plugin id.
   *
   * @return string
   *   Delivery type plugin id.
   */
  public function getDeliveryTypeId();

  /**
   * Sets the delivery type id.
   *
   * @param string $delivery_type
   *   The id of delivery plugin.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setDeliveryTypeId($delivery_type);

  /**
   * Gets the delivery type plugin object.
   *
   * @return \Drupal\site_commerce_delivery\DeliveryTypePluginInterface
   *   The delivery type plugin for order.
   */
  public function getDeliveryType();

  /**
   * Gets the payment method plugin id.
   *
   * @return string
   *   Payment method plugin id.
   */
  public function getPaymentMethodId();

  /**
   * Sets the payment method id.
   *
   * @param string $payment_method
   *   The id of payment method plugin.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setPaymentMethodId($payment_method);

  /**
   * Gets the payment method plugin object.
   *
   * @return \Drupal\site_payments\PaymentSystemPluginInterface
   *   The payment method plugin for order.
   */
  public function getPaymentMethod();

  /**
   * Gets the comment.
   *
   * @return string
   *   The order comment.
   */
  public function getComment();

  /**
   * Sets the comment.
   *
   * @param string $value
   *   The comment value.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setComment(string $value);

  /**
   * Gets the order description.
   *
   * @return string
   *   Order description.
   */
  public function getDescription();

  /**
   * Sets the description.
   *
   * @param string $value
   *   The description value.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setDescription(string $value);

  /**
   * Gets additional data of order.
   *
   * @return array
   */
  public function getData();

  /**
   * Sets additional data of order.
   *
   * @param array $data
   *   The additional data of order.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   *   The called entity.
   */
  public function setData(array $data);
}
