<?php

namespace Drupal\site_commerce_order;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\kvantstudio\Formatter;
use Drupal\kvantstudio\Validator;
use Drupal\user\UserInterface;

/**
 * Defines a storage class for order entity.
 */
class OrderStorage extends SqlContentEntityStorage implements OrderStorageInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Implements data formatting functions.
   *
   * @var \Drupal\kvantstudio\Formatter
   */
  protected $formatter;

  /**
   * Implements data validating functions.
   *
   * @var \Drupal\kvantstudio\Validator
   */
  protected $validator;

  /**
   * Constructs a CommentStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_info
   *   An array of entity info for the entity type.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend instance to use.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\kvantstudio\Formatter $formatter
   *   Implements data formatting functions.
   * @param \Drupal\kvantstudio\Validator $validator
   *   Implements data validating functions.
   */
  public function __construct(EntityTypeInterface $entity_info, Connection $database, EntityFieldManagerInterface $entity_field_manager, AccountInterface $current_user, CacheBackendInterface $cache, LanguageManagerInterface $language_manager, MemoryCacheInterface $memory_cache, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityTypeManagerInterface $entity_type_manager, Formatter $formatter, Validator $validator) {
    parent::__construct($entity_info, $database, $entity_field_manager, $cache, $language_manager, $memory_cache, $entity_type_bundle_info, $entity_type_manager);
    $this->currentUser = $current_user;
    $this->formatter = $formatter;
    $this->validator = $validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_info) {
    return new static(
      $entity_info,
      $container->get('database'),
      $container->get('entity_field.manager'),
      $container->get('current_user'),
      $container->get('cache.entity'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('kvantstudio.formatter'),
      $container->get('kvantstudio.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getUserOrdersQuantity(?string $status = NULL): int {
    $query = $this->database->select('site_commerce_order', 'n');
    $query->fields('n', ['order_id']);

    if ($this->currentUser->isAuthenticated()) {
      $query->condition('n.uid', $this->currentUser->id());
    } else {
      $query->condition('n.drupal_uuid', kvantstudio_user_hash());
    }

    if ($status) {
      $query->condition('n.status', $status);
    }

    return (int) $query->countQuery()->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function getPriceByCart(string $type = 'price', ?int $order_id = NULL, ?bool $api = NULL, ?UserInterface $account = NULL): float {
    $query = $this->database->select('site_commerce_cart', 'n');
    $query->addField('n', 'quantity', 'quantity');
    $query->addField('n', "{$type}__number", 'value');

    if ($order_id) {
      $query->condition('n.order_id', $order_id);
    } else {
      $query->isNull('n.order_id');
      if ($account) {
        $query->condition('n.uid', $account->id());
      } else {
        if ($this->currentUser->isAuthenticated()) {
          $query->condition('n.uid', $this->currentUser->id());
        } else {
          $query->condition('n.drupal_uuid', kvantstudio_user_hash());
        }
      }
    }

    if ($api) {
      $query->isNotNull('n.api');
    }

    $result = $query->execute()->fetchAll();

    $cost = 0.0;
    foreach ($result as $row) {
      $cost = $cost + ((float) $row->quantity * (float) $row->value);
    }

    return $cost;
  }

  /**
   * {@inheritdoc}
   */
  public function getDiscountPrice(float $price): float {
    // Discount to the first order if allowed in the settings.
    // TODO: implement through the discount management system.
    $discount = (int) \Drupal::config('site_commerce_order.settings')->get('discount');
    if ($discount) {
      $orders_quantity = $this->getUserOrdersQuantity();
      if (!$orders_quantity) {
        $price = $price - (($price * $discount) / 100);
      }
    }

    return $price;
  }

  /**
   * {@inheritdoc}
   */
  public function createOrder(string $type, array $data, ?UserInterface $account = NULL): OrderInterface {
    // Check if user is new client or not for current order.
    $client = $data['client'] ?? NULL;
    if (is_null($client)) {
      $client = 1;

      if ($this->currentUser->isAnonymous() && !$account) {
        $account_exist = FALSE;

        // If the phone is used as the login.
        if (!empty($data['phone'])) {
          $account_exist = user_load_by_name($data['phone']);
        }

        // If email is used as the login.
        if (!empty($data['mail']) && !$account_exist) {
          $account_exist = user_load_by_name($data['mail']);
        }

        // If the account is not loaded and mail is sent.
        if (!empty($data['mail']) && !$account_exist) {
          $account_exist = user_load_by_mail($data['mail']);
        }

        // If the account is loaded.
        if ($account_exist instanceof UserInterface) {
          $client = 0;
        }
      } else {
        $client = 0;
      }
    }
    $client = (int) $client;

    // The ID of the user who actually places the order. May be the ID of the manager, not the buyer.
    $uid = $account ? $account->id() : $this->currentUser->id();
    if (!empty($data['uid'])) {
      $uid = (int) $data['uid'];
    }

    // User ID for which the order was created. The owner of the order to deliver to.
    $owner = $account ? $account->id() : $this->currentUser->id();
    if (!empty($data['owner'])) {
      $owner = (int) $data['owner'];
    }

    // Default currency.
    $config = \Drupal::config('site_commerce_order.settings');
    $default_currency_code = empty($config->get('default_currency_code')) ? 'RUB' : $config->get('default_currency_code');

    // ID of the unauthorized user.
    $drupal_uuid = $data['drupal_uuid'] ?? kvantstudio_user_hash();
    if ($account instanceof UserInterface) {
      $drupal_uuid = $account->uuid();
    }

    // Get price and paid price for order.
    $use_api = $data['use_api'] ?? NULL;
    $price = $account ? $this->getPriceByCart('price', NULL, $use_api, $account) : $this->getPriceByCart('price');
    $paid = $account ? $this->getPriceByCart('paid', NULL, $use_api, $account) : $this->getPriceByCart('paid');

    // If allow use discounts.
    $use_discounts = (bool) ($data['use_discounts'] ?? TRUE);
    if ($use_discounts) {
      $price = $this->getDiscountPrice($price);

      // Select minimal cost for paid.
      if ($this->validator->comparingNumbers($paid, $price)) {
        $paid = $price;
      }
    }

    /** @var \Drupal\site_commerce_order\OrderInterface $entity */
    $entity = $this->create([
      'type' => $type,
      'number' => $data['number'] ?? NULL,
      'store_id' => $data['store_id'] ?? NULL,
      'uid' => $uid,
      'owner' => $owner,
      'client' => $client,
      'drupal_uuid' => $drupal_uuid,
      'google_uuid' => $data['google_uuid'] ?? NULL,
      'yandex_uuid' => $data['yandex_uuid'] ?? NULL,
      'price' => [
        'number' => $price,
        'currency_code' => $default_currency_code,
      ],
      'paid' => [
        'number' => $paid,
        'currency_code' => $default_currency_code,
      ],
      'name' => $data['name'] ?? NULL,
      'phone' => $data['phone'] ?? NULL,
      'mail' => $data['mail'] ?? NULL,
      'delivery_address' => [
        'latitude' => $data['latitude'] ?? NULL,
        'longitude' => $data['longitude'] ?? NULL,
        'country' => $data['country'] ?? NULL,
        'region' => $data['region'] ?? NULL,
        'postcode' => $data['postcode'] ?? NULL,
        'city' => $data['city'] ?? NULL,
        'street' => $data['street'] ?? NULL,
        'house_number' => $data['house_number'] ?? NULL,
        'flat_number' => $data['flat_number'] ?? NULL,
      ],
      'delivery_type' => $data['delivery_type'] ?? NULL,
      'payment_method' => $data['payment_method'] ?? NULL,
      'status' => $data['status'] ?? 'new',
      'comment' => $data['comment'] ?? NULL,
      'description' => $data['description'] ?? NULL,
      'data' => serialize([]),
      'created' => (int) ($data['created'] ?? \Drupal::time()->getRequestTime()),
      'hostname' => $data['hostname'] ?? \Drupal::request()->getClientIp(),
    ]);

    $entity->enforceIsNew(TRUE);
    $entity->save();

    // Binding items in the basket to the order.
    $query = $this->database->update('site_commerce_cart');
    $query->fields([
      'order_id' => (int) $entity->id(),
    ]);
    $query->isNull('order_id');

    if ($account) {
      $query->condition('uid', $account->id());
    } else {
      if ($this->currentUser->isAuthenticated()) {
        $query->condition('uid', $this->currentUser->id());
      } else {
        $query->condition('drupal_uuid', $drupal_uuid);
      }
    }

    if ($use_api) {
      $query->isNotNull('api');
    }

    $query->execute();

    return $entity;
  }
}
