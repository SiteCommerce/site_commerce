<?php

namespace Drupal\site_commerce_order\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\site_commerce_order\OrderInterface;

/**
 * Provides order event object.
 */
class OrderEvent extends Event {

  /**
   * Order entity.
   * @var \Drupal\site_commerce_order\OrderInterface
   */
  protected $order;

  /**
   * DummyFrontpageEvent constructor.
   *
   * @param \Drupal\site_commerce_order\OrderInterface $order
   */
  public function __construct(OrderInterface $order) {
    $this->order = $order;
  }

  /**
   * Returns \Drupal\site_commerce_order\OrderInterface.
   *
   * @return \Drupal\site_commerce_order\OrderInterface $order
   */
  public function getOrder() {
    return $this->order;
  }
}
