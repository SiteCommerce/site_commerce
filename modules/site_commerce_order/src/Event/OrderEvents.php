<?php

namespace Drupal\site_commerce_order\Event;

/**
* Contains events for ordering system.
*/
final class OrderEvents {

 /**
  * Name of the event when order created.
  */
 const CREATED = 'site_commerce_order.created';

 /**
  * Name of the event when order updated.
  */
 const UPDATED = 'site_commerce_order.updated';

 /**
  * Name of the event when order deleted.
  */
 const DELETED = 'site_commerce_order.deleted';
}
