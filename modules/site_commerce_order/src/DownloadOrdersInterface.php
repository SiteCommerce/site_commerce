<?php

namespace Drupal\site_commerce_order;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * An interface for DownloadOrders plugins.
 */
interface DownloadOrdersInterface extends PluginInspectionInterface {

  /**
   * The plugin ID.
   *
   * @return string
   */
  public function getId();

  /**
   * The human-readable name of the formatter type.
   *
   * @return \Drupal\Core\Annotation\Translation.
   */
  public function getLabel();

  /**
   * Sets data to worksheet.
   *
   * @param \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet.
   *   Active excel worksheet.
   * @param string $title.
   *   Title of worksheet.
   *
   * @return \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet.
   */
  public function setDataWorksheet(Worksheet &$sheet, string $title);

  /**
   * Gets data array of orders.
   *
   * @return array.
   */
  public function getData();

}
