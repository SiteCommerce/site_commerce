<?php

namespace Drupal\site_commerce_order\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a DownloadOrders annotation object.
 *
 * @Annotation
 */
class DownloadOrders extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the formatter type.
   *
   * @var \Drupal\Core\Annotation\Translation
   * @ingroup plugin_translatable
   */
  public $label;

}
