<?php

namespace Drupal\site_commerce_order\Plugin\SiteCommerce\DownloadOrders;

use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\site_commerce_order\DownloadOrdersBase;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class DefaultDownloadOrders
 * @package Drupal\site_commerce_order\Plugin\SiteCommerce\DownloadOrders\DefaultDownloadOrders
 *
 * @DownloadOrders(
 *   id = "site_commerce_order_default_download_orders",
 *   label = @Translation("Default")
 * )
 */
class DefaultDownloadOrders extends DownloadOrdersBase {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * {@inheritdoc}
   */
  public function setDataWorksheet(Worksheet &$sheet, string $title) {
    $data = $this->getData();

    $sheet->setTitle($title);

    $sheet->setCellValue('A1', 'Номер заказа');
    $sheet->setCellValue('B1', 'Дата создания');
    $sheet->setCellValue('C1', 'Статус заказа');
    $sheet->setCellValue('D1', 'Способ получения');
    $sheet->setCellValue('E1', 'Способ оплаты');
    $sheet->setCellValue('F1', 'Артикул');
    $sheet->setCellValue('G1', 'Каталожный номер');
    $sheet->setCellValue('H1', 'Стоимость');
    $sheet->setCellValue('I1', 'Количество');
    $sheet->setCellValue('J1', 'Название товара');
    $sheet->setCellValue('K1', 'ФИО');
    $sheet->setCellValue('L1', 'Телефон');
    $sheet->setCellValue('M1', 'E-mail');
    $sheet->setCellValue('N1', 'Адрес доставки');
    $sheet->setCellValue('O1', 'Расшифровка способа оплаты');
    $sheet->setCellValue('P1', 'Примечание');
    $sheet->setCellValue('Q1', 'Номер заказа в платежной системе');
    $sheet->setCellValue('R1', 'Статус оплаты');
    $sheet->setCellValue('S1', 'ClientID');
    $sheet->setCellValue('T1', 'Склад');

    $count = 2;
    foreach ($data as $key => $value) {
      $sheet->setCellValue('A' . $count, $value['id']);
      $sheet->setCellValue('B' . $count, $value['created']);
      $sheet->setCellValue('C' . $count, $value['status']);
      $sheet->setCellValue('D' . $count, $value['delivery']);
      $sheet->setCellValue('E' . $count, $value['payment_type']);
      $sheet->setCellValue('F' . $count, $value['art']);
      $sheet->setCellValue('G' . $count, $value['cnum']);
      $sheet->setCellValue('H' . $count, $value['price']);
      $sheet->setCellValue('I' . $count, $value['count']);
      $sheet->setCellValue('J' . $count, $value['title']);
      $sheet->setCellValue('K' . $count, $value['fio']);
      $sheet->setCellValue('L' . $count, $value['phone']);
      $sheet->setCellValue('M' . $count, $value['email']);
      $sheet->setCellValue('N' . $count, $value['address']);
      $sheet->setCellValue('O' . $count, $value['payment_description']);
      $sheet->setCellValue('P' . $count, $value['description']);
      $sheet->setCellValue('Q' . $count, $value['order_id']);
      $sheet->setCellValue('R' . $count, $value['status_payment']);

      $sheet->getStyle('S' . $count)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
      $sheet->setCellValue('S' . $count, $value['ym_uid']);

      $sheet->setCellValue('T' . $count, $value['storage']);

      $count++;
    }
  }

  /**
   * {@inheritdoc}
   */
  function getData() {
    // Время создания заказа.
    $created = \Drupal::time()->getRequestTime();
    $overdue = $created - (86400 * 7) - 1;

    // Loading an entities.
    $query = \Drupal::entityQuery('site_commerce_order');
    $query->accessCheck(TRUE);
    $query->condition('created', $overdue, '>');
    $query->condition('status', 'new');
    $query->sort('created', 'DESC');
    $order_ids = $query->execute();

    // Loading an entities.
    $entities = \Drupal::entityTypeManager()
      ->getStorage('site_commerce_order')
      ->loadMultiple($order_ids);

    $rows = [];

    /** @var \Drupal\site_commerce_order\Entity\Order $order */
    foreach ($entities as $order) {

      $delivery_type = $order->getDeliveryType();
      $payment_method = $order->getPaymentMethod();

      // Транзакция.
      $transaction = \Drupal::entityTypeManager()
        ->getStorage('site_payments_transaction')
        ->loadByProperties(['order_id' => $order->id()]);

      /** @var \Drupal\site_payments\Entity\Transaction $transaction */
      $transaction = reset($transaction);

      // Перечень товаров.
      $cart_entities = \Drupal::entityTypeManager()
        ->getStorage('site_commerce_cart')
        ->loadByProperties(['order_id' => $order->id()]);

      /** @var \Drupal\site_commerce_cart\Entity\Cart $cart */
      foreach ($cart_entities as $cart) {
        /** @var \Drupal\site_commerce_product\Entity\Product $product */
        $product = \Drupal::entityTypeManager()
          ->getStorage($cart->getCartItemEntityTypeId())
          ->load($cart->getCartItemEntityId());

        $rows[] = [
          'id' => $this->getSetting('order_number_prefix') . $order->id(),
          'created' => date('Y-m-d H:i:s', $order->getCreatedTime()),
          'status' => $order->getStatus(),
          'delivery' => $delivery_type->getLabel(),
          'payment_type' => $payment_method->getId(),
          'payment_description' => $payment_method->getLabel(),
          'art' => $product->get('field_code')->value,
          'cnum' => $product->get('field_code_manufacturer')->value,
          'price' => $cart->getPrice()['number'],
          'count' => $cart->getQuantity(),
          'title' => $cart->getTitle(),
          'fio' => $order->getCustomerName(),
          'phone' => $order->getCustomerPhone(),
          'email' => $order->getCustomerMail(),
          'address' => $order->getCustomerDeliveryAddress(),
          'description' => strip_tags($order->getComment()),
          'order_id' => $transaction ? $transaction->getPaymentId() : "",
          'status_payment' => $transaction ? $transaction->getPaymentStatus() : "",
          'ym_uid' => "",
          'storage' => $delivery_type->getId() == 'site_commerce_delivery_type_br1' ? 'БЦ Норд-Хаус Дмитровское ш., 100с2' : 'ТЦ Формула Х, 27 км МКАД',
        ];
      }
    }

    return $rows;
  }
}
