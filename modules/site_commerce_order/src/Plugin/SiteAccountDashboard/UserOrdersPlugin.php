<?php

namespace Drupal\site_commerce_order\Plugin\SiteAccountDashboard;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\site_account\Annotation\DashboardPlugin;
use Drupal\site_account\DashboardPluginBase;

/**
 * Плагин отображения заказов текущего пользователя.
 * @package Drupal\site_commerce_order\Plugin\SiteAccountDashboard\UserOrdersPlugin
 *
 * @DashboardPlugin(
 *   id = "site_commerce_order_user_orders_dashboard",
 *   label = @Translation("My orders"),
 *   view_label = TRUE
 * )
 */
class UserOrdersPlugin extends DashboardPluginBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return [
      '#theme' => 'site_commerce_order_user_orders_dashboard',
      '#attached' => [
        'library' => [
          'site_commerce_order/dashboard'
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
