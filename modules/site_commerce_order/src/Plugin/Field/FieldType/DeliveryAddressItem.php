<?php

namespace Drupal\site_commerce_order\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'delivery_address' field type.
 *
 * @FieldType(
 *   id = "site_commerce_order_delivery_address",
 *   label = @Translation("Delivery address"),
 *   description = @Translation("Stores information about the delivery address of the product or service."),
 *   category = @Translation("SiteCommerce"),
 *   default_widget = "site_commerce_order_delivery_address_default",
 *   default_formatter = "site_commerce_order_delivery_address_default"
 * )
 */
class DeliveryAddressItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['latitude'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Latitude'))
      ->setRequired(FALSE);

    $properties['longitude'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Longitude'))
      ->setRequired(FALSE);

      $properties['country'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Country'))
      ->setRequired(FALSE);

      $properties['region'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Region'))
      ->setRequired(FALSE);

      $properties['postcode'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Postcode'))
      ->setRequired(FALSE);

      $properties['city'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('City'))
      ->setRequired(FALSE);

      $properties['street'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Street'))
      ->setRequired(FALSE);

      $properties['house_number'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('House number'))
      ->setRequired(FALSE);

      $properties['flat_number'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Flat number'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'latitude' => [
          'description' => 'Широта, с которой зарегистрирован заказ.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'longitude' => [
          'description' => 'Долгота, с которой зарегистрирован заказ.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'country' => [
          'description' => 'Страна, с которой зарегистрирован заказ.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'region' => [
          'description' => 'Регион, с которой зарегистрирован заказ.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'postcode' => [
          'description' => 'Почтовый индекс для доставки.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'city' => [
          'description' => 'Город для доставки заказа.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'street' => [
          'description' => 'Улица для доставки заказа.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'house_number' => [
          'description' => 'Номер дома для доставки заказа.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'flat_number' => [
          'description' => 'Номер квартиры для доставки заказа.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('city')->getValue();
    return $value === NULL || $value === '';
  }

}
