<?php

namespace Drupal\site_commerce_order\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the delivery address default widget.
 *
 * @FieldWidget(
 *   id = "site_commerce_order_delivery_address_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_commerce_order_delivery_address"
 *   }
 * )
 */
class DeliveryAddressDefault extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['latitude'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Latitude'),
      '#default_value' => isset($items[$delta]->latitude) ? $items[$delta]->latitude : NULL,
      '#placeholder' => '',
    ];

    $element['longitude'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Longitude'),
      '#default_value' => isset($items[$delta]->longitude) ? $items[$delta]->longitude : NULL,
      '#placeholder' => '',
    ];

    $element['country'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Country'),
      '#default_value' => isset($items[$delta]->country) ? $items[$delta]->country : NULL,
      '#placeholder' => '',
    ];

    $element['region'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Region'),
      '#default_value' => isset($items[$delta]->region) ? $items[$delta]->region : NULL,
      '#placeholder' => '',
    ];

    $element['postcode'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Postcode'),
      '#default_value' => isset($items[$delta]->postcode) ? $items[$delta]->postcode : NULL,
      '#placeholder' => '',
    ];

    $element['city'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('City'),
      '#default_value' => isset($items[$delta]->city) ? $items[$delta]->city : NULL,
      '#placeholder' => '',
    ];

    $element['street'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Street'),
      '#default_value' => isset($items[$delta]->street) ? $items[$delta]->street : NULL,
      '#placeholder' => '',
    ];

    $element['house_number'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('House number'),
      '#default_value' => isset($items[$delta]->house_number) ? $items[$delta]->house_number : NULL,
      '#placeholder' => '',
    ];

    $element['flat_number'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Flat number'),
      '#default_value' => isset($items[$delta]->flat_number) ? $items[$delta]->flat_number : NULL,
      '#placeholder' => '',
    ];

    return $element;
  }
}
