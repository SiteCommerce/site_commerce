<?php

namespace Drupal\site_commerce_order\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements order filter form.
 */
class OrderFilterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_commerce_order_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();

    $form['form'] = [
      '#type' => 'fieldset',
    ];

    $form['form']['filter'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['site-commerce-order-filter-form__container'],
      ],
    ];

    $form['form']['filter']['order_id'] = [
      '#type' => 'number',
      '#size' => 'tiny',
      '#title' => $this->t('Order number'),
      '#default_value' => $request->get('order_id') ?? NULL,
      '#attributes' => [
        'class' => ['site-commerce-order-filter-form__item-input'],
      ],
    ];

    $form['form']['filter']['description'] = [
      '#type' => 'textfield',
      '#size' => 'tiny',
      '#title' => $this->t('Description'),
      '#default_value' => $request->get('description') ?? NULL,
      '#attributes' => [
        'class' => ['site-commerce-order-filter-form__item-input'],
      ],
    ];

    $form['form']['filter']['phone'] = [
      '#type' => 'number',
      '#size' => 'tiny',
      '#title' => $this->t('Customer'),
      '#default_value' => $request->get('phone') ?? NULL,
      '#attributes' => [
        'class' => ['site-commerce-order-filter-form__item-input'],
      ],
    ];

    $form['form']['filter']['name'] = [
      '#type' => 'textfield',
      '#size' => 'tiny',
      '#title' => $this->t('Recipient'),
      '#default_value' => $request->get('name') ?? NULL,
      '#attributes' => [
        'class' => ['site-commerce-order-filter-form__item-input'],
      ],
    ];

    $form['form']['filter']['paid__number'] = [
      '#type' => 'textfield',
      '#size' => 'tiny',
      '#title' => $this->t('Total paid'),
      '#default_value' => $request->get('paid__number') ?? NULL,
      '#attributes' => [
        'class' => ['site-commerce-order-filter-form__item-input'],
      ],
    ];


    $form['form']['actions']['wrapper'] = [
      '#type' => 'container',
    ];

    $form['form']['actions']['wrapper']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
      '#attributes' => [
        'class' => ['site-commerce-order-filter-form__item-submit']
      ],
    ];

    if ($request->getQueryString()) {
      $form['form']['actions']['wrapper']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#submit' => ['::resetForm'],
        '#attributes' => [
          'class' => ['site-commerce-order-filter-form__item-reset']
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // if (strlen($form_state->getValue('phone_number')) < 3) {
    //   $form_state->setErrorByName('phone_number', $this->t('The phone number is too short. Please enter a full phone number.'));
    // }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [];

    $order_id = $form_state->getValue('order_id') ?? NULL;
    if ($order_id) {
      $query['order_id'] = $order_id;
    }

    $description = $form_state->getValue('description') ?? NULL;
    if ($description) {
      $query['description'] = $description;
    }

    $phone = $form_state->getValue('phone') ?? NULL;
    if ($phone) {
      $query['phone'] = $phone;
    }

    $name = $form_state->getValue('name') ?? NULL;
    if ($name) {
      $query['name'] = $name;
    }

    $paid__number = $form_state->getValue('paid__number') ?? NULL;
    if ($paid__number) {
      $query['paid__number'] = $paid__number;
    }


    $route_name = \Drupal::routeMatch()->getRouteName();
    $form_state->setRedirect($route_name, $query);
  }

  public function resetForm(array $form, FormStateInterface &$form_state) {
    $route_name = \Drupal::routeMatch()->getRouteName();
    $form_state->setRedirect($route_name);
  }
}
