<?php

namespace Drupal\site_commerce_order\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Class OrderSettingsForm.
 */
class OrderSettingsForm extends ConfigFormBase {

  /**
   * Config name.
   *
   * @var string
   */
  const SETTINGS = 'site_commerce_order.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_commerce_order_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Реализует получения списка доступных плагинов экспорта заказов.
   * @return array
   */
  protected function getPluginList() {
    $definitions = \Drupal::service('plugin.manager.download_orders')->getDefinitions();
    $plugin_list = [];
    foreach ($definitions as $plugin_id => $plugin) {
      $plugin_list[$plugin_id] = $this->t($plugin['label']->render());
    }

    return $plugin_list;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config(static::SETTINGS);

    // Доступные валюты на сайте.
    $currencies = \Drupal::entityTypeManager()->getStorage('site_commerce_currency')->loadMultiple();
    $currency_codes = array_keys($currencies);

    // Валюта по умолчанию при добавлении в корзину и оформлении заказа.
    $form['default_currency_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Currency'),
      '#options' => array_combine($currency_codes, $currency_codes),
      '#default_value' => $config->get('default_currency_code') ? $config->get('default_currency_code') : 'RUB',
      '#description' => $this->t('The default currency when adding to the cart and placing an order.'),
    ];

    // Общие правила использования платежных систем.
    $block_rules_payment_systems = (int) $config->get('block_rules_payment_systems');
    $block = \Drupal::entityTypeManager()->getStorage('block_content')->load($block_rules_payment_systems);
    $form['block_rules_payment_systems'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Rules for using payment systems'),
      '#target_type' => 'block_content',
      '#default_value' => $block,
      '#description' => $this->t('You need to create a custom block and select it in this field. The block content is displayed above the payment method options. This is the general text for all payment methods.'),
    ];

    // Примечание после оформления заказа.
    $form['note_after_ordering'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Note after placing an order'),
      '#default_value' => $config->get('note_after_ordering'),
    ];

    // E-mail для получения информации о новых заказах.
    $form['manager_mail'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail for information about new orders'),
      '#default_value' => $config->get('manager_mail'),
    ];

    // Скидка в % при первом оформлении заказа.
    // TODO: данная функция временная до внедрения модуля управления скидками.
    $form['discount'] = [
      '#type' => 'number',
      '#title' => 'Скидка в % при первом оформлении заказа',
      '#default_value' => empty($config->get('discount')) ? 0 : $config->get('discount'),
      '#description' => 'Данная функция временная до внедрения модуля управления акциями и скидками.'
    ];

    $statuses = site_commerce_order_statuses('', FALSE);
    $form['statuses'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Available order processing statuses'),
      '#options' => $statuses,
      '#default_value' => empty($config->get('statuses')) ? [] : $config->get('statuses'),
    ];

    $fields = site_commerce_fields_checkout_form('', FALSE);
    $form['fields_checkout_form'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Available fields on the checkout form'),
      '#options' => $fields,
      '#default_value' => empty($config->get('fields_checkout_form')) ? [] : $config->get('fields_checkout_form'),
    ];

    // Настройки экспорта заказов в Excel.
    $form['export_orders_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings of export orders to Excel'),
    ];

    $form['export_orders_settings']['download_orders_plugin'] = [
      '#title' => $this->t('Select the plugin for download orders'),
      '#type' => 'select',
      '#options' => $this->getPluginList(),
      '#empty_option' => $this->t('None'),
      '#default_value' => $config->get('download_orders_plugin') ? $config->get('download_orders_plugin') : "site_commerce_order_default_download_orders",
    ];

    // Полное название файла Excel.
    $orders_filename = empty($config->get('orders_filename')) ? Crypt::randomBytesBase64(10) . '.xlsx' : $config->get('orders_filename');
    $url = URL::fromRoute('site_commerce_order.download_orders', ['filename' => $orders_filename]);
    $form['export_orders_settings']['orders_filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Excel file name'),
      '#default_value' => $orders_filename,
      '#description' => $this->t('Full name of the Excel file, which must be indicated in the download link @value.', [
        '@value' => Link::fromTextAndUrl($orders_filename, $url)->toString()
      ])
    ];

    // Префикс перед номером заказа.
    $form['export_orders_settings']['order_number_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix before order number'),
      '#default_value' => $config->get('order_number_prefix'),
      '#description' => $this->t('Used to identify orders from a specific site. For example, when orders are uploaded to 1C from different online stores.')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->configFactory->getEditable(static::SETTINGS);

    // Сохраняем валюту по умолчанию.
    $default_currency_code = trim($form_state->getValue('default_currency_code'));
    $config->set('default_currency_code', $default_currency_code);

    // Правила использования платежных систем.
    $block_rules_payment_systems = (int) $form_state->getValue('block_rules_payment_systems');
    $config->set('block_rules_payment_systems', $block_rules_payment_systems);

    // Примечание после оформления заказа.
    $note_after_ordering = trim(strip_tags($form_state->getValue('note_after_ordering')));
    $config->set('note_after_ordering', $note_after_ordering);

    // E-mail для получения информации о новых заказах.
    $manager_mail = trim(strip_tags($form_state->getValue('manager_mail')));
    $config->set('manager_mail', $manager_mail);

    // Скидка в % при первом оформлении заказа.
    $discount = (int) trim(strip_tags($form_state->getValue('discount')));
    $config->set('discount', $discount);

    // Доступные статусы оформления заказов.
    $statuses = $form_state->getValue('statuses');
    $config->set('statuses', $statuses);

    // Доступные поля на форме оформления заказа.
    $fields_checkout_form = $form_state->getValue('fields_checkout_form');
    $config->set('fields_checkout_form', $fields_checkout_form);

    // Название плагина экспорта заказов.
    $download_orders_plugin = strip_tags($form_state->getValue('download_orders_plugin'));
    $config->set('download_orders_plugin', $download_orders_plugin);

    // Название файла Excel.
    $orders_filename = trim(strip_tags($form_state->getValue('orders_filename')));
    $config->set('orders_filename', $orders_filename);

    // Префикс перед номером заказа.
    $order_number_prefix = trim(strip_tags($form_state->getValue('order_number_prefix')));
    $config->set('order_number_prefix', $order_number_prefix);

    $config->save();
  }

}
