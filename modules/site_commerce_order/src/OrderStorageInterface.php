<?php

namespace Drupal\site_commerce_order;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\user\UserInterface;
use Drupal\site_commerce_order\OrderInterface;

/**
 * Defines an interface for order entity storage class.
 */
interface OrderStorageInterface extends ContentEntityStorageInterface {

  /**
   * Загружает количество оформленных заказов для текущего пользователя.
   *
   * @return int
   */
  public function getUserOrdersQuantity(?string $status = NULL): int;

  /**
   * Loads the cost of the order according to the cost of the items in the cart witout discounts.
   *
   * @param string $type
   *   Type of cost: basic price - 'price' or paid price - 'paid'.
   * @param int|null $order_id
   *   ID of the order for get basic price.
   * @param bool|null $api
   *   TRUE if the item is added to the cart via API.
   * @param \Drupal\user\UserInterface|null $account
   *   User account for get basic price.
   *
   * @return float
   */
  public function getPriceByCart(string $type = 'price', ?int $order_id = NULL, ?bool $api = NULL, ?UserInterface $account = NULL): float;

  /**
   * Корректирует базовую цену с учетом скидок. Значение со скидкой отображается
   * на этапе оформления заказа и записывается в цену, которая оплачивается.
   *
   * @param float $price
   *   Базовая цена без скидок.
   *
   * @return float
   */
  public function getDiscountPrice(float $price): float;

  /**
   * Helper function for create order entity.
   *
   * @param string $type
   *   Type of the order. Set it to'default'. See other order type id in 'site_commerce_order_type' entities.
   * @param array $data
   *   Array with parametrs for create order.
   * @param \Drupal\user\UserInterface|null $account
   *   User account for create order and get data from cart by him.
   *
   * @return \Drupal\site_commerce_order\OrderInterface
   */
  public function createOrder(string $type, array $data, ?UserInterface $account = NULL): OrderInterface;

}
