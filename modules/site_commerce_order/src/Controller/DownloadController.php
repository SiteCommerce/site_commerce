<?php
namespace Drupal\site_commerce_order\Controller;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class for file download.
 */
class DownloadController extends ControllerBase {

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructor.
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Gets setting.
   */
  public function getSetting($setting) {
    $config = $this->configFactory->get('site_commerce_order.settings');
    return $config->get($setting);
  }

  /**
   * Download file with last orders.
   *
   * @param string $filename
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   */
  function downloadOrders(string $filename) {
    $orders_filename = $this->getSetting('orders_filename');
    if ($filename != $orders_filename) {
      throw new NotFoundHttpException();
    }

    // Generate plugin export.
    $type = \Drupal::service('plugin.manager.download_orders');
    $plugin_id = $this->getSetting('download_orders_plugin');
    /** @var \Drupal\site_commerce_order\DownloadOrdersInterface $plugin */
    $plugin = $type->createInstance($plugin_id);

    $spreadsheet = new Spreadsheet();
    $spreadsheet->setActiveSheetIndex(0);
    $sheet = $spreadsheet->getActiveSheet();

    // Write data to sheet.
    $plugin->setDataWorksheet($sheet, 'Orders');

    $spreadsheet->setActiveSheetIndex(0);
    $writer = new Xlsx($spreadsheet);

    // Saves the file to disk.
    $directory = 'export';
    $uri = \Drupal::config('system.file')->get('default_scheme') . '://' . $directory;

    \Drupal::service('file_system')->prepareDirectory($uri, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

    $directory = $directory . '/' . $filename;
    $uri = \Drupal::config('system.file')->get('default_scheme') . '://' . $directory;
    $realpath = \Drupal::service('file_system')->realpath($uri);
    $writer->save($realpath);

    return $this->getFile($uri, $filename);
  }

  /**
   * Creates a link to download the file.
   */
  public function getFile(string $uri, string $filename) {
    if (\Drupal::service('stream_wrapper_manager')->isValidScheme('public') && file_exists($uri)) {
      $response = new BinaryFileResponse($uri);
      $response->setContentDisposition(
        ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename
      );

      return $response;
    }

    throw new NotFoundHttpException();
  }

}
