<?php

namespace Drupal\site_commerce_order\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller to process routes requests.
 */
final class RoutesController extends ControllerBase {

  /**
   * Обновляет статус заказа.
   *
   * @param [int] $order_id
   * @param [string] $quantity
   * @param [string] $method
   * @return void
   */
  public function setOrderStatus(int $order_id, string $status, string $method) {
    $response = new AjaxResponse();

    // Проверка входных данных.
    // TODO: добавить проверку на соответствие статусов разрешенным значениям.
    if ($method != 'ajax') {
      throw new NotFoundHttpException();
    }

    /** @var \Drupal\site_commerce_order\OrderInterface $entity */
    $entity = $this->entityTypeManager()->getStorage('site_commerce_order')->load($order_id);
    if ($entity) {
      $entity->setStatusCode($status);
      $entity->save();
    }

    $response->addCommand(new RemoveCommand('#orders__item-' . $order_id));

    return $response;
  }
}
