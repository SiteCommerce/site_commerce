<?php

namespace Drupal\site_commerce_order\Controller;

use Drupal\site_payments\TransactionInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\site_commerce_order\OrderInterface;
use Drupal\site_commerce_order\Event\OrderEvents;
use Drupal\site_commerce_order\Event\OrderEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *  Orders view controller.
 *  @package Drupal\site_commerce_order\Controller
 */
class OrderController extends ControllerBase {

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityRepository = $container->get('entity.repository');

    return $instance;
  }

  /**
   * Checks access for view orders.
   *
   * @param string $order_uuid
   *   The UUID of order.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function viewAccess(string $order_uuid, AccountInterface $account) {
    // Проверяем наличие доступа к просмотру заказа без авторизации.
    // Без авторизации доступна ограниченная информация о заказе и возможность его удалить если он не оплачен или не принят в работу.
    if ($account->isAnonymous()) {
      $access_view_without_authorization = $account->hasPermission('view without authorization site_commerce_order entities');
      return AccessResult::allowedIf($access_view_without_authorization);
    }

    // Если пользователь авторизован.
    // Проверяем право на просмотр любых заказов.
    $access = $account->hasPermission('view site_commerce_order entities');
    if ($access) {
      return AccessResult::allowed();
    }

    // Проверяем право просмотра своих заказов.
    /** @var \Drupal\site_commerce_order\OrderInterface $order */
    $order = $this->entityRepository->loadEntityByUuid('site_commerce_order', $order_uuid);
    $account_owner = $order->getOwner();

    // Если аккаунт владельца не совпадает с текущим авторизованным пользователем
    // запрещаем просмотр заказа.
    if ($account_owner->id() != $account->id()) {
      return AccessResult::forbidden();
    }

    $access = $account->hasPermission('view own site_commerce_order entities');
    if ($access) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

  /**
   * Checks access for delete orders.
   *
   * @param string $order_uuid
   *   The UUID of order.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function deleteAccess(string $order_uuid, AccountInterface $account) {
    // Проверяем наличие доступа к просмотру заказа без авторизации.
    // Без авторизации доступна ограниченная информация о заказе и возможность его удалить если он не оплачен или не принят в работу.
    if ($account->isAnonymous()) {
      $access_view_without_authorization = $account->hasPermission('delete without authorization site_commerce_order entities');
      return AccessResult::allowedIf($access_view_without_authorization);
    }

    // Если пользователь авторизован.
    // Проверяем право на удаление любых заказов.
    $access = $account->hasPermission('delete site_commerce_order entities');
    if ($access) {
      return AccessResult::allowed();
    }

    // Проверяем право удаления своих заказов.
    /** @var \Drupal\site_commerce_order\OrderInterface $order */
    $order = $this->entityRepository->loadEntityByUuid('site_commerce_order', $order_uuid);
    $account_owner = $order->getOwner();

    // Если аккаунт владельца не совпадает с текущим авторизованным пользователем
    // запрещаем удаление заказа.
    if ($account_owner->id() != $account->id()) {
      return AccessResult::forbidden();
    }

    $access = $account->hasPermission('delete own site_commerce_order entities');
    if ($access) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

  /**
   * The callback function for order page.
   *
   * @param \Drupal\site_commerce_order\Order $site_commerce_order
   *   The current site_commerce_order.
   *
   * @return string
   *   The page title.
   */
  public function orderTitle(OrderInterface $site_commerce_order) {
    return $this->t('Order № @value', array('@value' => $site_commerce_order->getNumber()));
  }

  /**
   * The callback function for view order page.
   *
   * @param string $order_uuid
   *   The UUID of order.
   *
   * @return string
   *   The page title.
   */
  public function viewOrderTitle(string $order_uuid) {
    /** @var \Drupal\site_commerce_order\OrderInterface $site_commerce_order */
    $site_commerce_order = $this->entityRepository->loadEntityByUuid('site_commerce_order', $order_uuid);
    return $this->t('Order № @value', array('@value' => $site_commerce_order->getNumber()));
  }

  /**
   * The callback function for view order delete page.
   *
   * @param string $order_uuid
   *   The UUID of order.
   *
   * @return string
   *   The page title.
   */
  public function viewOrderDeleteTitle(string $order_uuid) {
    /** @var \Drupal\site_commerce_order\OrderInterface $site_commerce_order */
    $site_commerce_order = $this->entityRepository->loadEntityByUuid('site_commerce_order', $order_uuid);
    return $this->t('Deleting order № @value', array('@value' => $site_commerce_order->getNumber()));
  }

  /**
   * Страница оформления заказа.
   */
  public function checkout() {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    return [
      '#theme' => 'site_commerce_order_checkout',
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Страница подтверждения о том, что заказ оформлен.
   */
  public function orderConfirmed(string $order_uuid) {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    return [
      '#theme' => 'site_commerce_order_confirmed',
      '#order_uuid' => $order_uuid,
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Страница просмотра заказа.
   */
  public function view(string $order_uuid) {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    return [
      '#theme' => 'site_commerce_order_view',
      '#order_uuid' => $order_uuid,
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Удаление заказа.
   *
   * @param string $order_uuid
   * @return array
   */
  public function delete(string $order_uuid) {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    $deleted = FALSE;

    // Получаем заказ.
    $result = $this->entityTypeManager()->getStorage('site_commerce_order')->loadByProperties([
      'uuid' => $order_uuid
    ]);
    /** @var \Drupal\site_commerce_order\OrderInterface $order */
    $order = $result ? reset($result) : FALSE;

    // Статусы, при которых запрещается удалять заказы.
    // TODO: Вынести массив статусов в настройки модуля.
    $statuses = ['canceled', 'delivery', 'finished'];

    // Попытка загрузки транзакции по номеру заказа.
    if (\Drupal::entityTypeManager()->hasDefinition('site_payments_transaction')) {
      $storage_transaction = \Drupal::entityTypeManager()->getStorage('site_payments_transaction');
      $transactions = $storage_transaction->loadByProperties(['order_id' => $order->id()]);

      /** @var \Drupal\site_payments\TransactionInterface $transaction */
      $transaction = reset($transactions);
      if ($transaction instanceof TransactionInterface) {
        // Если транзакция не оплачена и заказ не в процессе доставки и завершен, разрешаем удалить заказ.
        if (!$transaction->isPaid() && !in_array($order->getStatusCode(), $statuses)) {
          $deleted = TRUE;
        }
      } else {
        // Если транзакция отсутствует и заказ не в процессе доставки и завершен, разрешаем удалить заказ.
        if (!in_array($order->getStatusCode(), $statuses)) {
          $deleted = TRUE;
        }
      }
    } else {
      // Если заказ не в процессе доставки и завершен, разрешаем удалить заказ.
      if (!in_array($order->getStatusCode(), $statuses)) {
        $deleted = TRUE;
      }
    }

    // If order was deleted.
    if ($deleted) {
      $order->setStatusCode('canceled')->save();

      // Dispatch the order delete event.
      \Drupal::service('event_dispatcher')->dispatch(new OrderEvent($order), OrderEvents::DELETED);
    }

    return [
      '#theme' => 'site_commerce_order_delete',
      '#deleted' => $deleted,
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
