<?php

namespace Drupal\site_commerce_order;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Component\Plugin\Factory\DefaultFactory;

/**
 * Provides an Archiver plugin manager.
 *
 * @see \Drupal\site_commerce_order\Annotation\DownloadOrders
 * @see \Drupal\site_commerce_order\DownloadOrdersInterface
 * @see plugin_api
 */
class DownloadOrdersManager extends DefaultPluginManager {

  /**
   * Constructs a DownloadOrdersManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SiteCommerce/DownloadOrders',
      $namespaces,
      $module_handler,
      'Drupal\site_commerce_order\DownloadOrdersInterface',
      'Drupal\site_commerce_order\Annotation\DownloadOrders'
    );
    $this->alterInfo('download_orders_info');
    $this->setCacheBackend($cache_backend, 'download_orders_info_plugins');
  }

}
