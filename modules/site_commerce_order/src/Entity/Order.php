<?php

namespace Drupal\site_commerce_order\Entity;

use Drupal\site_commerce_delivery\DeliveryTypePluginInterface;
use Drupal\site_payments\PaymentSystemPluginInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;
use Drupal\kvantstudio\Validator;
use Drupal\kvantstudio\Formatter;
use Drupal\site_commerce_order\OrderInterface;

/**
 * Defines the Order entity.
 *
 * @ingroup site_commerce_order
 *
 * @ContentEntityType(
 *   id = "site_commerce_order",
 *   label = @Translation("Order"),
 *   label_collection = @Translation("Orders", context = "SiteCommerce"),
 *   label_singular = @Translation("order", context = "SiteCommerce"),
 *   label_plural = @Translation("orders", context = "SiteCommerce"),
 *   label_count = @PluralTranslation(
 *     singular = "@count order",
 *     plural = "@count orders",
 *     context = "SiteCommerce",
 *   ),
 *   bundle_label = @Translation("Order type", context = "SiteCommerce"),
 *   handlers = {
 *     "storage" = "Drupal\site_commerce_order\OrderStorage",
 *     "view_builder" = "Drupal\site_commerce_order\OrderViewBuilder",
 *     "list_builder" = "Drupal\site_commerce_order\OrderListBuilder",
 *     "views_data" = "Drupal\site_commerce_order\OrderViewsData",
 *     "access" = "Drupal\site_commerce_order\OrderAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\site_commerce_order\Form\OrderForm",
 *       "edit" = "Drupal\site_commerce_order\Form\OrderForm",
 *       "delete" = "Drupal\site_commerce_order\Form\OrderDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_commerce_order\OrderHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "site_commerce_order",
 *   admin_permission = "administer site_commerce_order",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "order_id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "owner",
 *   },
 *   links = {
 *     "canonical" = "/admin/site-commerce-order/{site_commerce_order}",
 *     "edit-form" = "/admin/site-commerce/config/orders/{site_commerce_order}/edit",
 *     "delete-form" = "/admin/site-commerce/config/orders/{site_commerce_order}/delete",
 *     "collection" = "/admin/site-commerce/orders",
 *   },
 *   bundle_entity_type = "site_commerce_order_type",
 *   field_ui_base_route = "entity.site_commerce_order_type.edit_form"
 * )
 */
class Order extends ContentEntityBase implements OrderInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Удаляем все позиции из корзины, которые привязаны к заказу.
    if (\Drupal::moduleHandler()->moduleExists('site_commerce_cart')) {
      foreach ($entities as $entity) {
        $query = \Drupal::entityQuery('site_commerce_cart');
        $query->accessCheck(TRUE);
        $query->condition('order_id', $entity->id());
        $cart_items = $query->execute();

        $storage_handler = \Drupal::entityTypeManager()->getStorage('site_commerce_cart');
        $cart_entities = $storage_handler->loadMultiple($cart_items);
        $storage_handler->delete($cart_entities);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->get('number')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getNumber() {
    $number = $this->get('number')->value;
    if (empty($number)) {
      $number = $this->id();
    }

    return $number;
  }

  /**
   * {@inheritdoc}
   */
  public function setNumber($number) {
    $this->set('number', $number);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomerName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomerPhone() {
    return $this->get('phone')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomerMail() {
    return $this->get('mail')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomerDeliveryAddress() {
    $field_value = $this->get('delivery_address')->getValue();
    $field_value = reset($field_value);

    $data = [];
    $exclude_values = ['latitude', 'longitude'];
    foreach ($field_value as $key => $value) {
      if (!empty($value) && !\in_array($key, $exclude_values)) {
        $data[$key] = $value;
      }
    }

    if ($data) {
      return implode(', ', $data);
    }

    return "";
  }

  /**
   * {@inheritdoc}
   */
  public function isNewClient() {
    return (bool) $this->get('client')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice(bool $format = FALSE) {
    $number = (float) $this->get('price')->number;

    if ($format) {
      $number = Formatter::price($number);
    }

    return $number;
  }

  /**
   * {@inheritdoc}
   */
  public function getPriceСurrencyСode() {
    return $this->get('price')->currency_code;
  }

  /**
   * {@inheritdoc}
   */
  public function setPrice(float $value, string $currency_code = NULL) {
    $this->set('price', [
      'number' => $value,
      'currency_code' => $currency_code ?: $this->getPriceСurrencyСode()
    ]);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaid(bool $format = FALSE) {
    $number = (float) $this->get('paid')->number;

    if ($format) {
      $number = Formatter::price($number);
    }

    return $number;
  }

  /**
   * {@inheritdoc}
   */
  public function setPaid(float $value, string $currency_code = NULL) {
    $this->set('paid', [
      'number' => $value,
      'currency_code' => $currency_code ?: $this->getPriceСurrencyСode()
    ]);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaidСurrencyСode() {
    return $this->get('paid')->currency_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaidCurrencySymbol() {
    $currency_entities = \Drupal::entityTypeManager()->getStorage('site_commerce_currency')->loadByProperties([
      'letter_code' => $this->getPaidСurrencyСode()
    ]);
    /** @var \Drupal\site_commerce_price\Entity\Currency $currency */
    $currency = $currency_entities ? reset($currency_entities) : NULL;
    if ($currency) {
      return $currency->getSymbol();
    }

    return "";
  }

  /**
   * {@inheritdoc}
   */
  public function getDiscount() {
    $price = $this->get('price')->number;
    $paid = $this->get('paid')->number;

    $difference = $price - $paid;

    $percent = 0;
    if (Validator::comparingNumbers($price)) {
      $percent = ($difference * 100) / $price;
    }

    return [
      'value' => (float) $difference,
      'percent' => (float) $percent,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setDiscountByPercent(int $percent) {
    $price = $this->get('price')->number;
    $difference = ($price * $percent) / 100;
    $paid = $price - $difference;
    $this->setPaid($paid);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    $status_code = $this->getStatusCode();
    return site_commerce_order_statuses($status_code);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusCode() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatusCode($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeliveryTypeId() {
    return $this->get('delivery_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeliveryTypeId($delivery_type) {
    $this->set('delivery_type', $delivery_type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeliveryType() {
    if (\Drupal::hasService('plugin.manager.site_commerce_delivery.type')) {
      $plugin_id = $this->getDeliveryTypeId();

      /** @var \Drupal\site_commerce_delivery\DeliveryTypePluginManager $plugin_service */
      $plugin_service = \Drupal::service('plugin.manager.site_commerce_delivery.type');

      /** @var \Drupal\site_commerce_delivery\DeliveryTypePluginInterface $plugin */
      $plugin = $plugin_service->createInstance($plugin_id);
      if ($plugin instanceof DeliveryTypePluginInterface) {
        return $plugin;
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentMethodId() {
    return $this->get('payment_method')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentMethodId($payment_method) {
    $this->set('payment_method', $payment_method);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentMethod() {
    if (\Drupal::hasService('plugin.manager.site_payments.payment_system')) {
      $plugin_id = $this->getPaymentMethodId();

      /** @var \Drupal\site_commerce_delivery\DeliveryTypePluginManager $plugin_service */
      $plugin_service = \Drupal::service('plugin.manager.site_payments.payment_system');

      /** @var \Drupal\site_payments\PaymentSystemPluginInterface $plugin */
      $plugin = $plugin_service->createInstance($plugin_id);
      if ($plugin instanceof PaymentSystemPluginInterface) {
        return $plugin;
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getComment() {
    return $this->get('comment')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function setComment(string $value) {
    $this->set('comment', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $value) {
    $this->set('description', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('owner')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('owner')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('owner', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('owner', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreator(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $data = $this->get('data')->getValue();
    return reset($data);
  }

  /**
   * {@inheritdoc}
   */
  public function setData(array $data) {
    $this->set('data', $data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Order number'))
      ->setDescription(t('The order number displayed to the customer.'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['store_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Store'))
      ->setDescription(t('The store to which the order belongs.'))
      ->setRequired(FALSE)
      ->setCardinality(1);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Customer'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    $fields['owner'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setDescription(t('The user who placed the order.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['client'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Type of client'))
      ->setDescription(t('Specifies the customer type for the current order: 0 -existing, 1 -new.'))
      ->setRequired(FALSE)
      ->setSettings([
        'size' => 'tiny',
        'unsigned', TRUE,
      ]);

    $fields['drupal_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID of the unauthorized сustomer'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['google_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The customer ID in google.'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['yandex_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The customer ID in yandex.'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['price'] = BaseFieldDefinition::create('site_commerce_price')
      ->setLabel(t('Price'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['paid'] = BaseFieldDefinition::create('site_commerce_price')
      ->setLabel(t('Total price'))
      ->setDescription(t('The final price to pay for the order.'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Customer full name'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['phone'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Customer phone'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Customer email'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['delivery_address'] = BaseFieldDefinition::create('site_commerce_order_delivery_address')
      ->setLabel(t('Delivery address'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['delivery_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Delivery method'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['payment_method'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Payment method'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Order status'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['comment'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Comment on the order'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'))
      ->setRequired(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the order was created.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the order was last edited.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['placed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Placed'))
      ->setRequired(FALSE)
      ->setDescription(t('The time when the order was placed.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['completed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Completed'))
      ->setRequired(FALSE)
      ->setDescription(t('The time when the order was completed.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['hostname'] = BaseFieldDefinition::create('string')
      ->setLabel(t('IP-address'))
      ->setRequired(TRUE)
      ->setSettings([
        'type' => 'varchar_ascii',
        'max_length' => 128,
      ]);

    return $fields;
  }
}
