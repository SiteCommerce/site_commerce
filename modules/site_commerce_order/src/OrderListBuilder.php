<?php

namespace Drupal\site_commerce_order;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\site_commerce_order\OrderInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Order entities.
 *
 * @ingroup site_commerce_order
 */
class OrderListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * Builds the entity listing as renderable array for table.html.twig.
   */
  public function render() {
    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\site_commerce_order\Form\OrderFilterForm');

    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->getTitle(),
      '#rows' => [],
      '#empty' => $this->t('Orders are not created.'),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
      '#attributes' => [
        'class' => [
          'orders',
        ],
      ],
      '#attached' => [
        'library' => [
          'site_commerce_order/order_list_builder',
        ],
      ],
    ];

    foreach ($this->load() as $entity) {
      if ($entity instanceof OrderInterface) {
        if ($row = $this->buildRow($entity)) {
          $build['table'][$entity->id()] = $row;
        }
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Order number');
    $header['description'] = $this->t('Description');
    $header['created'] = $this->t('Created');
    $header['customer'] = $this->t('Customer');
    $header['recipient'] = $this->t('Recipient');
    $header['paid_number'] = $this->t('Total paid');
    $header['paid_currency'] = $this->t('Currency');
    $header['status'] = $this->t('Order status');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    if ($entity instanceof OrderInterface) {
      $row['#attributes'] = [
        'id' => 'orders__item-' . $entity->id(),
      ];

      $row['id'] = [
        '#markup' => $entity->id(),
      ];

      $row['description'] = [
        '#markup' => $entity->getDescription(),
      ];

      $timestamp = $entity->getCreatedTime();
      $created = \Drupal::service('date.formatter')->format($timestamp, 'custom', 'd.m.Y - H:i:s');
      $row['created'] = [
        '#markup' => $created,
      ];

      $owner = $entity->getOwner();
      $link = NULL;
      if ($owner) {
        $link = Link::fromTextAndUrl($owner->label(), $entity->getOwner()->toUrl());
      }
      $row['customer'] = [
        '#markup' => $link ? $link->toString() : $this->t('Unknown'),
      ];

      $row['recipient'] = [
        '#markup' => $entity->getCustomerName() ?: $this->t('Unknown'),
      ];

      $row['paid_number'] = [
        '#markup' => $entity->getPaid(),
      ];

      $row['paid_currency'] = [
        '#markup' => $entity->getPaidCurrencySymbol(),
      ];

      $row['status'] = [
        '#type' => 'select',
        '#title' => $this->t('Order status'),
        '#title_display' => 'invisible',
        '#value' => $entity->getStatusCode(),
        '#options' => site_commerce_order_statuses(),
        '#attributes' => [
          'class' => [
            'form-select__set-order-status',
            'js-set-order-status',
          ],
          'data-id' => $entity->id(),
        ],
      ];

      return $row + parent::buildRow($entity);
    }
  }

  /**
   * Gets this list's default operations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the operations are for.
   *
   * @return array
   *   The array structure is identical to the return value of
   *   self::getOperations().
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = [];

    if ($entity->access('view')) {
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 0,
        'url' => $this->ensureDestination($entity->toUrl('canonical')),
      ];
    }

    if ($entity->access('edit') && $entity->hasLinkTemplate('edit-form')) {
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'weight' => 10,
        'url' => $this->ensureDestination($entity->toUrl('edit-form')),
      ];
    }

    if ($entity->access('delete') && $entity->hasLinkTemplate('delete-form')) {
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'weight' => 20,
        'url' => $this->ensureDestination($entity->toUrl('delete-form')),
      ];
    }

    return $operations;
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $request = \Drupal::request();
    $query = $this->getStorage()->getQuery();

    $status = \Drupal::routeMatch()->getParameters()->get('status');
    if ($status) {
      $query->condition('status', $status);
    }

    $order_id = $request->get('order_id');
    if ($order_id) {
      $query->condition('order_id', $order_id);
    };

    $description = $request->get('description');
    if ($description) {
      $query->condition('description', $description, 'CONTAINS');
    }

    $phone = $request->get('phone');
    if ($phone) {
      $query->condition('phone', $phone);
    }

    $name = $request->get('name');
    if ($name) {
      $query->condition('name', $name, 'CONTAINS');
    }

    $paid__number = $request->get('paid__number');
    if ($paid__number) {
      $query->condition('paid__number', $paid__number);
    }


    $query->accessCheck(TRUE);
    $query->sort($this->entityType->getKey('id'), 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query->execute();
  }
}
