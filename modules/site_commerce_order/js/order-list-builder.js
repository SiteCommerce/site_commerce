(function (Drupal, once) {
	'use strict';

	Drupal.behaviors.setOrderStatus = {
		attach(context) {
			const buttonElements = once('setOrderStatus', document.querySelectorAll('.js-set-order-status'), context);
			buttonElements.forEach(function (item, index) {
				item.addEventListener('change', (event) => {

					let order_id = parseInt(event.target.dataset.id);
					let status = event.target.value;

					// Выполняет запрос ajax.
					var ajaxObject = Drupal.ajax({
						url: '/js-set-order-status/' + order_id + '/' + status + '/nojs',
						base: false,
						element: false,
						progress: false,
					});
					ajaxObject.execute();
				});
			});
		},
	};
})(Drupal, once);
