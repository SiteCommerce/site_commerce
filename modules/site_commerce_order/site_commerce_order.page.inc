<?php

use Drupal\site_payments\TransactionInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\kvantstudio\Formatter;

/**
 * Implements template_preprocess_site_commerce_order().
 */
function template_preprocess_site_commerce_order(&$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['site_commerce_order'] = $variables['elements']['#site_commerce_order'];
  $order = $variables['site_commerce_order'];

  // Helpful $content variable for templates.
  $variables['content'] = [];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  /** @var \Drupal\site_commerce_cart\CartStorageInterface $storage_cart */
  $storage_cart = \Drupal::entityTypeManager()->getStorage('site_commerce_cart');
  $variables['products'] = $storage_cart->loadCart($order->id());
}

/**
 * Implements template_preprocess_site_commerce_order_checkout().
 */
function template_preprocess_site_commerce_order_checkout(&$variables) {
  /** @var \Drupal\site_commerce_cart\CartStorageInterface $storage_cart */
  $storage_cart = \Drupal::entityTypeManager()->getStorage('site_commerce_cart');
  if (!$storage_cart->hasPositionsInCart()) {
    $url = Url::fromRoute('site_commerce_cart.cart_page')->toString();
    $response = new RedirectResponse($url, 302);
    $response->send();
  }

  // Шаблон блока с информацией о составе и стоимости заказа.
  $elements = [
    '#theme' => 'site_commerce_order_checkout_details',
  ];
  $variables['checkout_details'] = $elements;
}

/**
 * Implements template_preprocess_site_commerce_order_checkout_details().
 */
function template_preprocess_site_commerce_order_checkout_details(&$variables) {
  /** @var \Drupal\site_commerce_cart\CartStorageInterface $storage_cart */
  $storage_cart = \Drupal::entityTypeManager()->getStorage('site_commerce_cart');
  $variables['positions_in_cart'] = $storage_cart->hasPositionsInCart();
  if ($variables['positions_in_cart']) {
    // Загружает массив с данными корзины.
    $variables['products'] = $storage_cart->loadCart();

    // Шаблон блока с информацией о стоимости заказа на странице подтверждения заказа.
    $elements = [
      '#theme' => 'site_commerce_order_checkout_order_information',
    ];
    $variables['order_information'] = $elements;
  }
}

/**
 * Implements template_preprocess_site_commerce_order_checkout_order_information().
 */
function template_preprocess_site_commerce_order_checkout_order_information(&$variables) {
  /** @var \Drupal\site_commerce_order\OrderStorageInterface $storage_order */
  $storage_order = \Drupal::entityTypeManager()->getStorage('site_commerce_order');

  // Loading the configuration.
  $config = \Drupal::config('site_commerce_order.settings');

  // Define the price.
  $value = $storage_order->getPriceByCart();
  $variables['order_cost'] = Formatter::price($value);
  $variables['default_currency_code'] = $config->get('default_currency_code') ? $config->get('default_currency_code') : "RUB";

  // Define the currency symbol.
  $currency_entities = \Drupal::entityTypeManager()->getStorage('site_commerce_currency')->loadByProperties([
    'letter_code' => $variables['default_currency_code']
  ]);
  /** @var \Drupal\site_commerce_price\Entity\Currency $currency */
  $currency = reset($currency_entities);
  $variables['symbol'] = $currency->getSymbol();
}

/**
 * Implements template_preprocess_site_commerce_order_user_orders_dashboard().
 */
function template_preprocess_site_commerce_order_user_orders_dashboard(&$variables) {
  // Loading the configuration.
  $config = \Drupal::config('site_commerce_order.settings');

  // Текущий пользователь.
  $uid = \Drupal::currentUser()->id();

  // Loads orders by owner.
  $orders = \Drupal::entityTypeManager()->getStorage('site_commerce_order')->loadByProperties([
    'owner' => $uid
  ]);

  $variables['orders'] = [];
  $count = 1;

  /** @var \Drupal\site_commerce_order\OrderInterface $order */
  foreach ($orders as $oid => $order) {
    $variables['orders'][$count] = [
      'order_uuid' => $order->uuid(),
      'number' => $order->getPaid(),
      'symbol' => $order->getPaidCurrencySymbol(),
    ];

    $count++;
  }

  // Путь до каталога товаров.
  $config = \Drupal::config('site_commerce_product.settings');
  $variables['catalog_url'] = empty($config->get('catalog_url')) ? 'catalog' : $config->get('catalog_url');
}

/**
 * Implements template_preprocess_site_commerce_order_confirmed().
 */
function template_preprocess_site_commerce_order_confirmed(&$variables) {
  // Примечание после оформления заказа.
  $config = \Drupal::config('site_commerce_order.settings');
  $variables['note_after_ordering'] = $config->get('note_after_ordering');
}

/**
 * Implements template_preprocess_site_commerce_order_view().
 */
function template_preprocess_site_commerce_order_view(&$variables) {
  /** @var \Drupal\site_commerce_cart\CartStorageInterface $storage_cart */
  $storage_cart = \Drupal::entityTypeManager()->getStorage('site_commerce_cart');

  // Получаем номер заказа.
  $result = \Drupal::entityTypeManager()->getStorage('site_commerce_order')->loadByProperties([
    'uuid' => $variables['order_uuid']
  ]);
  /** @var \Drupal\site_commerce_order\OrderInterface $order */
  $order = reset($result);

  // URL на страницу удаления заказа.
  $url = Url::fromRoute('site_commerce_order.delete', ['order_uuid' => $order->uuid()], ['absolute' => TRUE]);

  // Статусы, при которых запрещается удалять заказы.
  // TODO: Вынести массив статусов в настройки модуля.
  $statuses = ['canceled', 'delivery', 'finished'];

  // Попытка загрузки транзакции по номеру заказа.
  $variables['order']['allow_delete'] = FALSE;
  if (\Drupal::entityTypeManager()->hasDefinition('site_payments_transaction')) {
    $storage_transaction = \Drupal::entityTypeManager()->getStorage('site_payments_transaction');
    $transactions = $storage_transaction->loadByProperties(['order_id' => $order->id()]);

    /** @var \Drupal\site_payments\TransactionInterface $transaction */
    $transaction = reset($transactions);

    // Если транзакция существует, проверяем ее статус оплаты.
    if ($transaction instanceof TransactionInterface) {
      // Если транзакция не оплачена и заказ новый, разрешаем удалить заказ.
      if (!$transaction->isPaid() && !\in_array($order->getStatusCode(), $statuses)) {
        $variables['order']['allow_delete'] = TRUE;
      }
    } else {
      // Если заказ не в процессе доставки и завершен, разрешаем удалить заказ.
      if (!\in_array($order->getStatusCode(), $statuses)) {
        $variables['order']['allow_delete'] = TRUE;
      }
    }
  } else {
    // Если заказ не в процессе доставки и завершен, разрешаем удалить заказ.
    if (!\in_array($order->getStatusCode(), $statuses)) {
      $variables['order']['allow_delete'] = TRUE;
    }
  }

  $variables['order'] += [
    'number' => $order->getPaid(),
    'symbol' => $order->getPaidCurrencySymbol(),
    'url' => $url->toString(),
    'status' => $order->getStatus(),
  ];

  // Загружает массив с данными корзины.
  $variables['products'] = $storage_cart->loadCart($order->id());
}
