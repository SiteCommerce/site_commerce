<?php

namespace Drupal\site_commerce_price;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the list builder for currencies.
 */
class CurrencyListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'name' => $this->t('Currency name'),
      'letter_code' => $this->t('Letter currency code'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [
      'name' => $entity->label(),
      'letter_code' => $entity->id(),
    ];

    return $row + parent::buildRow($entity);
  }

}
