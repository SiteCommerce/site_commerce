<?php

namespace Drupal\site_commerce_price\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\NumericItemBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the group price field type.
 *
 * @FieldType(
 *   id = "site_commerce_price_group",
 *   label = @Translation("Group price"),
 *   description = @Translation("Stores information about the decimal value of the group price, currency code, price group, and price notes."),
 *   category = @Translation("SiteCommerce"),
 *   default_widget = "site_commerce_price_group_default",
 *   default_formatter = "site_commerce_price_group_default"
 * )
 */
class PriceGroupItem extends NumericItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $currencies = \Drupal::entityTypeManager()->getStorage('site_commerce_currency')->loadMultiple();
    $currency_codes = array_keys($currencies);

    return [
      'available_currencies' => [],
      'all_currencies' => $currency_codes,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $currencies = \Drupal::entityTypeManager()->getStorage('site_commerce_currency')->loadMultiple();
    $currency_codes = array_keys($currencies);

    $element = [];
    $element['available_currencies'] = [
      '#type' => 'checkboxes',
      '#title' => new TranslatableMarkup('Available currencies'),
      '#description' => new TranslatableMarkup('If no currencies are selected, all currencies will be available.'),
      '#options' => array_combine($currency_codes, $currency_codes),
      '#default_value' => $this->getSetting('available_currencies'),
      '#multiple' => TRUE,
      '#size' => 5,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['group'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Price group'))
      ->setSetting('case_sensitive', FALSE)
      ->setRequired(TRUE);

    $properties['prefix'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Price prefix'))
      ->setSetting('case_sensitive', FALSE)
      ->setRequired(FALSE);

    $properties['suffix'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Price suffix'))
      ->setSetting('case_sensitive', FALSE)
      ->setRequired(FALSE);

    $properties['number_from'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Price from'))
      ->setRequired(FALSE);

    $properties['number'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Price'))
      ->setRequired(FALSE);

    $properties['number_sale'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Price sale'))
      ->setDescription(t('The final selling price for the selected price group, which is formed taking into account the discount. It is calculated according to the algorithm (if the discount field is filled in) or set manually.'))
      ->setRequired(FALSE);

    $properties['currency_code'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Currency'))
      ->setSetting('case_sensitive', FALSE)
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'group' => [
          'type' => 'varchar',
          'length' => 255,
          'binary' => FALSE,
          'description' => new TranslatableMarkup('Price group')
        ],
        'prefix' => [
          'type' => 'varchar',
          'length' => 255,
          'binary' => FALSE,
          'description' => new TranslatableMarkup('Price prefix')
        ],
        'suffix' => [
          'type' => 'varchar',
          'length' => 255,
          'binary' => FALSE,
          'description' => new TranslatableMarkup('Price suffix')
        ],
        'number_from' => [
          'type' => 'numeric',
          'precision' => 32,
          'scale' => 10,
          'description' => new TranslatableMarkup('Price from')
        ],
        'number' => [
          'type' => 'numeric',
          'precision' => 32,
          'scale' => 10,
          'description' => new TranslatableMarkup('Price')
        ],
        'number_sale' => [
          'type' => 'numeric',
          'precision' => 32,
          'scale' => 10,
          'description' => new TranslatableMarkup('Price sale')
        ],
        'currency_code' => [
          'type' => 'varchar',
          'length' => 3,
          'binary' => FALSE,
          'description' => new TranslatableMarkup('Letter currency code')
        ],
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->group === NULL || $this->number === '' || empty($this->currency_code);
  }

  /**
   * Return available fields array for display in template.
   */
  public static function getAvailableFields() {
    return [
      'group' => new TranslatableMarkup('Price group'),
      'number_from' => new TranslatableMarkup('Price from'),
      'number' => new TranslatableMarkup('Price'),
      'number_sale' => new TranslatableMarkup('Price sale'),
      'currency_code' => new TranslatableMarkup('Letter currency code'),
      'prefix' => new TranslatableMarkup('Price prefix'),
      'suffix' => new TranslatableMarkup('Price suffix'),
    ];
  }
}
