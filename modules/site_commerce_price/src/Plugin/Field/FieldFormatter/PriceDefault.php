<?php

namespace Drupal\site_commerce_price\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the price formatter.
 *
 * @FieldFormatter(
 *   id = "site_commerce_price_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_commerce_price"
 *   }
 * )
 */
class PriceDefault extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    $values = [];

    foreach ($items as $delta => $item) {
      $values = [
        'number' => \Drupal::service('kvantstudio.formatter')->price($item->number),
        'currency_code' => $item->currency_code,
      ];
    }

    $elements = [
      '#theme' => 'site_commerce_price_group_default_formatter',
      '#data' => $values,
    ];

    return $elements;
  }

}
