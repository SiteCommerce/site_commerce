/**
 * @file
 * Implements functions for catalog of products.
 */
(function () {

  'use strict';

  const products = document.getElementById('catalog-products__items');

  const ro = new ResizeObserver(entries => {
    for (let entry of entries) {
      const cr = entry.contentRect;

      products.classList.remove('catalog-products__items-2');
      if (cr.width >= 768) {
        products.classList.add('catalog-products__items-2');
      }

      products.classList.remove('catalog-products__items-3');
      if (cr.width >= 992) {
        products.classList.add('catalog-products__items-3');
      }

      products.classList.remove('catalog-products__items-4');
      if (cr.width >= 1200) {
        products.classList.add('catalog-products__items-4');
      }

      products.classList.remove('catalog-products__items-5');
      if (cr.width >= 1600) {
        products.classList.add('catalog-products__items-5');
      }
    }
  });
  ro.observe(document.getElementById('catalog-products'));

})();
