/**
* @file
* JavaScript behaviors for Ajax.
*/
(function($, Drupal) {
  "use strict";

  /**
   * Ajax command to show products by categories.
   *
   * @param {Drupal.Ajax} [ajax]
   *   An Ajax object.
   * @param {object} response
   *   The Ajax response.
   * @param {object} response
   *   Data for load.
   * @param {number} [status]
   *   The HTTP status code.
   */
  Drupal.AjaxCommands.prototype.productsLoadByCategories = (ajax, response, status) => {
    // TODO: запрещаем использовать загрузку товаров по ajax.
    // Наблюдается проблема при добавлении товаров в корзину
    // после выполнения данной команды.
    // // Ajax object.
    // var ajaxObject = Drupal.ajax({
    //   url: '/ajax-products-load-by-categories/nojs',
    //   submit: {
    //     data: JSON.stringify(response)
    //   }
    // });

    // // Send a request to the server.
    // ajaxObject.execute().done();

    // Перезагружаем страницу.
    location.reload();
  };

})(jQuery, Drupal);
