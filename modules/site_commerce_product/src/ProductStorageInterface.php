<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for product entity storage class.
 */
interface ProductStorageInterface extends ContentEntityStorageInterface {

  /**
   * Count of products in a given сategories and attributes.
   *
   * @param array $tids
   *   Categories ID array.
   * @param array $attribute_ids
   *   Attribute ID array.
   *
   * @return int
   *   A count of products in a given сategories and attributes.
   */
  public function countProductsByCategoriesAndAttributes(array $tids, array $attribute_ids): int;

  /**
   * Count of products in a given сategory.
   *
   * @param int|null $tid
   *   Category ID.
   * @param bool $child
   *   If TRUE chech with children categories.
   *
   * @return int
   *   A count of products in a given сategory.
   */
  public function countProductsByCategory(?int $tid = NULL, bool $child = TRUE): int;

}
