<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * Defines an interface for attributes group entity storage classes.
 */
interface ProductAttributeGroupStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Gets top-level attributes IDs of groups.
   *
   * @param array $gids
   *   Array of group IDs.
   *
   * @return array
   *   Array of top-level attribute IDs.
   */
  public function getToplevelAttributes($gids);

}
