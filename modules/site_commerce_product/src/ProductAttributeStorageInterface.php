<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\site_commerce_product\ProductAttributeInterface;
use Drupal\site_commerce_product\ProductInterface;

/**
 * Defines an interface for attribute entity storage class.
 */
interface ProductAttributeStorageInterface extends ContentEntityStorageInterface {

  /**
   * Finds all links of a given attributes and delete it from products.
   *
   * @param array $attributes
   *   Deleted attributes.
   */
  public function deleteProductLink(array $attributes);

  /**
   * Finds all parents of a given attribute ID.
   *
   * @param int $aid
   *   Attribute ID to retrieve parents for.
   *
   * @return \Drupal\site_commerce_product\ProductAttributeInterface[]
   *   An array of attribute objects which are the parents of the attribute $aid.
   */
  public function loadParents(int $aid);

  /**
   * Finds all children of a attribute ID.
   *
   * @param int $aid
   *   Attribute ID to retrieve children for.
   *
   * @return \Drupal\site_commerce_product\ProductAttributeInterface[]
   *   An array of attribute objects that are the children of the attribute $aid.
   */
  public function loadChildren(int $aid);

  /**
   * Returns all children attributes of this attribute.
   *
   * \Drupal\site_commerce_product\ProductAttributeInterface $attribute
   *   Attribute to retrieve children for.
   *
   * @return \Drupal\site_commerce_product\ProductAttributeInterface[]
   *   An array of attribute objects that are the children of the attribute $aid.
   */
  public function getChildren(ProductAttributeInterface $attribute);

  /**
   * Finds all attributes in a given group ID.
   *
   * @param string $gid
   *   Group ID to retrieve attributes for.
   * @param int $parent
   *   The attribute ID under which to generate the tree. If 0, generate the tree
   *   for the entire group.
   * @param int $max_depth
   *   The number of levels of the tree to return. Leave 0 to return all
   *   levels.
   * @param bool $load_entities
   *   If TRUE, a full entity load will occur on the attribute objects. Defaults to FALSE.
   *
   * @return object[]|\Drupal\site_commerce_product\ProductAttributeInterface[]
   *   An array of attribute objects that are the children of the group $gid.
   */
  public function loadTree($gid, int $parent = 0, int $max_depth = 0, bool $load_entities = FALSE);

  /**
   * Finds links with attribute ID and product ID.
   *
   * @param \Drupal\site_commerce_product\ProductAttributeInterface $attribute
   *   Attribute entity.
   *
   * @param \Drupal\site_commerce_product\ProductInterface $entity
   *   Product entity.
   *
   * @return bool
   *   TRUE if link exist
   */
  public function existLinkWithProduct(ProductAttributeInterface $attribute, ProductInterface $entity);

}
