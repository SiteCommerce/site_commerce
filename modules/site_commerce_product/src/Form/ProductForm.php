<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Core\Field\WidgetBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the Product entity edit form.
 *
 * @ingroup site_commerce_product
 */
class ProductForm extends ContentEntityForm {

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_commerce_product_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\site_commerce_product\ProductInterface $entity */
    $entity = $this->entity;

    $form['advanced'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity-meta']],
      '#weight' => 100,
    ];

    $form['changed'] = [
      '#type' => 'hidden',
      '#default_value' => $entity->getChangedTime(),
    ];

    /* @var $entity \Drupal\site_commerce_product\Entity\Product */
    $form = parent::buildForm($form, $form_state);

    $form['meta'] = [
      '#type' => 'container',
      '#group' => 'advanced',
      '#weight' => -10,
      '#title' => $this->t('Status'),
      '#attributes' => ['class' => ['entity-meta__header']],
      '#tree' => TRUE,
      '#access' => TRUE,
    ];
    $form['meta']['published'] = [
      '#type' => 'item',
      '#markup' => $entity->isPublished() ? $this->t('Published') : $this->t('Not published'),
      '#access' => !$entity->isNew(),
      '#wrapper_attributes' => ['class' => ['entity-meta__title']],
    ];
    $form['meta']['changed'] = [
      '#type' => 'item',
      '#title' => $this->t('Last saved'),
      '#markup' => !$entity->isNew() ? \Drupal::service('date.formatter')->format($entity->getChangedTime(), 'short') : $this->t('Not saved yet'),
      '#wrapper_attributes' => ['class' => ['entity-meta__last-saved']],
    ];
    $form['meta']['author'] = [
      '#type' => 'item',
      '#title' => $this->t('Author'),
      '#markup' => $entity->getOwner()->getAccountName(),
      '#wrapper_attributes' => ['class' => ['entity-meta__author']],
    ];

    $form['statuses'] = [
      '#type' => 'container',
      '#group' => 'advanced',
      '#weight' => -10,
      '#attributes' => ['class' => ['entity-meta__header']],
      '#tree' => TRUE,
      '#access' => TRUE,
    ];

    if (isset($form['status'])) {
      $form['status']['#group'] = 'statuses';
    }

    if (isset($form['status'])) {
      $form['status_catalog']['#group'] = 'statuses';
    }

    $form['group'] = [
      '#type' => 'details',
      '#title' => t('Display settings'),
      '#group' => 'advanced',
      '#weight' => -5,
      '#attributes' => ['class' => ['node-form-author']],
      '#tree' => TRUE,
      '#access' => TRUE,
    ];

    if (isset($form['settings'])) {
      $form['settings']['#group'] = 'group';
    }

    if (isset($form['search_index'])) {
      $form['search_index']['#group'] = 'group';
    }

    $form['author'] = [
      '#type' => 'details',
      '#title' => t('Authoring information'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['node-form-author'],
      ],
      '#attached' => [
        'library' => ['node/drupal.node'],
      ],
      '#weight' => 90,
      '#optional' => TRUE,
    ];

    if (isset($form['uid'])) {
      $form['uid']['#group'] = 'author';
    }

    if (isset($form['created'])) {
      $form['created']['#group'] = 'author';
    }

    // Виджет стоимости.
    $field_updated = $form['price'];
    $field_updated['widget']['#theme'] = 'site_commerce_product_field_cost_multiple_value_form';
    $field_state = WidgetBase::getWidgetState($form['#parents'], $field_updated['widget']['#field_name'], $form_state);
    if ($field_state['items_count']) {
      unset($field_updated['widget'][$field_state['items_count']]);
    }
    $form['price'] = $field_updated;

    $form['#theme'] = 'site_commerce_product_form';
    $form['#attached']['library'][] = 'site_commerce_product/form';
    $form['#attached']['library'][] = 'site_commerce_product/form_settings';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->save();
    $form_state->setRedirect('entity.site_commerce_product.canonical', [
      'site_commerce_product' => $entity->id(),
    ]);
  }
}
