<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\language\Entity\ContentLanguageSettings;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\site_commerce_product\ProductAttributeGroupStorageInterface;

/**
 * Base form for attributes group edit forms.
 */
class ProductAttributeGroupForm extends BundleEntityFormBase {

  /**
   * The product attribute group storage.
   *
   * @var \Drupal\site_commerce_product\ProductAttributeGroupStorageInterface
   */
  protected $productAttributeGroupStorage;

  /**
   * Constructs a new product attribute group form.
   *
   * @param \Drupal\site_commerce_product\ProductAttributeGroupStorageInterface $product_attribute_group_storage
   *   The product attribute group storage.
   */
  public function __construct(ProductAttributeGroupStorageInterface $product_attribute_group_storage) {
    $this->productAttributeGroupStorage = $product_attribute_group_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('site_commerce_attribute_group')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $group = $this->entity;

    if ($group->isNew()) {
      $form['#title'] = $this->t('Add group');
    }
    else {
      $form['#title'] = $this->t('Edit group');
    }

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $group->label(),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];

    $form['gid'] = [
      '#type' => 'machine_name',
      '#default_value' => $group->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['name'],
      ],
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $group->getDescription(),
    ];

    $form['filter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow displaying as a filter when searching'),
      '#default_value' => $group->getFilter(),
    ];

    $form['expanded'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow to display the list of attributes in the group expanded'),
      '#default_value' => $group->getExpanded(),
    ];

    // $form['langcode'] is not wrapped in an
    // if ($this->moduleHandler->moduleExists('language')) check because the
    // language_select form element works also without the language module being
    // installed. https://www.drupal.org/node/1749954 documents the new element.
    $form['langcode'] = [
      '#type' => 'language_select',
      '#title' => $this->t('Language for the attributes group'),
      '#languages' => LanguageInterface::STATE_ALL,
      '#default_value' => $group->language()->getId(),
    ];
    if ($this->moduleHandler->moduleExists('language')) {
      $form['default_attributes_language'] = [
        '#type' => 'details',
        '#title' => $this->t('Language for the attributes'),
        '#open' => FALSE,
      ];
      $form['default_attributes_language']['default_language'] = [
        '#type' => 'language_configuration',
        '#entity_information' => [
          'entity_type' => 'site_commerce_attribute',
          'bundle' => $group->id(),
        ],
        '#default_value' => ContentLanguageSettings::loadByEntityTypeBundle('site_commerce_attribute', $group->id()),
      ];
    }

    // Set the hierarchy to "multiple parents" by default.
    $form['hierarchy'] = [
      '#type' => 'value',
      '#value' => '0',
    ];

    $form = parent::form($form, $form_state);
    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $group = $this->entity;

    // Prevent leading and trailing spaces in attribute names.
    $group->set('name', trim($group->label()));

    $status = $group->save();
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created new attributes group %name.', ['%name' => $group->label()]));
        $form_state->setRedirectUrl($group->toUrl('collection'));
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('Updated attributes group %name.', ['%name' => $group->label()]));
        $form_state->setRedirectUrl($group->toUrl('collection'));
        break;
    }

    $form_state->setValue('gid', $group->id());
    $form_state->set('gid', $group->id());
  }

  /**
   * Determines if the attributes group already exists.
   *
   * @param string $gid
   *   The attributes group ID.
   *
   * @return bool
   *   TRUE if the attributes group exists, FALSE otherwise.
   */
  public function exists($gid) {
    $action = $this->productAttributeGroupStorage->load($gid);
    return !empty($action);
  }

}
