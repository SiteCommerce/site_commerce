<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ProductSettingsForm.
 * @package Drupal\site_commerce_product\Form
 * @ingroup site_commerce_product
 */
class ProductSettingsForm extends ConfigFormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'site_commerce_product_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_commerce_product.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config('site_commerce_product.settings');

    $form['catalog'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings of catalog'),
    ];

    // Название страницы каталога товаров.
    $form['catalog']['catalog_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The title of the product catalog'),
      '#default_value' => $config->get('catalog_title') ? $config->get('catalog_title') : $this->t('Product catalog'),
    ];

    // URL страницы каталога товаров.
    $form['catalog']['catalog_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The url of the product catalog'),
      '#default_value' => $config->get('catalog_url') ? $config->get('catalog_url') : "catalog",
      '#description' => $this->t('The path, which begins with the Url of the product catalog.'),
    ];

    $form['product'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Product page settings'),
    ];

    // URL страницы товара.
    $form['product']['product_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The url of the product page'),
      '#default_value' => $config->get('product_url') ? $config->get('product_url') : "product",
      '#description' => $this->t('The path, which begins with the Url of the product page.'),
    ];

    // Примечание, которое отображается в поле цена товара при ее отсутствии.
    $form['product']['product_note_cost_none'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Note that appears in the item price field when it is not present'),
      '#default_value' => $config->get('product_note_cost_none') ? $config->get('product_note_cost_none') : "",
    ];

    // Разрешить отображать заголовок товара над формой добавления в корзину.
    $form['product']['product_allow_title'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow displaying the product title above the add to cart form'),
      '#default_value' => $config->get('product_allow_title') ? $config->get('product_allow_title') : 0,
      '#description' => '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Записывает значения в конфигурацию.
    $config = \Drupal::service('config.factory')->getEditable('site_commerce_product.settings');
    $config->set('catalog_title', trim($form_state->getValue('catalog_title')));
    $config->set('catalog_url', trim($form_state->getValue('catalog_url')));
    $config->set('product_url', trim($form_state->getValue('product_url')));
    $config->set('product_note_cost_none', trim($form_state->getValue('product_note_cost_none')));
    $config->set('product_allow_title', (bool) $form_state->getValue('product_allow_title'));

    $config->save();
  }
}
