<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to duplicate an Product type entities.
 */

class ProductTypeDuplicateForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to duplicate %name?', array('%name' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.site_commerce_product_type.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Duplicate');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();

    $content = array();

    foreach (\Drupal::entityTypeManager()->getStorage('site_commerce_product_type')->loadMultiple() as $type) {
      $content[] = $type->id();
    }

    $duplicate = $entity->createDuplicateType("product_" . count($content));

    $duplicate->save();

    $messenger = \Drupal::messenger();
    $messenger->addMessage($this->t('Product type %label has been duplicated.', ['%label' => $duplicate->label()]));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}
