<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\site_commerce_product\Ajax\ProductsLoadByFilters;

/**
 * Class implement form for loading products by filters.
 */
class ProductsLoadByFiltersForm extends FormBase {

  /**
   * Массив выбранных фильтров.
   */
  protected $aids = [];

  /**
   * Ранее выбранные фильтры.
   */
  protected $selected_aids = [];

  /**
   * Марки.
   */
  protected $marks = [];

  /**
   * Конструктор класса.
   * @param int $parent - родительский термин.
   * @return void
   */
  public function __construct() {
    // Загружаем перечень групп фильтров.
    $result = \Drupal::entityTypeManager()
    ->getStorage('site_commerce_attribute_group')
    ->loadByProperties([
      'filter' => TRUE
    ]);


    foreach ($result as $id => $group) {

      $result_attributes = \Drupal::entityTypeManager()
      ->getStorage('site_commerce_attribute')
      ->loadByProperties([
        'gid' => $id,
        'filter' => TRUE,
        'parent' => 0
      ]);

      foreach ($result_attributes as $attribute) {
        $marks[$id][] = [

        ];
      }
    }

    $session = \Drupal::service('session');
    $data = $session->get('selected_aids');
    if (empty($data->tids)) {
      $this->selected_aids = [];
    } else {
      $this->selected_aids = $data->aids;
    }
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'site_commerce_products_load_by_filters_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Класс для элементов формы.
    $form_class = Html::getClass($this->getFormId());

    // Обертка элементов формы.
    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $form_class . '__wrapper',
      ],
    ];

    // Добавляем нашу кнопку для отправки.
    $form['wrapper']['actions']['#type'] = 'actions';
    $form['wrapper']['actions']['#attributes'] = ['class' => [$form_class . '__actions']];

    // Кнопка Применить.
    $form['wrapper']['actions']['load_products'] = [
      '#type' => 'submit',
      '#id' => $form_class . '__load-products',
      '#value' => $this->t('Apply'),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => '::ajaxCallback',
      ],
      '#attributes' => [
        'class' => [$form_class . '__btn', $form_class . '__btn-load-products'],
      ],
    ];

    $form['#attached']['library'][] = 'site_commerce_product/catalog_products';
    $form['#attached']['library'][] = 'site_commerce_product/products_load_by_categories';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Ajax load products callback.
   */
  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // Если нет ошибок валидации.
    if (!$form_state->hasAnyErrors()) {
      $categories = $form_state->getValue('categories');
      $tids = [];
      foreach ($categories as $key => $value) {
        if ($value) {
          $tids[] = (int) $key;
        }
      }
    }

    if (count($tids)) {
      $session = \Drupal::service('session');
      $data = [
        'id' => '#products-load',
        'tids' => $tids,
      ];
      $session->set('selected_aids', (object) $data);
      $response->addCommand(new ProductsLoadByFilters($data['id'], $data['tids']));
    }

    return $response;
  }
}
