<?php

namespace Drupal\site_commerce\Form;

use Drupal\site_commerce\Form\Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Drupal\Core\Url;

/**
 * Form controller for the SiteCommerce Get Price.
 * TODO: not work, need fix it.
 *
 * @ingroup site_commerce
 */
class SiteCommerceGetPriceForm extends FormBase {
  /**
   * {@inheritdoc}.
   */
  protected $type;

  /**
   * {@inheritdoc}.
   */
  public function __construct($type = 'default') {
    $this->type = $type;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_commerce_get_price_form_' . $this->type;
  }

  /**
   *
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $tid = NULL) {
    $form['#tree'] = true;

    $form['#attributes'] = ['class' => ['site-commerce-get-price-form']];

    $form_state->setCached(FALSE);

    $term = Term::load($tid);

    $form['term'] = array(
      '#type' => 'value',
      '#value' => $term,
    );

    //$form['actions']['#type'] = 'actions';
    $form['download'] = [
      '#type' => 'submit',
      '#name' => 'submit',
      '#value' => $this->t('Download price'),
      '#button_type' => 'primary',
      '#attributes' => ['class' => ['site-commerce-get-price-form__get-price-button']],
    ];

    $url = Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $tid]);
    //$link = Link::fromTextAndUrl($term->getName(), $url);

    $form['name'] = array(
      '#type' => 'link',
      '#url' => $url,
      '#title' => $term->getName(),
      // '#markup' => '<div class="site-commerce-get-price-form__name-price">' . $link->toRenderable() . '</div>',
      '#attributes' => ['class' => ['site-commerce-get-price-form__name-price'], 'target' => '_blank'],
    );

    $form['#attached']['library'][] = 'site_commerce/get_price_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $database = Database::getConnection();

    $term = $form_state->getValues()['term'];

    $scheme = 'public';

    $inputFileName = $scheme . '://export/templates/getPrice.xls';
    $reader = IOFactory::createReader("Xls");
    $spreadsheet = $reader->load($inputFileName);

    // Формируем данные в шаблон.
    $spreadsheet->setActiveSheetIndex(0);
    $worksheet = $spreadsheet->getActiveSheet();

    // Название прайс-листа.
    $worksheet->setCellValue([1, 4], $term->getName());

    // Перечень товаров по категории.
    $query = $database->select('site_commerce_field_data', 'n');
    $query->condition('n.status', 1);
    $query->condition('n.status_catalog', 1);
    $query->fields('n', array('pid'));
    $query->innerJoin('site_commerce__field_category', 't', 't.entity_id=n.pid');
    $query->condition('t.field_category_target_id', $term->id());
    $result = $query->execute();

    $pids = [];
    foreach ($result as $row) {
      $pids[] = $row->pid;
    }

    if ($pids) {
      // Массовая загрузка перечня товаров.
      $site_commerces = \Drupal::entityTypeManager()->getStorage('site_commerce_product')->loadMultiple($pids);

      $id = 1;
      $row = 8;
      foreach ($site_commerces as $position) {
        $title = $position->getTitle();

        // Формируем стоимость товара.
        $data = $position->getPrice();
        if ($data['from']) {
          $cost = $this->t('from') . ' ' . $data['from'];
        } else {
          $cost = $data['value'];
        }
        if (!$cost) {
          $cost = $this->t('Check the price');
        }

        // Единица кол-ва.
        $unit = $position->get('settings')->quantity_unit;

        // Загружаем данные в Excel.
        $worksheet->getCell('A' . $row)->setValue($id);
        $worksheet->getCell('B' . $row)->setValue($title);
        $worksheet->getCell('AQ' . $row)->setValue($cost);
        $worksheet->getCell('AX' . $row)->setValue($unit);

        $id++;
        $row++;
      }

      // Скрываем пустые строки.
      $count_hide_row = $row;
      for ($i = $count_hide_row; $i <= 1000; $i++) {
        $worksheet->getRowDimension($i)->setOutlineLevel(1);
        $worksheet->getRowDimension($i)->setVisible(false);
        $count_hide_row++;
      }
      $worksheet->getRowDimension($count_hide_row)->setCollapsed(true);
    }

    // Формируем имя файла.
    $filename = $term->getName() . '-' . date('d-m-Y-H-i-s', \Drupal::time()->getRequestTime()) . '.xls';
    $uri = $scheme . '://export/' . $filename;

    $writer = IOFactory::createWriter($spreadsheet, "Xls");
    $writer->save($uri);

    if (StreamWrapperManagerInterface::isValidScheme($scheme) && file_exists($uri)) {
      $response = new BinaryFileResponse($uri);
      $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
      $form_state->setResponse($response);
    }
  }
}
