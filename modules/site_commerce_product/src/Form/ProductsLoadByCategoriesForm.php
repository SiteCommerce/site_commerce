<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\site_commerce_product\Ajax\ProductsLoadByCategories;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a products load by categories form.
 */
class ProductsLoadByCategoriesForm extends FormBase {

  /**
   * The tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * Category terms for selection.
   */
  protected $tids;

  /**
   * Constructs a \Drupal\site_commerce_product\Form\ProductsLoadByCategoriesForm object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory) {
    // Category terms for selection.
    $query = \Drupal::database()->select('taxonomy_term_field_data', 'n');
    $query->fields('n', ['tid', 'name']);
    $query->condition('n.vid', 'site_commerce_catalog');
    $query->orderBy('n.weight', 'ASC');
    $query->innerJoin('taxonomy_term__parent', 'h', 'h.entity_id=n.tid');
    $query->condition('h.parent_target_id', 0);
    $query->condition('h.bundle', 'site_commerce_catalog');
    $query->innerJoin('taxonomy_term__field_view', 'v', 'v.entity_id=n.tid');
    $query->condition('v.bundle', 'site_commerce_catalog');
    $query->condition('v.field_view_value', 1);
    $this->tids = $query->execute()->fetchAllKeyed();

    // The tempstore.
    $this->tempStore = $temp_store_factory->get('site_commerce_product');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'site_commerce_product_load_by_categories';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Class for form elements.
    $form_class = Html::getClass($this->getFormId());

    // Search filter values ​​for the current user.
    $taxonomy_search = $this->tempStore->get('taxonomy_search');

    // Wrapping form elements.
    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $form_class . '__wrapper',
      ],
    ];

    $form['wrapper']['categories'] = [
      '#type' => 'checkboxes',
      '#options' => $this->tids,
      '#title' => $this->t('Product catalog'),
      '#default_value' => $taxonomy_search['tids'],
    ];

    // Button to send.
    $form['wrapper']['actions']['#type'] = 'actions';
    $form['wrapper']['actions']['#attributes'] = ['class' => [$form_class . '__actions']];
    $form['wrapper']['actions']['load_products'] = [
      '#type' => 'submit',
      '#id' => $form_class . '__load-products',
      '#value' => $this->t('Apply'),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => '::ajaxSubmitCallback',
        'disable-refocus' => TRUE,
      ],
      '#attributes' => [
        'class' => [$form_class . '__btn', $form_class . '__btn-load-products'],
      ],
    ];

    $form['#attached']['library'][] = 'site_commerce_product/catalog_products';
    $form['#attached']['library'][] = 'site_commerce_product/products_load_by_categories';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Categories for load products.
    $tids = [];

    $categories = $form_state->getValue('categories');
    foreach ($categories as $key => $value) {
      if ($value) {
        $tids[] = (int) $key;
      }
    }

    $data = [
      'id' => 'catalog-products-ajax',
      'tids' => $tids,
    ];

    // Save the values ​​of search filters for the current user.
    $this->tempStore->set('taxonomy_search', $data);
  }

  /**
   * Ajax load products callback.
   */
  public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // Search filter values ​​for the current user.
    $taxonomy_search = $this->tempStore->get('taxonomy_search');
    $response->addCommand(new ProductsLoadByCategories($taxonomy_search));

    return $response;
  }
}
