<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the Product type entities add and edit forms.
 */
class ProductTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->entity;

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t("Short name of the product type."),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => array(
        'exists' => array($this, 'exist'),
      ),
      '#disabled' => !$entity->isNew(),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();
    $entity = $this->entity;
    $status = $entity->save();
    $message_params = [
      '%label' => $entity->label(),
    ];

    // Provide a message for the user and redirect them back to the collection.
    switch ($status) {
    case SAVED_NEW:
      $messenger->addMessage($this->t('Created the %label product type.', $message_params));
      break;

    default:
      $messenger->addMessage($this->t('Saved the %label product type.', $message_params));
    }

    $form_state->setRedirect('entity.site_commerce_product_type.collection');
  }

}
