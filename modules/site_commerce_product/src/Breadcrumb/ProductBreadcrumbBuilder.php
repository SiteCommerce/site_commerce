<?php

/**
 * @file
 * Contains \Drupal\site_commerce_product\Breadcrumb\ProductBreadcrumbBuilder.
 */

namespace Drupal\site_commerce_product\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\site_commerce_product\ProductInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Class to define the Product entities pages breadcrumb builder.
 */
class ProductBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The user settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The taxonomy storage.
   *
   * @var \Drupal\Taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Constructs the ProductBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
    $this->config = $config_factory->get('site_commerce_product.settings');
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * @inheritdoc
   */
  public function applies(RouteMatchInterface $route_match) {
    if ($route_match->getRouteName() == 'entity.site_commerce_product.canonical') {
      return TRUE;
    }
  }

  /**
   * @inheritdoc
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));

    // Ссылка на каталог продукции.
    if ($this->config->get('catalog_title') && $this->config->get('catalog_url')) {
      $link = Link::fromTextAndUrl($this->t('@value', ['@value' => $this->config->get('catalog_title')]), Url::fromUserInput('/' . $this->config->get('catalog_url')));
      $breadcrumb->addLink($link);
    }

    $site_commerce_product = $route_match->getParameter('site_commerce_product');
    $site_commerce_product = $this->entityRepository->getTranslationFromContext($site_commerce_product);
    if ($site_commerce_product instanceof ProductInterface) {
      $entity = $site_commerce_product->getCategory();
      if ($entity instanceof TermInterface) {
        $term = $this->entityRepository->getTranslationFromContext($entity);

        // Breadcrumb needs to have terms cacheable metadata as a cacheable
        // dependency even though it is not shown in the breadcrumb because e.g. its
        // parent might have changed.
        $breadcrumb->addCacheableDependency($term);

        // @todo This overrides any other possible breadcrumb and is a pure
        //   hard-coded presumption. Make this behavior configurable per
        //   vocabulary or term.
        $parents = $this->termStorage->loadAllParents($term->id());

        // Remove current term being accessed.
        array_shift($parents);
        foreach (array_reverse($parents) as $parents_term) {
          $parents_term = $this->entityRepository->getTranslationFromContext($parents_term);
          $breadcrumb->addCacheableDependency($parents_term);
          $breadcrumb->addLink(Link::createFromRoute($parents_term->label(), 'entity.taxonomy_term.canonical', array('taxonomy_term' => $parents_term->id())));
        }

        $breadcrumb->addLink(Link::createFromRoute($term->label(), 'entity.taxonomy_term.canonical', array('taxonomy_term' => $term->id())));
      }
    }

    // This breadcrumb builder is based on a route parameter, and hence it
    // depends on the 'route' cache context.
    $breadcrumb->addCacheContexts(['route']);

    return $breadcrumb;
  }
}
