<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;
/**
 * Provides an interface for defining Product entities.
 *
 * @ingroup site_commerce_product
 */
interface ProductInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Gets the product name.
   *
   * @return string
   *   The product name.
   */
  public function getTitle();

  /**
   * Sets the product name.
   *
   * @param string $title
   *   The product name.
   *
   * @return \Drupal\site_commerce_product\ProductInterface
   *   The called Product entity.
   */
  public function setTitle(string $title);

  /**
   * Gets the product creation timestamp.
   *
   * @return int
   *   Creation timestamp of the product.
   */
  public function getCreatedTime();

  /**
   * Sets the product creation timestamp.
   *
   * @param int $timestamp
   *   The product creation timestamp.
   *
   * @return \Drupal\site_commerce_product\ProductInterface
   *   The called Product entity.
   */
  public function setCreatedTime(int $timestamp);

  /**
   * Get the stock status view.
   *
   * @return int
   *   Stock status.
   */
  public function getStockAllow();

  /**
   * Get the count of products in stock.
   *
   * @return int
   *   Count of the product.
   */
  public function getStockValue();

  /**
   * Sets the count of products in stock.
   *
   * @param int $value
   *   The count of products in stock.
   *
   * @return \Drupal\site_commerce_product\ProductInterface
   *   The called Product entity.
   */
  public function setStockValue(int $value);

  /**
   * Get the quantity unit.
   *
   * @return string
   *   Quantity unit of the product.
   */
  public function getQuantityUnit();

  /**
   * Sets the unit of quantity.
   *
   * @param string $unit
   *   The unit of quantity.
   *
   * @return \Drupal\site_commerce_product\ProductInterface
   *   The called Product entity.
   */
  public function setQuantityUnit(string $unit);

  /**
   * Get the quantity order minimum value.
   *
   * @return int
   *   Quantity order minimum value.
   */
  public function getQuantityOrderMin();

  /**
   * Get the product prices by price group.
   * Values of prices: price from - 'number_from', basic - 'number', real price with discount - 'number_sale'.
   *
   * @param string|null $group
   *   The price group name.
   * @param bool $get_raw_value
   *   The price return type.
   * @return array
   *   The array of prices.
   */
  public function getPrices(?string $group = 'retail'): array;

  /**
   * Sets the data of price.
   *
   * @param array $values
   *   The data of price.
   *
   * @return \Drupal\site_commerce_product\ProductInterface
   *   The called Product entity.
   */
  public function setPrice(array $values, string $group);

  /**
   * Get the formatted prices.
   *
   * @param array $data
   *   The price of product from 'getPrice' metod.
   * @param int $decimals
   *   The number of characters after the delimiter.
   * @param string $dec_point
   *   Separator type. Dot by default.
   * @param string $thousands_sep
   *   Thousandth separator type.
   *
   * @return array
   *   The array of formatted prices.
   */
  public function formattedPrices(array $data, int $decimals = 2, string $dec_point = '.', string $thousands_sep = ' '): array;

  /**
   * Checks if the product has a price at which it can be bought by price group.
   *
   * @param string $group
   *   The price group name.
   *
   * @return bool
   *   TRUE if the product can be purchased at a specific base price or at a discount.
   *   If the product has only a price from, such product cannot be added to the cart and the buyer must check the price with the seller.
   */
  public function isPaidPriceExist(string $group = 'retail'): bool;

  /**
   * Gets value price at which product can be bought by price group.
   *
   * @param string $group
   *   The price group name.
   *
   * @return array
   *   Paid price value and currency code.
   */
  public function getPaidPrice(string $group = 'retail'): array;

  /**
   * Sets the weight of products in catalog.
   *
   * @param int $weight
   *   The weight of products.
   *
   * @return \Drupal\site_commerce_product\ProductInterface
   *   The called Product entity.
   */
  public function setWeight(int $weight);

  /**
   * Check if attribute set to the entity.
   *
   * @param \Drupal\site_commerce_product\ProductAttributeInterface $entity
   *   The attribute entity.
   *
   * @return bool TRUE || FALSE
   *   TRUE if attribute exist in the entity.
   */
  public function isAttributeExist(ProductAttributeInterface $entity);

  /**
   * Sets the attribute entity.
   *
   * @param \Drupal\site_commerce_product\ProductAttributeInterface $entity
   *   The attribute entity.
   *
   * @return \Drupal\site_commerce_product\ProductInterface
   *   The called Product entity.
   */
  public function setAttribute(ProductAttributeInterface $entity);

  /**
   * Get the taxonomy term entity of main category.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   The taxonomy term entity.
   */
  public function getCategory();

  /**
   * Get the taxonomy term entity id of main category.
   *
   * @return int
   *   The taxonomy term entity id.
   */
  public function getCategoryId();
}
