<?php

namespace Drupal\site_commerce_product\TwigExtension;

use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\site_commerce_product\Controller\ProductCatalogController;
use Drupal\site_commerce_product\Controller\ProductController;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension that adds a custom function and a custom filter.
 */
class ProductTwigExtension extends AbstractExtension {

  /**
   * The tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * Constructs a \Drupal\site_commerce_product\Form\ProductsLoadByCategoriesForm object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory) {
    $this->tempStore = $temp_store_factory->get('site_commerce_product');
  }

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return [
      new TwigFunction('site_commerce_product_catalog', $this->catalog(...)),
      new TwigFunction('site_commerce_product_catalog_by_view', $this->catalogByView(...)),
      new TwigFunction('site_commerce_product_catalog_by_term', $this->catalogByTerm(...)),
      new TwigFunction('site_commerce_product_catalog_list', $this->catalogList(...)),
      new TwigFunction('site_commerce_product_catalog_children_list', $this->catalogChildrenList(...)),
      new TwigFunction('site_commerce_product_load_by_categories_form', $this->loadByCategoriesForm(...)),
      new TwigFunction('site_commerce_product_price_editor', $this->priceEditor(...)),
      new TwigFunction('site_commerce_product_status_stock', $this->statusStock(...)),
      new TwigFunction('site_commerce_product_export_price', $this->exportPrice(...)),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'site_commerce_product.twig_extension';
  }

  /**
   * Корневые категории товаров.
   */
  public static function catalog(string $vid = 'site_commerce_catalog') {
    return ProductCatalogController::catalog($vid);
  }

  /**
   * Products view by display_id.
   */
  public function catalogByView(string $display_id, string $name = NULL, string $summary = NULL) {
    $build[] = views_embed_view(
      'site_commerce_product_products_catalog',
      $display_id,
      NULL,
      '',
      [
        'name' => $name,
        'summary' => $summary,
        'child' => FALSE,
        'title' => TRUE,
        '#pager' => ['view' => FALSE]
      ],
    );

    return $build;
  }

  /**
   * Перечень товаров по выбранной категории.
   */
  public function catalogByTerm(int $tid) {
    $build = [];

    // Search filter values ​​for the current user.
    $taxonomy_search = $this->tempStore->get('taxonomy_search');
    if (empty($taxonomy_search['tids'])) {
      $taxonomy_search['tids'][] = $tid;
    }

    /** @var \Drupal\taxonomy\TermStorageInterface $term_storage */
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

    foreach ($taxonomy_search['tids'] as $tid) {
      $entity = $term_storage->load($tid);
      if ($entity->field_view->value) {
        continue;
      }

      // Если термин таксономии имеет вложенные категории.
      $terms = $term_storage->loadTree($entity->bundle(), $entity->id(), NULL, TRUE);
      if ($terms) {
        foreach ($terms as $term) {
          if ($term->field_view->value) {
            $build[] = views_embed_view(
              'site_commerce_product_products_catalog',
              'products_in_catalog_preview',
              $term->id(),
              '',
              ['child' => TRUE, 'title' => FALSE]
            );
          }
        }
      } else {
        $build = views_embed_view(
          'site_commerce_product_products_catalog',
          'products_in_catalog',
          $entity->id(),
          '',
          ['child' => FALSE, 'title' => TRUE, '#pager' => ['view' => FALSE]],
        );
      }
    }

    return $build;
  }

  /**
   * Корневые категории товаров в виде списка.
   */
  public static function catalogList(string $vid = "site_commerce_catalog") {
    return ProductCatalogController::catalogList($vid);
  }

  /**
   * Дочерние категории товаров в виде списка.
   */
  public static function catalogChildrenList(int $tid, int $max_depth = NULL) {
    return ProductCatalogController::catalogChildrenList($tid, $max_depth);
  }

  /**
   * Форма для загрузки товаров по выбранным категориям.
   */
  public static function loadByCategoriesForm() {
    $form = \Drupal::formBuilder()->getForm('Drupal\site_commerce_product\Form\ProductsLoadByCategoriesForm');
    return $form;
  }

  /**
   * Редактор цен по выбранной категории товаров.
   */
  public static function priceEditor(int $tid) {
    return ProductController::priceEditor($tid);
  }

  /**
   * Возвращает строковое значение статуса товара на складе.
   */
  public static function statusStock($site_commerce_product) {
    return ProductController::productStatusStock($site_commerce_product);
  }

  /**
   * Возвращает кнопку на скачивание прайс листа по выбранной категории.
   */
  public static function exportPrice(int $tid) {
    return ProductController::exportPrice($tid);
  }
}
