<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Provides an interface defining a taxonomy term entity.
 */
interface ProductAttributeInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the term description.
   *
   * @return string
   *   The term description.
   */
  public function getDescription();

  /**
   * Sets the term description.
   *
   * @param string $description
   *   The term description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the term name.
   *
   * @return string
   *   The term name.
   */
  public function getName();

  /**
   * Sets the term name.
   *
   * @param string $name
   *   The term name.
   *
   * @return $this
   */
  public function setName($name);

  /**
   * Gets the term weight.
   *
   * @return int
   *   The term weight.
   */
  public function getWeight();

  /**
   * Sets the term weight.
   *
   * @param int $weight
   *   The term weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Check if category set to the entity.
   *
   * @param \Drupal\taxonomy\TermInterface; $entity
   *   The taxonomy term entity.
   *
   * @return bool TRUE || FALSE
   *   TRUE if category exist in the entity.
   */
  public function isCategoryExist(TermInterface $entity);

  /**
   * Sets the attribute category.
   *
   * @param \Drupal\taxonomy\TermInterface; $entity
   *   The taxonomy term entity.
   *
   * @return $this
   */
  public function setCategory(TermInterface $entity);

  /**
   * Sets the parent attribute of this attribute.
   *
   * @param \Drupal\site_commerce_product\ProductAttributeInterface $entity
   *   The attribute entity.
   *
   * @return $this
   */
  public function setParent(ProductAttributeInterface $entity);

  /**
   * Check if attribute is root in the own group.
   *
   * @param \Drupal\taxonomy\TermInterface; $entity
   *   The taxonomy term entity.
   *
   * @return bool TRUE || FALSE
   *   If TRUE the attribute is root in the own group.
   */
  public function isRoot();

  /**
   * Sets the attribute as root in the own group.
   *
   * @return $this
   */
  public function setRoot();

  /**
   * Sets the attribute as not root in the own group.
   *
   * @return $this
   */
  public function unsetRoot();

  /**
   * Gets additional data of attribute.
   *
   * @return array
   */
  public function getData();

  /**
   * Sets additional data of attribute.
   *
   * @param array $data
   *   The additional data of attribute.
   *
   * @return \Drupal\site_commerce_product\ProductAttributeInterface
   *   The called Transaction entity.
   */
  public function setData(array $data);

}
