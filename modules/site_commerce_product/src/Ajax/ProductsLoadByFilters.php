<?php

namespace Drupal\site_commerce_product\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Load products by filters.
 *
 * This command is implemented in Drupal.AjaxCommands.prototype.productsLoadByFilters.
 */
class ProductsLoadByFilters implements CommandInterface {

  /**
   * ID of the HTML element to insert data in.
   *
   * @var string
   */
  protected $id;

  /**
   * Assigned attributes id.
   *
   * @var array
   */
  protected $aids;

  /**
   * Constructs a \Drupal\site_commerce_product\Ajax\ProductsLoadByFilters object.
   */
  public function __construct(string $id, array $aids) {
    $this->id = $id;
    $this->aids = $aids;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'productsLoadByFilters',
      'id' => $this->id,
      'tids' => $this->tids,
    ];
  }

}
