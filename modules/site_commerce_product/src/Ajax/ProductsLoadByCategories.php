<?php

namespace Drupal\site_commerce_product\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Load products by categories.
 *
 * This command is implemented in Drupal.AjaxCommands.prototype.productsLoadByCategories.
 */
class ProductsLoadByCategories implements CommandInterface {

  /**
   * Assigned data to load products.
   *
   * @var array
   */
  protected $data;

  /**
   * Constructs a \Drupal\site_commerce_product\Ajax\ProductsLoadByCategories object.
   *
   * @param array $data
   *   $data['tids'] - Assigned taxonomy terms id of products categoties.
   *   $data['id'] - ID of the HTML element to insert data in.
   */
  public function __construct(array $data) {
    $this->data = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'productsLoadByCategories',
      'id' => $this->data['id'],
      'tids' => $this->data['tids'],
    ];
  }

}
