<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\site_commerce_product\ProductAttributeGroupInterface;
use Drupal\site_commerce_product\ProductAttributeInterface;
use Drupal\Core\Entity\Sql\TableMappingInterface;
use Drupal\site_commerce_product\ProductInterface;
use Drupal\site_commerce_product\Entity\Product;

/**
 * Defines a storage class for attributes.
 */
class ProductAttributeStorage extends SqlContentEntityStorage implements ProductAttributeStorageInterface {

  /**
   * Array of attribute parents keyed by group ID and child attribute ID.
   *
   * @var array
   */
  protected $treeParents = [];

  /**
   * Array of attribute ancestors keyed by group ID and parent attribute ID.
   *
   * @var array
   */
  protected $treeChildren = [];

  /**
   * Array of attributes in a tree keyed by group ID and attribute ID.
   *
   * @var array
   */
  protected $treeAttributes = [];

  /**
   * Array of loaded trees keyed by a cache id matching tree arguments.
   *
   * @var array
   */
  protected $trees = [];

  /**
   * The type of hierarchy allowed within a group.
   *
   * Possible values:
   * - ProductAttributeGroupInterface::HIERARCHY_DISABLED: No parents.
   * - ProductAttributeGroupInterface::HIERARCHY_SINGLE: Single parent.
   * - ProductAttributeGroupInterface::HIERARCHY_MULTIPLE: Multiple parents.
   *
   * @var int[]
   *   An array of one the possible values above, keyed by group ID.
   */
  protected $groupHierarchyType;

  /**
   * {@inheritdoc}
   */
  public function deleteProductLink(array $attributes) {
    /** @var \Drupal\site_commerce_product\ProductAttributeInterface $attribute */
    foreach ($attributes as $attribute) {
      $query = $this->entityTypeManager->getStorage('site_commerce_product')->getQuery();
      $query->condition('attribute.target_id', $attribute->id());
      $query->accessCheck(TRUE);
      $product_ids = $query->execute();
      if (!empty($product_ids)) {
        $products = Product::loadMultiple($product_ids);
        foreach ($products as $product) {
          $referenced_items = $product->get('attribute')->getValue();
          $index_to_remove = array_search(['target_id' => $attribute->id()], $referenced_items);
          if ($index_to_remove !== FALSE) {
            $product->get('attribute')->removeItem($index_to_remove);
            $product->save();
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadParents($aid) {
    $attributes = [];
    /** @var \Drupal\site_commerce_product\ProductAttributeInterface $attribute */
    if ($aid && $attribute = $this->load($aid)) {
      foreach ($this->getParents($attribute) as $id => $parent) {
        if (!empty($id)) {
          $attributes[$id] = $parent;
        }
      }
    }

    return $attributes;
  }

  /**
   * Returns a list of parents of this attribute.
   *
   * @return \Drupal\site_commerce_product\ProductAttributeInterface[]
   *   The parent entities keyed by ID. If this attribute has a
   *   <root> parent, that item is keyed with 0 and will have NULL as value.
   */
  protected function getParents(ProductAttributeInterface $attribute) {
    $parents = $ids = [];
    foreach ($attribute->get('parent') as $item) {
      if ($item->target_id == 0) {
        // The <root> parent.
        $parents[0] = NULL;
        continue;
      }
      $ids[] = $item->target_id;
    }

    $loaded_parents = [];

    if ($ids) {
      $query = \Drupal::entityQuery('site_commerce_attribute')->condition('aid', $ids, 'IN');
      $query->accessCheck(TRUE);
      $loaded_parents = $this->loadMultiple($query->execute());
    }

    return $parents + $loaded_parents;
  }

  /**
   * {@inheritdoc}
   */
  public function loadChildren($aid) {
    /** @var \Drupal\site_commerce_product\ProductAttributeInterface $attribute */
    return $attribute = $this->load($aid) ? $this->getChildren($attribute) : [];
  }

  /**
   * Returns all children attributes of this attribute.
   *
   * @return \Drupal\site_commerce_product\ProductAttributeInterface []
   *   A list of children attribute entities keyed by attribute ID.
   */
  public function getChildren(ProductAttributeInterface $attribute) {
    $query = \Drupal::entityQuery('site_commerce_attribute')->condition('parent', $attribute->id());
    $query->accessCheck(TRUE);
    return $this->loadMultiple($query->execute());
  }

  /**
   * {@inheritdoc}
   */
  public function loadTree($gid, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {
    $cache_key = implode(':', func_get_args());
    if (!isset($this->trees[$cache_key])) {
      // We cache trees, so it's not CPU-intensive to call on a attribute and its
      // children, too.
      if (!isset($this->treeChildren[$gid])) {
        $this->treeChildren[$gid] = [];
        $this->treeParents[$gid] = [];
        $this->treeAttributes[$gid] = [];
        $query = $this->database->select($this->getDataTable(), 't');
        $query->join('site_commerce_attribute__parent', 'p', 't.aid = p.entity_id');
        $query->addExpression('parent_target_id', 'parent');
        $result = $query
          ->addTag('site_commerce_attribute_access')
          ->fields('t')
          ->condition('t.gid', $gid)
          ->condition('t.default_langcode', 1)
          ->orderBy('t.weight')
          ->orderBy('t.name')
          ->execute();
        foreach ($result as $attribute) {
          $this->treeChildren[$gid][$attribute->parent][] = $attribute->aid;
          $this->treeParents[$gid][$attribute->aid][] = $attribute->parent;
          $this->treeAttributes[$gid][$attribute->aid] = $attribute;
        }
      }

      // Load full entities, if necessary. The entity controller statically
      // caches the results.
      $attribute_entities = [];
      if ($load_entities) {
        $attribute_entities = $this->loadMultiple(array_keys($this->treeAttributes[$gid]));
      }

      $max_depth = (!isset($max_depth)) ? count($this->treeChildren[$gid]) : $max_depth;
      $tree = [];

      // Keeps track of the parents we have to process, the last entry is used
      // for the next processing step.
      $process_parents = [];
      $process_parents[] = $parent;

      // Loops over the parent attributes and adds its children to the tree array.
      // Uses a loop instead of a recursion, because it's more efficient.
      while (count($process_parents)) {
        $parent = array_pop($process_parents);
        // The number of parents deattributeines the current depth.
        $depth = count($process_parents);
        if ($max_depth > $depth && !empty($this->treeChildren[$gid][$parent])) {
          $has_children = FALSE;
          $child = current($this->treeChildren[$gid][$parent]);
          do {
            if (empty($child)) {
              break;
            }
            $attribute = $load_entities ? $attribute_entities[$child] : $this->treeAttributes[$gid][$child];
            if (isset($this->treeParents[$gid][$load_entities ? $attribute->id() : $attribute->aid])) {
              // Clone the attribute so that the depth attribute remains correct
              // in the event of multiple parents.
              $attribute = clone $attribute;
            }
            $attribute->depth = $depth;
            if (!$load_entities) {
              unset($attribute->parent);
            }
            $aid = $load_entities ? $attribute->id() : $attribute->aid;
            $attribute->parents = $this->treeParents[$gid][$aid];
            $tree[] = $attribute;
            if (!empty($this->treeChildren[$gid][$aid])) {
              $has_children = TRUE;

              // We have to continue with this parent later.
              $process_parents[] = $parent;
              // Use the current attribute as parent for the next iteration.
              $process_parents[] = $aid;

              // Reset pointers for child lists because we step in there more
              // often with multi parents.
              reset($this->treeChildren[$gid][$aid]);
              // Move pointer so that we get the correct attribute the next time.
              next($this->treeChildren[$gid][$parent]);
              break;
            }
          } while ($child = next($this->treeChildren[$gid][$parent]));

          if (!$has_children) {
            // We processed all attributes in this hierarchy-level, reset pointer
            // so that this function works the next time it gets called.
            reset($this->treeChildren[$gid][$parent]);
          }
        }
      }
      $this->trees[$cache_key] = $tree;
    }
    return $this->trees[$cache_key];
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupHierarchyType($gid) {
    // Return early if we already computed this value.
    if (isset($this->groupHierarchyType[$gid])) {
      return $this->groupHierarchyType[$gid];
    }

    $parent_field_storage = $this->entityFieldManager->getFieldStorageDefinitions($this->entityTypeId)['parent'];
    $table_mapping = $this->getTableMapping();

    $target_id_column = $table_mapping->getFieldColumnName($parent_field_storage, 'target_id');
    $delta_column = $table_mapping->getFieldColumnName($parent_field_storage, TableMappingInterface::DELTA);

    $query = $this->database->select($table_mapping->getFieldTableName('parent'), 'p');
    $query->addExpression("MAX($target_id_column)", 'max_parent_id');
    $query->addExpression("MAX($delta_column)", 'max_delta');
    $query->condition('bundle', $gid);

    $result = $query->execute()->fetchAll();

    // If all the attributes have the same parent, the parent can only be root (0).
    if ((int) $result[0]->max_parent_id === 0) {
      $this->groupHierarchyType[$gid] = ProductAttributeGroupInterface::HIERARCHY_DISABLED;
    }
    // If no attribute has a delta higher than 0, no attribute has multiple parents.
    elseif ((int) $result[0]->max_delta === 0) {
      $this->groupHierarchyType[$gid] = ProductAttributeGroupInterface::HIERARCHY_SINGLE;
    }
    else {
      $this->groupHierarchyType[$gid] = ProductAttributeGroupInterface::HIERARCHY_MULTIPLE;
    }

    return $this->groupHierarchyType[$gid];
  }

  /**
   * Returns all children attributes of this attribute.
   *
   * @return \Drupal\site_commerce_product\ProductAttributeInterface[]
   *   A list of children attribute entities keyed by attribute ID.
   */
  public function existLinkWithProduct(ProductAttributeInterface $attribute, ProductInterface $entity) {
    $query = \Drupal::entityQuery('site_commerce_attribute');
    $query->accessCheck(TRUE);
    $query->condition('aid', $attribute->id());
    $query->condition('product', $entity->id());
    $result = $query->execute();

    if ($result) {
      return TRUE;
    }

    return FALSE;
  }

}
