<?php

namespace Drupal\site_commerce_product\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\site_commerce_product\ProductTypeInterface;

/**
 * Defines the Product type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "site_commerce_product_type",
 *   label = @Translation("Product type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\site_commerce_product\ProductTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\site_commerce_product\Form\ProductTypeForm",
 *       "edit" = "Drupal\site_commerce_product\Form\ProductTypeForm",
 *       "duplicate" = "Drupal\site_commerce_product\Form\ProductTypeDuplicateForm",
 *       "delete" = "Drupal\site_commerce_product\Form\ProductTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_commerce_product\ProductTypeHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer site_commerce_product",
 *   config_prefix = "site_commerce_product_type",
 *   bundle_of = "site_commerce_product",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   links = {
 *     "canonical" = "/admin/site-commerce/config/products/products-types/{site_commerce_product_type}",
 *     "add-form" = "/admin/site-commerce/config/products/products-types/add",
 *     "edit-form" = "/admin/site-commerce/config/products/products-types/{site_commerce_product_type}/edit",
 *     "duplicate-form" = "/admin/site-commerce/config/products/products-types/{site_commerce_product_type}/duplicate",
 *     "delete-form" = "/admin/site-commerce/config/products/products-types/{site_commerce_product_type}/delete",
 *     "collection" = "/admin/site-commerce/config/products/products-types",
 *   },
 * )
 */
class ProductType extends ConfigEntityBundleBase implements ProductTypeInterface {

  /**
   * The machine name of this Product entity type.
   *
   * @var string
   *
   */
  protected $id;

  /**
   * The human-readable name of the Product entity type.
   *
   * @var string
   *
   */
  protected $label;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOriginalId($id) {
    // Do not call the parent method since that would mark this entity as no
    // longer new. Unlike content entities, new configuration entities have an
    // ID.
    // @todo https://www.drupal.org/node/2478811 Document the entity life cycle
    //   and the differences between config and content.
    $this->originalId = $id;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function createDuplicateType($new_id = NULL) {
    $duplicate = parent::createDuplicate();
    if (!empty($new_id)) {
      $duplicate->id = $new_id;
      $duplicate->label = "Duplicate. " . $duplicate->label;
    }

    return $duplicate;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    if ($update) {
      // Clear the cached field definitions as some settings affect the field definitions.
      $this->entityTypeManager()->clearCachedDefinitions();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Clear the site_commerce_product type cache to reflect the removal.
    $storage->resetCache(array_keys($entities));
  }
}
