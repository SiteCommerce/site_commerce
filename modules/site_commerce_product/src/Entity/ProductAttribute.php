<?php

namespace Drupal\site_commerce_product\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\site_commerce_product\ProductAttributeInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Defines the attribute entity.
 *
 * @ContentEntityType(
 *   id = "site_commerce_attribute",
 *   label = @Translation("Attribute of product"),
 *   label_collection = @Translation("Attributes of product"),
 *   label_singular = @Translation("attribute of product"),
 *   label_plural = @Translation("attributes of product"),
 *   label_count = @PluralTranslation(
 *     singular = "@count attribute of product",
 *     plural = "@count attributes of product",
 *   ),
 *   bundle_label = @Translation("Attributes group"),
 *   handlers = {
 *     "storage" = "Drupal\site_commerce_product\ProductAttributeStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "access" = "Drupal\site_commerce_product\ProductAttributeAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\site_commerce_product\Form\ProductAttributeForm",
 *       "delete" = "Drupal\site_commerce_product\Form\ProductAttributeDeleteForm"
 *     },
 *   },
 *   admin_permission = "administer site_commerce_attribute",
 *   base_table = "site_commerce_attribute",
 *   data_table = "site_commerce_attribute_field_data",
 *   translatable = TRUE,
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "aid",
 *     "bundle" = "gid",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *     "root" = "root",
 *   },
 *   bundle_entity_type = "site_commerce_attribute_group",
 *   field_ui_base_route = "entity.site_commerce_attribute_group.overview_form",
 *   common_reference_target = TRUE,
 *   links = {
 *     "canonical" = "/product-attribute/{site_commerce_attribute}",
 *     "add-form" = "/admin/site-commerce/products/attributes-groups/attribute/{site_commerce_attribute}/add",
 *     "edit-form" = "/admin/site-commerce/products/attributes-groups/attribute/{site_commerce_attribute}/edit",
 *     "delete-form" = "/admin/site-commerce/products/attributes-groups/attribute/{site_commerce_attribute}/delete",
 *   },
 *   permission_granularity = "bundle",
 * )
 */
class ProductAttribute extends ContentEntityBase implements ProductAttributeInterface {

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    /** @var \Drupal\site_commerce_product\Entity\ProductAttributeStorageInterface $storage */
    parent::postDelete($storage, $entities);

    // See if any of the attribute's children are about to be become orphans.
    $orphans = [];

    // Перечень удаленных атрибутов с учетом дочерних.
    $deleted_attributes = [];

    /** @var \Drupal\site_commerce_product\ProductAttributeInterface $attribute */
    foreach ($entities as $aid => $attribute) {
      $deleted_attributes[] = $attribute;

      if ($children = $storage->getChildren($attribute)) {
        /** @var \Drupal\site_commerce_product\ProductAttributeInterface $child */
        foreach ($children as $child) {
          $parent = $child->get('parent');

          // Update child parents item list.
          $parent->filter(function ($item) use ($aid) {
            return $item->target_id != $aid;
          });

          // If the attribute has multiple parents, we don't delete it.
          if ($parent->count()) {
            $child->save();
          } else {
            $orphans[] = $child;
            $deleted_attributes[] = $child;
          }
        }
      }
    }

    if (!empty($orphans)) {
      $storage->delete($orphans);
    }

    // Удаляет привязку атрибутов к товарам.
    $storage->deleteProductLink($deleted_attributes);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    if (!$this->get('parent')->count()) {
      $this->parent->target_id = 0;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  /**
   * Returns the timestamp of the last entity change across all translations.
   *
   * @return int
   *   The timestamp of the last entity save operation across all
   *   translations.
   */
  public function getChangedTimeAcrossTranslations() {
    $changed = $this->getUntranslated()->getChangedTime();
    foreach ($this->getTranslationLanguages(FALSE) as $language) {
      $translation_changed = $this->getTranslation($language->getId())->getChangedTime();
      $changed = max($translation_changed, $changed);
    }
    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('published');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished() {
    $this->set('status', TRUE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUnpublished() {
    $this->set('status', FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->label();
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isCategoryExist(TermInterface $entity) {
    $categories = $this->get('category')->getValue();
    if (\is_array($categories)) {
      $key = array_search(['target_id' => $entity->id()], $categories);
      if ($key !== FALSE) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setCategory(TermInterface $entity) {
    /** @var \Drupal\taxonomy\TermStorageInterface $term_storage */
    $term_storage = $this->entityTypeManager()->getStorage('taxonomy_term');

    if (!$this->isCategoryExist($entity)) {
      $this->category[] = $entity->id();
    }

    $parents = $term_storage->loadAllParents($entity->id());
    foreach ($parents as $parent) {
      if (!$this->isCategoryExist($parent)) {
        $this->category[] = $parent->id();
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setParent(ProductAttributeInterface $entity) {
    $parents = $this->get('parent')->getValue();

    // Если привязка атрибута товаров отсутствует.
    $key = array_search(['target_id' => $entity->id()], $parents);
    if ($key === FALSE) {
      $this->parent[] = $entity->id();
    }

    // Удаляем привязку к нулевому индексу.
    // Нулевой индекс проставляется по умолчанию, если нет приваязки
    // при создании сущности.
    if ($this->get('parent')->count() > 1) {
      $key = array_search(['target_id' => 0], $parents);
      $key = array_search(0, array_column($parents, 'target_id'));
      if ($key !== FALSE) {
        $this->get('parent')->removeItem($key);
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isRoot() {
    return (bool) $this->getEntityKey('root');
  }

  /**
   * {@inheritdoc}
   */
  public function setRoot() {
    $this->set('root', TRUE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetRoot() {
    $this->set('root', FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $data = $this->get('data')->getValue();
    return reset($data);
  }

  /**
   * {@inheritdoc}
   */
  public function setData(array $data) {
    $this->set('data', $data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['gid']->setLabel(t('Group name'))
      ->setDescription(t('The attributes group to which the attribute is assigned.'))
      ->setRequired(TRUE);

    $fields['category'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Attribute category'))
      ->setDescription(t('The category of this attribute.'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setTranslatable(TRUE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Attribute parent'))
      ->setDescription(t('The parent of this attribute.'))
      ->setSetting('target_type', 'site_commerce_attribute')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield'
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea'
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['image'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Image'))
      ->setDescription(t('Image of the product variant when selecting an attribute.'))
      ->setSetting('target_type', 'media')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'media_library_widget'
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Type of attribute'))
      ->setSetting('allowed_values_function', ['\Drupal\site_commerce_product\Entity\ProductAttribute', 'getTypes'])
      ->setDisplayOptions('form', [
        'type' => 'options_select'
      ])
      ->setDisplayConfigurable('form', FALSE);

    $fields['value'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Value'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield'
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['quantity_unit'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Unit of quantity measurement'))
      ->setSetting('allowed_values_function', ['\Drupal\site_commerce_product\Entity\ProductAttribute', 'getQuantityUnits'])
      ->setDisplayOptions('form', [
        'type' => 'options_select'
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['price'] = BaseFieldDefinition::create('site_commerce_price_group')
      ->setLabel(t('Price'))
      ->setDisplayOptions('form', [
        'type' => 'site_commerce_price_group_default'
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['root'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('The attribute is root within the group'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox'
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow displaying in the product card'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox'
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['filter'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow displaying as a filter when searching'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox'
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['page'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow creating a page with a list of products'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox'
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'));

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this attribute in relation to other attributes.'))
      ->setDefaultValue(0)
      ->setRequired(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the attribute was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields['parent'] = clone $base_field_definitions['parent'];
    return $fields;
  }

  /**
   * Gets the allowed types of attributes.
   * TODO: Support for attribute types will be implemented in future versions.
   *
   * @return array
   *   The allowed values.
   */
  public static function getTypes() {
    return [];
  }

  /**
   * Gets the allowed quantity units.
   *
   * @return array
   *   The allowed values.
   */
  public static function getQuantityUnits() {
    return getUnitMeasurement();
  }

}
