<?php

namespace Drupal\site_commerce_product\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\kvantstudio\BundleFieldDefinition;
use Drupal\site_commerce_product\ProductAttributeInterface;
use Drupal\site_commerce_product\ProductInterface;
use Drupal\user\EntityOwnerTrait;
use Drupal\kvantstudio\Formatter;
use Drupal\kvantstudio\Validator;

/**
 * Defines the Product entity.
 *
 * @ingroup site_commerce_product
 *
 * @ContentEntityType(
 *   id = "site_commerce_product",
 *   label = @Translation("Product"),
 *   label_singular = @Translation("product"),
 *   label_plural = @Translation("products"),
 *   label_count = @PluralTranslation(
 *     singular = "@count product",
 *     plural = "@count products"
 *   ),
 *   bundle_label = @Translation("Product type"),
 *   handlers = {
 *     "storage" = "Drupal\site_commerce_product\ProductStorage",
 *     "view_builder" = "Drupal\site_commerce_product\ProductViewBuilder",
 *     "list_builder" = "Drupal\site_commerce_product\ProductListBuilder",
 *     "views_data" = "Drupal\site_commerce_product\ProductViewsData",
 *     "translation" = "Drupal\site_commerce_product\ProductTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\site_commerce_product\Form\ProductForm",
 *       "add" = "Drupal\site_commerce_product\Form\ProductForm",
 *       "edit" = "Drupal\site_commerce_product\Form\ProductForm",
 *       "duplicate" = "Drupal\site_commerce_product\Form\ProductDuplicateForm",
 *       "delete" = "Drupal\site_commerce_product\Form\ProductDeleteForm",
 *     },
 *     "access" = "Drupal\site_commerce_product\ProductAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\site_commerce_product\ProductHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "site_commerce_product",
 *   data_table = "site_commerce_product_field_data",
 *   permission_granularity = "bundle",
 *   admin_permission = "administer site_commerce_product",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "product_id",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/product/{site_commerce_product}",
 *     "add-page" = "/admin/site-commerce/products/add",
 *     "add-form" = "/admin/site-commerce/products/add/{site_commerce_product_type}",
 *     "edit-form" = "/admin/site-commerce/products/{site_commerce_product}/edit",
 *     "duplicate-form" = "/admin/site-commerce/products/{site_commerce_product}/duplicate",
 *     "delete-form" = "/admin/site-commerce/products/{site_commerce_product}/delete",
 *     "collection" = "/admin/site-commerce/products",
 *   },
 *   bundle_entity_type = "site_commerce_product_type",
 *   field_ui_base_route = "entity.site_commerce_product_type.edit_form",
 * )
 */
class Product extends ContentEntityBase implements ProductInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    if (\Drupal::moduleHandler()->moduleExists('search')) {
      foreach ($entities as $entity) {
        \Drupal::service('search.index')->clear('site_commerce_product', $entity->id());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  /**
   * Returns the timestamp of the last entity change across all translations.
   *
   * @return int
   *   The timestamp of the last entity save operation across all
   *   translations.
   */
  public function getChangedTimeAcrossTranslations() {
    $changed = $this->getUntranslated()->getChangedTime();
    foreach ($this->getTranslationLanguages(FALSE) as $language) {
      $translation_changed = $this->getTranslation($language->getId())->getChangedTime();
      $changed = max($translation_changed, $changed);
    }
    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public function getStockAllow() {
    return $this->get('settings')->quantity_stock_allow;
  }

  /**
   * {@inheritdoc}
   */
  public function getStockValue() {
    return $this->get('settings')->quantity_stock_value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStockValue(int $value) {
    $this->settings->quantity_stock_value = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuantityUnit() {
    return $this->get('settings')->quantity_unit;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuantityUnit(string $unit) {
    $this->settings->quantity_unit = $unit;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuantityOrderMin() {
    return $this->get('settings')->quantity_order_min;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrices(?string $group = 'retail'): array {
    // Определяем ценовую группу для текущего пользователя.
    // TODO: создание ценовых групп реализовать через конфигурационную сущность.
    if (!$group) {
      $group = site_commerce_product_default_price_group();
    }

    // Стоимость для ценовой группы по умолчанию для пользователя.
    $data[$group] = [
      'prefix' => '',
      'suffix' => '',
      'number_from' => 0,
      'number' => 0,
      'number_sale' => 0,
      'currency_code' => '',
    ];

    // Если задана стоимость сущности товара.
    // Выполняем обновление параметров в ценовой группе.
    if ($items = $this->get('price')->getValue()) {
      foreach ($items as $delta => $item) {
        $data[$item['group']] = [
          'group' => $item['group'],
          'prefix' => $item['prefix'],
          'suffix' => $item['suffix'],
          'number_from' => (float) $item['number_from'],
          'number' => (float) $item['number'],
          'number_sale' => (float) $item['number_sale'],
          'currency_code' => $item['currency_code'],
        ];
      }
    }

    // Если нужно вывести все найденные ценовые группы.
    if ($group == 'all') {
      return $data;
    }

    return $data[$group];
  }

  /**
   * {@inheritdoc}
   */
  public function formattedPrices(array $items, int $decimals = 2, string $dec_point = '.', string $thousands_sep = ' '): array {
    $data = [];
    if (count($items, COUNT_RECURSIVE) > 7) {
      foreach ($items as $item) {
        $data[$item['group']] = [
          'group' => $item['group'],
          'prefix' => $item['prefix'],
          'suffix' => $item['suffix'],
          'number_from' => Formatter::price($item['number_from']),
          'number' => Formatter::price($item['number']),
          'number_sale' => Formatter::price($item['number_sale']),
          'currency_code' => $item['currency_code'],
        ];
      }
    } else {
      foreach ($items as $item) {
        $data = [
          'group' => $item['group'],
          'prefix' => $item['prefix'],
          'suffix' => $item['suffix'],
          'number_from' => Formatter::price($item['number_from']),
          'number' => Formatter::price($item['number']),
          'number_sale' => Formatter::price($item['number_sale']),
          'currency_code' => $item['currency_code'],
        ];
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function setPrice(array $values, string $group) {
    $groups = $this->get('price')->getValue();
    if (count($groups)) {
      foreach ($groups as $delta => $item) {
        if ($item['group'] == $group) {
          $data = array_replace($item, $values);
          $this->price[$delta] = $data;
        }
      }
    } else {
      $this->price[] = $values;
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPaidPriceExist(string $group = 'retail'): bool {
    $item = self::getPrices($group);
    if (Validator::comparingNumbers($item['number']) || Validator::comparingNumbers($item['number_sale'])) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaidPrice(string $group = 'retail'): array {
    $data = ['number' => 0, 'currency_code' => ''];

    $item = self::getPrices($group);
    if (Validator::comparingNumbers($item['number_sale'])) {
      $data = ['number' => $item['number_sale'], 'currency_code' => $item['currency_code']];
      return $data;
    }

    if (Validator::comparingNumbers($item['number'])) {
      $data = ['number' => $item['number'], 'currency_code' => $item['currency_code']];
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight(int $weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isAttributeExist(ProductAttributeInterface $entity) {
    $attributes = $this->get('attribute')->getValue();
    if (is_array($attributes)) {
      $key = array_search(['target_id' => $entity->id()], $attributes);
      if ($key !== FALSE) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setAttribute(ProductAttributeInterface $entity) {
    if (!$this->isAttributeExist($entity)) {
      $this->attribute[] = $entity->id();
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategory() {
    return $this->get('category')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoryId() {
    return (int) $this->get('category')->value;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['attribute'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Attribute'))
      ->setDescription(t('The attribute ID.'))
      ->setSetting('target_type', 'site_commerce_attribute')
      ->setTranslatable(TRUE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Base product'))
      ->setDescription(t('A product that includes the current product and is a variant of it.'))
      ->setSetting('target_type', 'site_commerce_product')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Manager'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('target_type', 'user')
      ->setDisplayConfigurable('form', TRUE);

    $fields['category'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Main category'))
      ->setDescription(t('The main category of the product in the catalog. The link to the product depends on the choice of category.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'site_commerce_catalog' => 'site_commerce_catalog'
        ]
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select'
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['additional_category'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Additional categories'))
      ->setDescription(t('The product will additionally be displayed in the selected categories of the catalog.'))
      ->setTranslatable(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'site_commerce_catalog' => 'site_commerce_catalog'
        ]
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['price'] = BaseFieldDefinition::create('site_commerce_price_group')
      ->setLabel(t('Price'))
      ->setDisplayOptions('form', [
        'type' => 'site_commerce_price_group_default',
        'settings' => [
          'display_label' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'site_commerce_price_group_default',
        'settings' => [
          'display_label' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['settings'] = BundleFieldDefinition::create('site_commerce_product_settings')
      ->setLabel(t('Display settings'))
      ->setDisplayOptions('form', [
        'type' => 'site_commerce_product_settings_default',
        'settings' => [
          'display_label' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setCardinality(1);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Product code'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['code_manufacturer'] = BaseFieldDefinition::create('string')
      ->setLabel(t("Product code according to the manufacturer's catalog"))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['code_external'] = BaseFieldDefinition::create('string')
      ->setLabel(t("Identifier in external database"))
      ->setDisplayConfigurable('form', TRUE);

    $fields['search_index'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t("Search index"))
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow displaying a product'))
      ->setDefaultValue(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['status_catalog'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow displaying a product in the catalog'))
      ->setDefaultValue(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the product was created.'))
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the product was last edited.'));

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('Weight of product in catalog.'))
      ->setDefaultValue(0);

    return $fields;
  }
}
