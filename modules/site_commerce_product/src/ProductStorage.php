<?php

namespace Drupal\site_commerce_product;

use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines a storage class for products.
 */
class ProductStorage extends SqlContentEntityStorage implements ProductStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function countProductsByCategoriesAndAttributes(array $tids, array $attribute_ids): int {
    $query = $this->database->select('site_commerce_product__attribute', 'n');
    $query->addExpression('COUNT(DISTINCT n.attribute_target_id)', 'counts');

    // If categories is not empty.
    if ($tids) {
      $query->join('site_commerce_product_field_data', 'c', 'c.entity_id=n.entity_id');
      $query->condition('c.category', $tids, 'IN');
    }

    $group = $query->orConditionGroup();
    $matches = 0;
    foreach ($attribute_ids as $attribute_id) {
      if ($attribute_id) {
        $group->condition('n.attribute_target_id', $attribute_id);
        $matches++;
      }
    }

    if ($matches) {
      $query->condition($group);
      $query->having('counts = :matches', [':matches' => $matches]);
    }

    $query->groupBy('n.entity_id');

    return (int) $query->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function countProductsByCategory(?int $tid = NULL, bool $child = TRUE): int {
    $count = 0;

    $query = $this->database->select('site_commerce_product_field_data', 'n');
    $query->addExpression('COUNT(n.product_id)');
    $query->condition('n.status', 1);
    $query->condition('n.status_catalog', 1);

    $tids = [];
    if ($term = Term::load($tid)) {
      // If it is allowed to display products by category in the catalog.
      if ($term->isPublished() && (bool) $term->field_view->value) {
        $tids[] = $term->id();
      }

      if ($child) {
        /** @var \Drupal\taxonomy\TermStorageInterface $storage */
        $storage = $this->entityTypeManager->getStorage('taxonomy_term');
        $terms = $storage->loadTree($term->bundle(), $term->id(), NULL, TRUE);
        foreach ($terms as $term) {
          // If it is allowed to display products by category in the catalog.
          if ($term->isPublished() && (bool) $term->field_view->value) {
            $tids[] = $term->id();
          }
        }
      }

      if (count($tids)) {
        $query->condition('n.category', $tids, 'IN');
        $count = (int) $query->execute()->fetchField();
      }
    }

    return $count;
  }
}
