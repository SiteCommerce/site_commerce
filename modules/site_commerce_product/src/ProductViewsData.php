<?php

namespace Drupal\site_commerce_product;

use Symfony\Component\DependencyInjection\Container;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Product entities.
 */
class ProductViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // TODO: added before fix Drupal core https://www.drupal.org/project/drupal/issues/2509986
    $data['site_commerce_product__price']['price_number_from']['filter']['id'] = 'numeric';
    $data['site_commerce_product__price']['price_number']['filter']['id'] = 'numeric';
    $data['site_commerce_product__price']['price_number_sale']['filter']['id'] = 'numeric';

    return $data;
  }

}
