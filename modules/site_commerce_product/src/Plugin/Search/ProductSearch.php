<?php

namespace Drupal\site_commerce_product\Plugin\Search;

use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\Config;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;
use Drupal\search\Plugin\SearchIndexingInterface;
use Drupal\search\Plugin\SearchPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Html;

/**
 * Executes a keyword search for products.
 *
 * @SearchPlugin(
 *   id = "site_commerce_product",
 *   title = @Translation("Products")
 * )
 */
class ProductSearch extends SearchPluginBase implements AccessibleInterface, SearchIndexingInterface {

  /**
   * A database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * A config object for 'search.settings'.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $search_settings;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database, Config $search_settings, array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
    $this->search_settings = $search_settings;
  }

  /**
   * {@inheritdoc}
   */
  static public function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('database'),
      $container->get('config.factory')->get('search.settings'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation = 'view', AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::neutral();
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function indexStatus() {
    $total = $this->database->query("SELECT COUNT(*) FROM {site_commerce_product} n")->fetchField();
    $remaining = $this->database->query("SELECT COUNT(DISTINCT n.product_id) FROM {site_commerce_product} n LEFT JOIN {search_dataset} sd ON sd.sid = n.product_id AND sd.type = :type WHERE sd.sid IS NULL OR sd.reindex <> 0", array(':type' => 'site_commerce_product'))->fetchField();
    return ['remaining' => $remaining, 'total' => $total];
  }

  /**
   * {@inheritdoc}
   */
  public function indexClear() {
    \Drupal::service('search.index')->clear('site_commerce_product');
  }

  /**
   * {@inheritdoc}
   */
  public function markForReindex() {
    \Drupal::service('search.index')->markForReindex('site_commerce_product');
  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex() {
    $limit = (int) $this->search_settings->get('index.cron_limit');

    $query = $this->database->select('site_commerce_product', 'n');
    $query->addField('n', 'product_id');
    $query->leftJoin('search_dataset', 'sd', 'sd.sid = n.product_id AND sd.type = :type', array(':type' => 'site_commerce_product'));
    $query->addExpression('CASE MAX(sd.reindex) WHEN NULL THEN 0 ELSE 1 END', 'ex');
    $query->addExpression('MAX(sd.reindex)', 'ex2');
    $query->condition(
      $query->orConditionGroup()
        ->where('sd.sid IS NULL')
        ->condition('sd.reindex', 0, '<>')
    );
    $query->orderBy('ex', 'DESC')
      ->orderBy('ex2')
      ->orderBy('n.product_id')
      ->groupBy('n.product_id')
      ->range(0, $limit);

    $results = $query->execute()->fetchCol();

    if (!$results) {
      return;
    }

    foreach ($results as $product_id) {
      $this->indexEntity($product_id);
    }
  }

  /**
   * Indexes a single product.
   *
   * @param int $product_id
   *   The product ID.
   */
  protected function indexEntity(int $product_id) {
    $storage = \Drupal::entityTypeManager()->getStorage('site_commerce_product');
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('site_commerce_product');

    /** @var \Drupal\site_commerce_product\Entity\Product $entity */
    $entity = $storage->load($product_id);

    // Render content.
    $pre_render = $view_builder->view($entity, 'search_index');
    $text = \Drupal::service('renderer')->render($pre_render);

    $term = $entity->getCategory();
    $category = '';
    if ($term) {
      $category = $term->label();
    }
    $text = "{$category} {$entity->label()} $text";
    $text = mb_strtolower($text, "UTF-8");

    // Clean rendered content.
    $text = strip_tags($text);
    $string_search = array('/', ' - ', '-', ',', '&nbsp;', ':', '.');
    $string_replace = array(' ', ' ', ' ', ' ', ' ', ' ', ' ');
    $text = str_replace($string_search, $string_replace, $text);
    $text = preg_replace('/"([^"]+)"/', '$1', $text);
    $text = preg_replace('/\s{2,}/', ' ', $text);
    $text = preg_replace('/\s+/', ' ', $text);

    // Use the current default interface language.
    $language = \Drupal::languageManager()->getCurrentLanguage();
    $langcode = $language->getId();

    // Transliterate text.
    $text_transliterate = \Drupal::service('transliteration')->transliterate($text, $langcode);

    // Add incorrect text.
    $en = explode(" ", "q w e r t y u i o p [ ] a s d f g h j k l ; ' z x c v b n m , .");
    $ru = explode(" ", "й ц у к е н г ш щ з х ъ ф ы в а п р о л д ж э я ч с м и т ь б ю");
    $text = str_replace($en, $ru, $text);

    $text = $text . ' ' . $text_transliterate;
    $text = trim($text);

    // Update index.
    $type = 'site_commerce_product';
    $sid = $entity->id();
    \Drupal::service('search.index')->index($type, $sid, $langcode, $text);
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $results = [];
    return $results;
  }

}
