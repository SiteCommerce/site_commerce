<?php

namespace Drupal\site_commerce_product\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * A Views style that renders markup for products in catalog.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "site_commerce_product_products_catalog",
 *   title = @Translation("Products in the catalog"),
 *   theme = "site_commerce_product_views_products_catalog",
 *   display_types = {"normal"}
 * )
 */
class ProductsCatalog extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Does the style plugin support grouping of rows?
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

}
