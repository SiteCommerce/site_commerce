<?php

namespace Drupal\site_commerce_product\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the Product entities settings field type.
 *
 * @FieldType(
 *   id = "site_commerce_product_settings",
 *   label = @Translation("Display settings"),
 *   module = "site_commerce_product",
 *   category = @Translation("SiteCommerce"),
 *   translatable = FALSE,
 *   description = @Translation("Display settings"),
 *   default_widget = "site_commerce_product_settings_default",
 *   cardinality = 1
 * )
 */
class ProductSettings extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    //-------------------------------------------------------------------------
    // Настройки отображения товара.
    //-------------------------------------------------------------------------
    $properties['status_new'] = DataDefinition::create('integer')->setLabel(t('New product'));
    $properties['status_appearance'] = DataDefinition::create('string')->setLabel(t('Appearance'));
    $properties['status_stock'] = DataDefinition::create('string')->setLabel(t('Stock availability'));
    $properties['status_type_sale'] = DataDefinition::create('string')->setLabel(t('Type of sale'));

    //-------------------------------------------------------------------------
    // Настройки отображения количества товара.
    //-------------------------------------------------------------------------
    $properties['quantity_stock_allow'] = DataDefinition::create('integer')->setLabel(t('Allow displaying the quantity in stock'));
    $properties['quantity_stock_value'] = DataDefinition::create('integer')->setLabel(t('Quantity in stock'));
    $properties['quantity_order_min'] = DataDefinition::create('integer')->setLabel(t('Minimum order quantity'));
    $properties['quantity_order_max'] = DataDefinition::create('integer')->setLabel(t('Maximum order quantity'));
    $properties['quantity_unit'] = DataDefinition::create('string')->setLabel(t('Unit of quantity measurement'));

    //-------------------------------------------------------------------------
    // Настройки добавления товара в корзину.
    //-------------------------------------------------------------------------
    $properties['cart_form_allow'] = DataDefinition::create('integer')->setLabel(t('Allow displaying the add to cart form'));
    $properties['cart_quantity_allow'] = DataDefinition::create('integer')->setLabel(t('Allow changing the quantity of products in the cart'));
    $properties['cart_after_adding_event'] = DataDefinition::create('string')->setLabel(t('Event after adding to cart'));
    $properties['cart_label_button_add'] = DataDefinition::create('string')->setLabel(t('Button name adding to cart'));
    $properties['cart_label_button_click'] = DataDefinition::create('string')->setLabel(t('Button name buy in one click'));
    $properties['cart_description'] = DataDefinition::create('string')->setLabel(t('Note on the form of adding to cart'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'status_new' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'size' => 'small',
          'not null' => FALSE,
          'default' => 0,
        ],
        'status_appearance' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'status_stock' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'status_type_sale' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'quantity_stock_allow' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'size' => 'small',
          'not null' => FALSE,
          'default' => 1,
        ],
        'quantity_stock_value' => [
          'type' => 'int',
          'not null' => FALSE,
          'default' => 0,
        ],
        'quantity_order_min' => [
          'type' => 'int',
          'not null' => FALSE,
          'default' => 1,
        ],
        'quantity_order_max' => [
          'type' => 'int',
          'not null' => FALSE,
          'default' => 0,
        ],
        'quantity_unit' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'cart_form_allow' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'size' => 'small',
          'not null' => FALSE,
          'default' => 1,
        ],
        'cart_quantity_allow' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'size' => 'small',
          'not null' => FALSE,
          'default' => 1,
        ],
        'cart_after_adding_event' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => 'load_open_cart_form',
        ],
        'cart_label_button_add' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'cart_label_button_click' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'cart_description' => [
          'type' => 'text',
          'not null' => FALSE,
        ],
      ],
      'indexes' => [],
    ];
  }

}
