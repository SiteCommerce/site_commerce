<?php

namespace Drupal\site_commerce_product\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Entity\File;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Image\ImageFactory;

/**
 * Plugin implementation of the 'site_commerce_product_flickity_media_image' formatter.
 *
 * @FieldFormatter(
 *   id = "site_commerce_product_flickity_media_image",
 *   label = @Translation("Flickity product slider"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ProductFlickityMediaImage extends ImageFormatter {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * Constructs a MediaThumbnailFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage, FileUrlGeneratorInterface $file_url_generator, RendererInterface $renderer, ImageFactory $image_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user, $image_style_storage, $file_url_generator);
    $this->renderer = $renderer;
    $this->imageFactory = $image_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('file_url_generator'),
      $container->get('renderer'),
      $container->get('image.factory')
    );
  }

  /**
   * {@inheritdoc}
   *
   * This has to be overridden because FileFormatterBase expects $item to be
   * of type \Drupal\file\Plugin\Field\FieldType\FileItem and calls
   * isDisplayed() which is not in FieldItemInterface.
   */
  protected function needsEntityLoad(EntityReferenceItem $item) {
    return !$item->hasNewEntity();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => '',
      'flickity_auto_play' => 0,
      'flickity_wrap_around' => TRUE
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    unset($element['image_link']);

    $element['flickity_auto_play'] = [
      '#title' => t('Image switching speed in milliseconds'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('flickity_auto_play')
    ];

    $element['flickity_wrap_around'] = [
      '#title' => t('Infinite scrolling of images'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('flickity_wrap_around')
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($this->getSetting('flickity_auto_play')) {
      $seconds = $this->getSetting('flickity_auto_play') / 1000;
      $summary[] = t('Image switching speed seconds: @value', ['@value' => $seconds]);
    }

    if ($this->getSetting('flickity_wrap_around')) {
      $summary[] = t('Infinite scrolling of images');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $media_items = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($media_items)) {
      return $elements;
    }

    $image_style_setting = $this->getSetting('image_style');

    /** @var \Drupal\image\Entity\ImageStyle $image_style */
    $image_style = $this->imageStyleStorage->load($image_style_setting);
    if ($image_style) {
      $elements['#data'] = [];

      /** @var \Drupal\media\MediaInterface[] $media_items */
      foreach ($media_items as $delta => $media) {
        if ($this->viewMode != 'full' && $delta > 2) {
          continue;
        }

        $fid = (int) $media->getSource()->getSourceFieldValue($media);
        if ($file = File::load($fid)) {
          $uri = $file->getFileUri();

          $image_style_uri = $image_style->buildUri($uri);
          $image_factory = $this->imageFactory->get($image_style_uri);

          $elements['#data'][$delta] = [
            'alt' => $media->label(),
            'url' => \Drupal::service('file_url_generator')->generateAbsoluteString($uri),
            'style_url' => $image_style->buildUrl($uri),
            'style_width' => $image_factory->getWidth(),
            'style_height' => $image_factory->getHeight()
          ];

          $this->renderer->addCacheableDependency($elements['#data'][$delta], $media);
        }
      }

      $elements['#theme'] = 'site_commerce_product_flickity_media_image';
      $elements['#attached']['library'][] = 'site_commerce_product/flickity';

      $settings = [
        'cellAlign' => 'left',
        'contain' => TRUE,
        'pageDots' => $delta ? TRUE : FALSE,
        'lazyLoad' => TRUE,
        'autoPlay' => empty($this->getSetting('flickity_auto_play')) ? FALSE : (int) $this->getSetting('flickity_auto_play'),
        'prevNextButtons' => (count($elements['#data']) > 1 && $this->viewMode == 'full') ? TRUE : FALSE,
        'wrapAround' => empty($this->getSetting('flickity_wrap_around')) ? FALSE : (bool) $this->getSetting('flickity_wrap_around'),
      ];
      $elements['#settings'] = Json::encode($settings);

      $this->renderer->addCacheableDependency($elements, $image_style);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for entity types that reference media items.
    return ($field_definition->getFieldStorageDefinition()->getSetting('target_type') == 'media');
  }
}
