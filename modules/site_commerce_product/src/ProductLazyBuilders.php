<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

class ProductLazyBuilders implements TrustedCallbackInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CacheItLazyBuilders object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['cartBlock'];
  }

  /**
   * Callback lazy builder.
   *
   * @param [int] $pid
   * @return array
   */
  public function cartBlock(int $product_id, bool $is_catalog = FALSE) {
    $element = [];

    // Объект карточки товара.
    $entity = $this->entityTypeManager->getStorage('site_commerce_product')->load($product_id);
    if ($entity) {
      // Тип отображения по умолчанию.
      $type = 'default';

      $element = [
        '#theme' => 'site_commerce_product_cart_block',
        '#entity' => $entity,
        '#type' => $type,
        '#is_catalog' => $is_catalog,
      ];
    }

    return $element;
  }

}
