<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Defines a storage handler class for attribute groups.
 */
class ProductAttributeGroupStorage extends ConfigEntityStorage implements ProductAttributeGroupStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getToplevelAttributes($gids) {
    $aids = \Drupal::entityQuery('site_commerce_attribute')
      ->accessCheck(TRUE)
      ->condition('gid', $gids, 'IN')
      ->condition('parent.target_id', 0)
      ->execute();

    return array_values($aids);
  }

}
