<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Product entities.
 *
 * @see \Drupal\site_commerce_product\Entity\Product.
 */
class ProductAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   *
   * Link the activities to the permissions. checkAccess is called with the
   * $operation as defined in the routing.yml file.
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\site_commerce_product\Entity\Product $entity */

    // Fetch information from the product object if possible.
    $status = $entity->isPublished();
    $uid = $entity->getOwnerId();

    // Check if authors can view their own unpublished product.
    if ($operation === 'view' && !$status && $account->isAuthenticated() && $account->id() == $uid) {
      return AccessResult::allowed()->cachePerPermissions()->cachePerUser()->addCacheableDependency($entity);
    }

    // Check if other user can view product.
    if ($operation === 'view' && $status) {
      return AccessResult::allowedIfHasPermission($account, 'site commerce view published products');
    }

    switch ($operation) {
    case 'edit':
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_product');

    case 'update':
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_product');

    case 'duplicate':
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_product');

    case 'delete':
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_product');
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   *
   * Separate from the checkAccess because the entity does not yet exist, it
   * will be created during the 'add' process.
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_product');
  }

}
