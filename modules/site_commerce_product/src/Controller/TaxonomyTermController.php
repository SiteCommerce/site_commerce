<?php

namespace Drupal\site_commerce_product\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\TermInterface;

/**
 * Class TaxonomyTermController
 */
class TaxonomyTermController extends ControllerBase {

  /**
   * Selecting the views function to render the taxonomy term.
   *
   * @param TermInterface $taxonomy_term
   * @return array|null
   */
  public function render(TermInterface $taxonomy_term) {
    if ($taxonomy_term->bundle() == 'site_commerce_catalog') {
      return $this->termView($taxonomy_term);
    } else {
      return views_embed_view('taxonomy_term', 'page_1', $taxonomy_term->id());
    }
  }

  /**
   * Displaying the product catalog.
   *
   * @param TermInterface $entity
   * @return array|null
   */
  private function termView(TermInterface $entity) {
    /** @var \Drupal\taxonomy\TermStorageInterface $term_storage */
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

    // Определяем нужно ли отображать товары с учётом атрибутов (фильтров поиска).
    $private_tempstore = \Drupal::service('tempstore.private')->get('site_commerce');

    // Запоминаем текущую и корневую категорию товаров
    // для установки по умолчанию в формы поиска.
    $parents = $term_storage->loadAllParents($entity->id());
    $category_root = array_key_last($parents);
    if ($category_root) {
      $search['category_root'] = $category_root;
    }
    $search['category'] = $entity->id();
    $private_tempstore->set('search', $search);

    // Считываем значения атрибутов (поисковых фильтров).
    $attributes = [];
    $attributes_separated = '';
    $search = $private_tempstore->get('search');
    if (!empty($search['attributes'])) {
      $attributes = array_values($search['attributes']);

      // Удаляем пустые фильтры, значения которых равно '', 0 или NULL.
      $attributes = array_diff($attributes, ['', 0, NULL]);
    }

    // Первым элементом устанавливаем текущую категорию.
    $terms[$entity->id()] = $entity->id();

    // Если термин таксономии имеет вложенные категории.
    $child_terms = $term_storage->loadTree($entity->bundle(), $entity->id(), NULL, TRUE);
    if (!empty($child_terms)) {
      foreach ($child_terms as $child_term) {
        // Проверяем разрешено или нет отображать текущую категорию в каталоге.
        if ($child_term->isPublished() && $child_term->field_view->value) {
          // Проверяем количество активных товаров в категории.
          $query = \Drupal::entityQuery('site_commerce_product');
          $query->accessCheck(TRUE);
          $query->condition('category', $child_term->id());
          $query->condition('status', 1);
          $query->condition('status_catalog', 1);
          $count = $query->count()->execute();

          if ($count) {
            $terms[$child_term->id()] = $child_term->id();
          }
        }
      }
    }

    $display_id = 'products_in_catalog';
    if ($attributes) {
      $display_id = 'products_by_attributes';
      $attributes_separated = implode(",", $attributes);
    }

    return views_embed_view(
      'site_commerce_product_products_catalog',
      $display_id,
      implode(",", $terms),
      $attributes_separated,
      ['child' => FALSE, 'title' => FALSE]
    );
  }

}
