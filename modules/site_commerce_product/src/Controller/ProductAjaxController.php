<?php

/**
 * @file
 * Contains \Drupal\site_commerce_product\Controller\ProductAjaxController
 */

namespace Drupal\site_commerce_product\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\site_commerce_product\Controller\ProductDatabaseController;
use Drupal\site_commerce_product\ProductInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductAjaxController extends ControllerBase {

  /**
   * The servises classes.
   *
   * @var \Drupal\site_commerce_product\Controller\ProductDatabaseController
   */
  protected $database;

  /**
   * The tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * Construct.
   *
   * @param \Drupal\site_commerce_product\Controller\ProductDatabaseController $connection
   *   The database connection.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore.
   */
  public function __construct(ProductDatabaseController $database, PrivateTempStoreFactory $temp_store_factory) {
    $this->database = $database;
    $this->tempStore = $temp_store_factory->get('site_commerce_product');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('site_commerce_product.database'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Регистрирует изменение стоимости позиции.
   * @param Request $request
   * @param string $method
   * @return AjaxResponse
   */
  public function updatePrice(Request $request, $method) {
    // Инициализация AjaxResponse.
    $response = new AjaxResponse();

    // Проверка входных данных.
    $data = $request->request->get('data');
    if ($method != 'ajax') {
      throw new NotFoundHttpException();
    }
    $data = json_decode($data);

    $entity = \Drupal::entityTypeManager()
      ->getStorage('site_commerce_product')
      ->load($data->pid);
    if ($entity instanceof ProductInterface) {
      $number_from = trim($data->number_from);
      $number_from = \Drupal::service('kvantstudio.formatter')->decimal($number_from);

      $number = trim($data->number);
      $number = \Drupal::service('kvantstudio.formatter')->decimal($number);

      // Изменяем стоимость.
      $group = 'retail';
      $values = [
        'group' => $group,
        'number_from' => $number_from,
        'number' => $number,
      ];
      $entity->setPrice($values, $group);

      // Изменяем единицу измерения количества.
      $entity->setQuantityUnit($data->quantity_unit);

      // Сохраняем изменения.
      $entity->save();

      // Изменяем цвет строки.
      $selector = '#product-' . $data->pid;
      $css = array(
        'background-color' => '#00db6a',
      );
      $response->addCommand(new CssCommand($selector, $css));

      // Очищает cache.
      \Drupal::service('cache_tags.invalidator')->invalidateTags([
        'taxonomy_term:' . $data->tid,
        'site_commerce_product:' . $entity->pid,
      ]);
    }

    // Return ajax response.
    return $response;
  }

  /**
   * Регистрирует вес позиций в категории.
   *
   * @param string $method
   * @param int $cid
   * @param string $pids
   *
   * @return ajax response
   *
   * @access public
   */
  public function positionsSetWeight($method, $tid, $pids) {
    if ($method == 'ajax') {
      // Create AJAX Response object.
      $response = new AjaxResponse();

      $pids = explode("&pid=", $pids);
      unset($pids[0]);

      $weight = 1;
      foreach ($pids as $pid) {
        $entity = \Drupal::entityTypeManager()
          ->getStorage('site_commerce_product')
          ->load($pid);
        if ($entity instanceof ProductInterface) {
          $entity->setWeight($weight);

          // Сохраняем изменения.
          $entity->save();

          $weight++;
        }
      }

      // Очищает cache.
      \Drupal::service('cache_tags.invalidator')->invalidateTags([
        'taxonomy_term:' . $tid,
      ]);

      // Return ajax response.
      return $response;
    }
  }

  /**
   * Ajax handler for loading products for selected categories.
   * @param Request $request
   * @param string $method
   * @return AjaxResponse
   */
  public function productsLoadByCategories(Request $request, $method) {
    // Initializing AjaxResponse.
    $response = new AjaxResponse();

    // Input data validation.
    if ($method != 'ajax') {
      throw new NotFoundHttpException();
    }

    // Search filter values ​​for the current user.
    $taxonomy_search = $this->tempStore->get('taxonomy_search');

    $data = $request->request->get('data');
    $data = Json::decode($data);
    if (empty($data['tids'])) {
      $data = $taxonomy_search['tids'];
    }

    $build = [];

    /** @var \Drupal\taxonomy\TermStorageInterface $term_storage */
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

    if (!empty($data['tids'])) {
      foreach ($data['tids'] as $tid) {
        $entity = $term_storage->load($tid);
        if ($entity->field_view->value) {
          continue;
        }

        // Если термин таксономии имеет вложенные категории.
        $terms = $term_storage->loadTree($entity->bundle(), $entity->id(), NULL, TRUE);
        if ($terms) {
          foreach ($terms as $term) {
            if ($term->field_view->value) {
              $build[] = views_embed_view(
                'site_commerce_product_products_catalog',
                'products_in_catalog_preview',
                $term->id(),
                '',
                ['child' => TRUE, 'title' => FALSE]
              );
            }
          }
        } else {
          $build = views_embed_view(
            'site_commerce_product_products_catalog',
            'products_in_catalog',
            $entity->id(),
            '',
            ['child' => FALSE, 'title' => TRUE, '#pager' => ['view' => FALSE]],
          );
        }
      }
    }

    // If no product category is selected.
    if (empty($build)) {
      $response->addCommand(new AlertCommand($this->t('Select the product categories and click «Apply».')));
    } else {
      if ($id = Html::getId($data['id'])) {
        $response->addCommand(new HtmlCommand("#{$id}", $build));
      }
    }

    return $response;
  }
}
