<?php

namespace Drupal\site_commerce_product\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SearchController extends ControllerBase {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Construct.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Clean search string.
   *
   * @param string $string
   * @return string
   */
  private function cleanString(string $string) {
    $string = Xss::filter($string);
    $string = strip_tags($string);
    $string_search = array('/', ' - ', '-', ',', '&nbsp;', ':', '.');
    $string_replace = array(' ', ' ', ' ', ' ', ' ', ' ', ' ');
    $string = str_replace($string_search, $string_replace, $string);
    $string = preg_replace('/"([^"]+)"/', '$1', $string);
    $string = preg_replace('/\s{2,}/', ' ', $string);
    $string = preg_replace('/\s+/', ' ', $string);
    $string = mb_strtolower($string, "UTF-8");

    return trim($string);
  }

  /**
   * Ajax handler for loading products for selected categories.
   * @param Request $request
   * @param string $method
   * @return AjaxResponse
   */
  public function saveKeywords(Request $request, string $method) {
    // Initializing AjaxResponse.
    $response = new AjaxResponse();

    // Input data validation.
    if ($method != 'ajax') {
      throw new NotFoundHttpException();
    }

    $data = $request->request->get('data');
    $data = Json::decode($data);
    if (!empty($data['keywords'])) {
      $string = $data['keywords'];
      $keywords = rawurlencode($string);
      $url = Url::fromRoute('site_commerce_product.search_results', ['keywords' => $keywords])->toString();
      $response->addCommand(new RedirectCommand($url));
    } else {
      throw new NotFoundHttpException();
    }

    return $response;
  }

  /**
   * Create title for search results page.
   */
  public function searchResultsTitle(string $keywords) {
    // Clean search string.
    $string = rawurldecode($keywords);
    $string = $this->cleanString($string);

    return $this->t('Are you looking for @value', ['@value' => $string]);
  }

  /**
   * Create search results page.
   */
  public function searchResults(string $keywords) {
    // Clean search string.
    $string = rawurldecode($keywords);
    $string = $this->cleanString($string);

    $string_array = explode(" ", $string);
    $request = '';
    $arguments = [];
    $i = 1;
    $count_array_string = count($string_array);
    foreach ($string_array as $key) {
      $arguments[':key' . $i] = $key;
      if ($count_array_string > $i) {
        $request .= 'INSTR(i.data, :key' . $i . ') > 0 AND ';
      } else {
        $request .= 'INSTR(i.data, :key' . $i . ') > 0';
      }
      $i++;
    }

    $query = $this->connection->select('search_dataset', 'i');
    $query->fields('i', array('sid'));
    $query->join('site_commerce_product_field_data', 's', 's.product_id = i.sid');
    $query->condition('i.type', 'site_commerce_product');
    $query->condition('s.status', 1);
    $query->where($request, $arguments);
    $query->orderBy('s.title', 'ASC');
    $result = $query->execute()->fetchCol();

    // $query = $this->connection->select('search_index', 'i');
    // $query->fields('i', array('sid'));
    // $query->join('site_commerce_product_field_data', 's', 's.product_id = i.sid');
    // $query->condition('i.type', 'site_commerce_product');
    // $query->condition('s.status', 1);
    // $query->where($request, $arguments);
    // $query->orderBy('s.title', 'ASC');
    // $result = $query->execute()->fetchCol();

    $matches = [];
    foreach ($result as $sid) {
      if (!array_key_exists($sid, $matches)) {
        $matches[$sid] = $sid;
      }
    }

    $build = [];
    if ($matches) {
      $string_matches = implode("+", $matches);
      $build = views_embed_view(
        'site_commerce_product_products_catalog',
        'search_products',
        $string_matches,
        NULL,
        [
          'name' => NULL,
          'summary' => NULL,
          'child' => FALSE,
          'title' => TRUE,
          '#pager' => ['view' => TRUE],
        ],
      );
    } else {
      $build = [
        '#markup' => $this->t('No products found.'),
      ];
    }

    return $build;
  }
}
