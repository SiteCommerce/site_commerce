<?php

namespace Drupal\site_commerce_product\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Implements functions for working with the product catalog.
 */
class ProductCatalogController {

  /**
   * Display root or child categories by dictionary.
   */
  public static function catalog(string $vid = "site_commerce_catalog", int $parent = 0) {
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vid);
    $query->condition('status', 1);
    $query->condition('field_view', 1);
    $query->condition('parent', $parent);
    $query->sort('weight', 'ASC');
    $query->sort('name', 'ASC');
    $query->accessCheck(TRUE);
    $tids = $query->execute();

    $tags = ['site_commerce_product_catalog'];

    /** @var \Drupal\site_commerce_product\ProductStorageInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('site_commerce_product');
    foreach ($tids as $tid) {
      $count = $storage->countProductsByCategory($tid);
      if ($count) {
        $tags[] = "taxonomy_term:$tid";
      } else {
        unset($tids[$tid]);
      }
    }

    $entities = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadMultiple($tids);

    return [
      '#theme' => 'site_commerce_product_catalog',
      '#terms' => $entities,
      '#vid' => $vid,
      '#attached' => [
        'library' => [
          'site_commerce_product/catalog',
        ],
      ],
      '#cache' => [
        'tags' => $tags,
      ],
    ];
  }

  /**
   * Display categories by dictionary as a list.
   */
  public static function catalogList($vid = 'site_commerce_catalog', int $parent = 0) {
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vid);
    $query->condition('status', 1);
    $query->condition('field_view', 1);
    $query->condition('parent', $parent);
    $query->sort('weight', 'ASC');
    $query->sort('name', 'ASC');
    $query->accessCheck(TRUE);
    $tids = $query->execute();

    $tags = ['site_commerce_product_catalog_list'];

    /** @var \Drupal\site_commerce_product\ProductStorageInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('site_commerce_product');
    foreach ($tids as $tid) {
      $count = $storage->countProductsByCategory($tid);
      if ($count) {
        $tags[] = "taxonomy_term:$tid";
      } else {
        unset($tids[$tid]);
      }
    }

    $entities = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadMultiple($tids);

    return [
      '#theme' => 'site_commerce_product_catalog_list',
      '#terms' => $entities,
      '#vid' => $vid,
      '#attached' => [
        'library' => [
          'site_commerce_product/catalog',
        ],
      ],
      '#cache' => [
        'tags' => $tags,
      ],
    ];
  }

  /**
   * List of child categories by category ID of the parent.
   */
  public static function catalogChildrenList(int $tid, int $max_depth = NULL) {
    return [
      '#theme' => 'site_commerce_product_catalog_children_list',
      '#tid' => $tid,
      '#max_depth' => $max_depth,
      '#attached' => [
        'library' => [
          'site_commerce_product/catalog',
        ],
      ],
    ];
  }
}
