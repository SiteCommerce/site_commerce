<?php

use Drupal\Core\Url;
use Drupal\Core\Render\Element;
use Drupal\taxonomy\TermInterface;

/**
 * Implements template_preprocess_site_commerce_product().
 */
function template_preprocess_site_commerce_product(&$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['site_commerce_product'] = $variables['elements']['#site_commerce_product'];

  /** @var \Drupal\site_commerce_product\Entity\Product $entity */
  $entity = $variables['site_commerce_product'];

  // Helpful $content variable for templates.
  $variables['content'] = [];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  // Product image.
  $view_builder = $entity->get('field_media_image')->view($variables['view_mode']);
  $variables['content']['field_media_image'] = $view_builder;

  // Load the configuration.
  $config = \Drupal::config('site_commerce_product.settings');

  // Product name.
  $variables['title'] = '';
  $product_allow_title = (bool) $config->get('product_allow_title');
  if ($product_allow_title) {
    $variables['title'] = $entity->label();
  }

  // Allow to display basic parameters.
  $variables['basic_parametrs'] = FALSE;

  // Product code in stock.
  $variables['code'] = '';
  $code = $entity->get('code')->value;
  if ($code) {
    $variables['basic_parametrs'] = TRUE;
    $variables['code'] = $code;
  }

  // Manufacturer part number.
  $variables['code_manufacturer'] = '';
  $code_manufacturer = $entity->get('code_manufacturer')->value;
  if ($code_manufacturer) {
    $variables['basic_parametrs'] = TRUE;
    $variables['code_manufacturer'] = $code_manufacturer;
  }

  // Count in stock.
  $variables['quantity_stock_allow'] = (bool) $entity->get('settings')->quantity_stock_allow;
  $variables['quantity_stock_value'] = '';
  $variables['quantity_unit'] = '';
  if ($variables['quantity_stock_allow']) {
    $variables['basic_parametrs'] = TRUE;
    $variables['quantity_stock_value'] = (int) $entity->get('settings')->quantity_stock_value;
    $variables['quantity_unit'] = $entity->get('settings')->quantity_unit;
  }

  // Note above the add to cart form.
  $variables['cart_description'] = '';
  $cart_description = $entity->get('settings')->cart_description;
  if ($cart_description) {
    $variables['cart_description'] = $cart_description;
  }

  // A block with a shopping cart for the current state of the purchase.
  $variables['cart'] = [];
  $cart_form_allow = (bool) $entity->get('settings')->cart_form_allow;
  if ($cart_form_allow) {
    $variables['cart']['data'] = [
      '#lazy_builder' => ['site_commerce_product.lazy_builders:cartBlock', [$entity->id(), FALSE]],
      '#create_placeholder' => TRUE,
    ];
  }
}

/**
 * Prepares variables for site-commerce-products-catalog-products.html.twig template.
 *
 * @param array $variables
 */
function template_preprocess_site_commerce_product_catalog_products(&$variables) {
  // Перечень товаров по категориям.
  $data = $variables['data']['taxonomy_term'];
  $view = $variables['data']['view'];

  $config = \Drupal::config('site_commerce_product.settings');

  // Текущая категория.
  $variables['category'] = [
    'name' => '',
    'root' => $config->get('catalog_url'),
    'parents' => [],
  ];

  /** @var \Drupal\taxonomy\TermStorageInterface $term_storage */
  $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

  // Фильтры по каталогу товаров.
  $tid = (int) $variables['data']['tid'];
  $term = $term_storage->load($tid);
  if ($term instanceof TermInterface && $view->current_display == 'products_in_catalog') {
    $variables['category']['name'] = $term->label();

    // Перечень родителей текущей категории.
    $parents = $term_storage->loadAllParents($term->id());
    $parents = array_reverse($parents);
    foreach ($parents as $parent) {
      if ($parent->id() != $term->id()) {
        $variables['category']['parents'][$parent->id()] = [
          'name' => $parent->label(),
        ];
      }
    }

    // Проверяем наличие дочерних терминов.
    $childs_terms = $term_storage->loadTree($term->bundle(), $term->id(), 1, TRUE);
    foreach ($childs_terms as $child_term) {
      // Количество товаров в категории.
      // Если у категории есть дочерние категории, то к ней нельзя привязывать товары.
      $count_products = 0;

      // Если разрешено отображать категорию.
      if ($child_term->isPublished() && $child_term->field_view->value) {
        // Проверяем наличие товаров во всех вложенных категориях.
        $childs = $term_storage->loadTree($child_term->bundle(), $child_term->id(), NULL, FALSE);
        if ($childs) {
          foreach ($childs as $child) {
            $query = \Drupal::database()->select('site_commerce_product_field_data', 'n');
            $query->fields('n', ['product_id']);
            $query->condition('n.status', 1);
            $query->condition('n.status_catalog', 1);
            $query->condition('n.category', $child->tid);
            $count = $query->countQuery()->execute()->fetchField();
            if ($count) {
              $count_products = $count_products + $count;
            }
          }
        } else {
          // Проверяем наличие товара в текущей категории, которую нужно отобразить в списке.
          $query = \Drupal::database()->select('site_commerce_product_field_data', 'n');
          $query->fields('n', ['product_id']);
          $query->condition('n.status', 1);
          $query->condition('n.status_catalog', 1);
          $query->condition('n.category', $child_term->id());
          $count = $query->countQuery()->execute()->fetchField();
          if ($count) {
            $count_products = $count;
          }
        }

        // Если есть товары.
        if ($count_products) {
          $variables['categories'][$child_term->id()]['name'] = $child_term->label();
          $variables['categories'][$child_term->id()]['count'] = $count_products;
        }
      }
    }
  }

  $variables['products'] = [];
  foreach ($data as $tid => $value) {
    $variables['products'][$tid]['tid'] = $value['tid'];
    $variables['products'][$tid]['name'] = $value['name'];
    $variables['products'][$tid]['summary'] = $value['summary'];
    $variables['products'][$tid]['count_products'] = 0;

    foreach ($value['products'] as $entity) {
      $variables['products'][$tid]['items'][$entity->id()]['site_commerce_product'] = $entity;

      $variables['products'][$tid]['items'][$entity->id()]['title'] = $entity->label();

      $view_builder = $entity->get('field_summary_catalog')->view('catalog');
      $variables['products'][$tid]['items'][$entity->id()]['field_summary_catalog'] = $view_builder;

      $view_builder = $entity->get('price')->view('catalog');
      $variables['products'][$tid]['items'][$entity->id()]['price'] = $view_builder;

      // Форма добавления в корзину.
      $add_cart_form = [
        '#create_placeholder' => TRUE,
        '#lazy_builder' => ['site_commerce_product.lazy_builders:cartBlock', [$entity->id(), TRUE]],
      ];
      $variables['products'][$tid]['items'][$entity->id()]['add_cart_form'] = $add_cart_form;

      // Изображение товара.
      // $value = $entity->get('field_media_image')->getValue();
      // if (!$value) {
      //   $entity = \Drupal::service('kvantstudio.helper')->setDefaultMedia($entity, 'field_media_image');
      // }
      $view_builder = $entity->get('field_media_image')->view('catalog');
      $variables['products'][$tid]['items'][$entity->id()]['field_media_image'] = $view_builder;

      // Наличие на складе.
      $variables['products'][$tid]['items'][$entity->id()]['quantity_stock_allow'] = (bool) $entity->get('settings')->quantity_stock_allow;
      $variables['products'][$tid]['items'][$entity->id()]['quantity_stock_value'] = (int) $entity->get('settings')->quantity_stock_value;
      $variables['products'][$tid]['items'][$entity->id()]['quantity_unit'] = $entity->get('settings')->quantity_unit;

      // Счётчик товаров.
      $variables['products'][$tid]['count_products'] = $variables['products'][$tid]['count_products'] + 1;
    }
  }
}

/**
 * Prepares variables for site-commerce-products-catalog-products-preview.html.twig template.
 *
 * @param array $variables
 */
function template_preprocess_site_commerce_product_catalog_products_preview(&$variables) {
  // Перечень товаров по категориям.
  $data = $variables['data']['taxonomy_term'];

  $variables['products'] = [];
  foreach ($data as $tid => $value) {
    $variables['products'][$tid]['tid'] = $value['tid'];
    $variables['products'][$tid]['name'] = $value['name'];
    $variables['products'][$tid]['summary'] = $value['summary'];
    $variables['products'][$tid]['count_products'] = 0;

    foreach ($value['products'] as $entity) {
      $variables['products'][$tid]['items'][$entity->id()]['site_commerce_product'] = $entity;

      $variables['products'][$tid]['items'][$entity->id()]['title'] = $entity->label();

      $view_builder = $entity->get('field_summary_catalog')->view('catalog');
      $variables['products'][$tid]['items'][$entity->id()]['field_summary_catalog'] = $view_builder;

      $view_builder = $entity->get('price')->view('catalog');
      $variables['products'][$tid]['items'][$entity->id()]['price'] = $view_builder;

      // Форма добавления в корзину.
      $add_cart_form = [
        '#create_placeholder' => TRUE,
        '#lazy_builder' => ['site_commerce_product.lazy_builders:cartBlock', [$entity->id(), TRUE]],
      ];
      $variables['products'][$tid]['items'][$entity->id()]['add_cart_form'] = $add_cart_form;

      // Изображение товара.
      // $value = $entity->get('field_media_image')->getValue();
      // if (!$value) {
      //   $entity = \Drupal::service('kvantstudio.helper')->setDefaultMedia($entity, 'field_media_image');
      // }
      $view_builder = $entity->get('field_media_image')->view('catalog');
      $variables['products'][$tid]['items'][$entity->id()]['field_media_image'] = $view_builder;

      // Наличие на складе.
      $variables['products'][$tid]['items'][$entity->id()]['quantity_stock_allow'] = (bool) $entity->get('settings')->quantity_stock_allow;
      $variables['products'][$tid]['items'][$entity->id()]['quantity_stock_value'] = (int) $entity->get('settings')->quantity_stock_value;
      $variables['products'][$tid]['items'][$entity->id()]['quantity_unit'] = $entity->get('settings')->quantity_unit;

      // Счётчик товаров.
      $variables['products'][$tid]['count_products'] = $variables['products'][$tid]['count_products'] + 1;
    }
  }
}

/**
 * Implements template_preprocess_HOOK() for list of available site_commerce_product type templates.
 */
function template_preprocess_site_commerce_product_add_list(&$variables) {
  if (!empty($variables['content'])) {
    foreach ($variables['content'] as $type) {
      $variables['types'][$type->id()]['label'] = $type->label();
      $variables['types'][$type->id()]['url'] = Url::fromRoute('entity.site_commerce_product.add_form', ['site_commerce_product_type' => $type->id()])->toString();
    }
  }
}

/**
 * Prepares variables for site-commerce-product-catalog.html.twig template.
 *
 * @param array $variables
 */
function template_preprocess_site_commerce_product_catalog(&$variables) {
  $terms = $variables['terms'];

  $variables['categories'] = [];
  foreach ($terms as $term) {
    $variables['categories'][$term->id()]['name'] = $term->label();

    // Изображение категории.
    $variables['categories'][$term->id()]['field_media_image'] = NULL;
    $value = $term->get('field_media_image')->getValue();
    if ($value) {
      $view_builder = $term->get('field_media_image')->view('catalog');
      $variables['categories'][$term->id()]['field_media_image'] = $view_builder;
    }
  }
}

/**
 * Prepares variables for site-commerce-product-catalog-list.html.twig template.
 *
 * @param array $variables
 */
function template_preprocess_site_commerce_product_catalog_list(&$variables) {
  $terms = $variables['terms'];
  $variables['categories'] = [];
  foreach ($terms as $term) {
    $variables['categories'][$term->id()]['name'] = $term->label();
  }
}

/**
 * Prepares variables for site-commerce-product-catalog-children-list.html.twig template.
 *
 * @param array $variables
 */
function template_preprocess_site_commerce_product_catalog_children_list(&$variables) {
  $variables['categories'] = [];

  $tid = (int) $variables['tid'];
  $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($tid);
  if ($term instanceof TermInterface) {
    /** @var \Drupal\taxonomy\TermStorageInterface $term_storage */
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

    $max_depth = $variables['max_depth'];

    // Проверяем существование дочерних терминов.
    $childs_terms = $term_storage->loadTree($term->bundle(), $term->id(), $max_depth, TRUE);
    foreach ($childs_terms as $child_term) {
      // Количество товаров в категории.
      // Если у категории есть дочерние категории, то к ней нельзя привязывать товары.
      $count_products = 0;

      // Если разрешено отображать категорию.
      if ($child_term->isPublished() && $child_term->field_view->value) {
        // Проверяем наличие товаров во всех вложенных категориях.
        $childs = $term_storage->loadTree($child_term->bundle(), $child_term->id(), NULL, FALSE);
        if ($childs) {
          foreach ($childs as $child) {
            $query = \Drupal::database()->select('site_commerce_product_field_data', 'n');
            $query->fields('n', ['product_id']);
            $query->condition('n.status', 1);
            $query->condition('n.status_catalog', 1);
            $query->condition('n.category', $child->tid);
            $count = $query->countQuery()->execute()->fetchField();
            if ($count) {
              $count_products = $count_products + $count;
            }
          }
        } else {
          // Проверяем наличие товара в текущей категории, которую нужно отобразить в списке.
          $query = \Drupal::database()->select('site_commerce_product_field_data', 'n');
          $query->fields('n', ['product_id']);
          $query->condition('n.status', 1);
          $query->condition('n.status_catalog', 1);
          $query->condition('n.category', $child_term->id());
          $count = $query->countQuery()->execute()->fetchField();
          if ($count) {
            $count_products = $count;
          }
        }

        // Если есть товары.
        if ($count_products) {
          $variables['categories'][$child_term->id()]['name'] = $child_term->label();
          $variables['categories'][$child_term->id()]['count_products'] = $count_products;
        }
      }
    }
  }
}

/**
 * Prepares variables for site-commerce-product-views-products-catalog.html.twig template.
 *
 * @param array $variables
 */
function template_preprocess_site_commerce_product_views_products_catalog(&$variables) {
  /** @var \Drupal\views\ViewExecutable $view */
  $view = $variables['view'];

  // Массив с данными для отображения.
  $data = [];
  $data['view'] = $view;
  $data['tid'] = 0;

  // Products entities for view.
  $rows = $variables['rows'];
  $entities = [];

  // Переопределяем список товаров для режима
  // отображения popular_products - Популярные товары.
  if ($view->current_display == 'popular_products' || $view->current_display == 'popular_products_preview') {
    $entities = \Drupal::entityTypeManager()->getStorage('site_commerce_cart')->lastAddedProducts($view->getItemsPerPage());
  } else {
    foreach ($rows as $row) {
      $entities[] = $row['#site_commerce_product'];
    }
  }

  // Cache tags.
  $tags = [];

  if ($entities) {
    // Текущий термин таксономии.
    $tid = 0;
    if (isset($view->argument['category'])) {
      $tid = (int) $view->argument['category']->getValue();
      $data['tid'] = $tid;
    }

    // Аргументы views.
    $is_child = FALSE;
    $is_title = FALSE;
    if (isset($view->args[2])) {
      $parametrs = $view->args[2];
      $is_child = isset($parametrs['child']) ? (bool) $parametrs['child'] : FALSE;
      $is_title = isset($parametrs['title']) ? (bool) $parametrs['title'] : FALSE;
    }

    // Определяем привязку товаров к атрибутам-фильтрам.
    // Удаляем пустые фильтры, значения которых равно нулю.
    $private_tempstore = \Drupal::service('tempstore.private');
    $attributes_search = $private_tempstore->get('site_commerce_product')->get('attributes_search');
    if ($attributes_search && is_array($attributes_search)) {
      $attributes_search = array_values($attributes_search);
      $attributes_search = array_diff($attributes_search, ['', 0, NULL]);
    }

    // Cache tags.
    $attributes_separated = "0";
    if ($attributes_search && is_array($attributes_search)) {
      $attributes_separated = implode("-", $attributes_search);
    }
    $tags += [
      'products_catalog_attributes',
      'products_catalog_attributes:' . $attributes_separated,
    ];

    // Массив с данными для отображения.
    $data['taxonomy_term'][$tid]['tid'] = 0;
    $data['taxonomy_term'][$tid]['name'] = empty($parametrs['name']) ? '' : $parametrs['name'];
    $data['taxonomy_term'][$tid]['summary'] = empty($parametrs['summary']) ? '' : $parametrs['summary'];
    $data['taxonomy_term'][$tid]['products'] = $entities;

    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $term = $term_storage->load($tid);
    if ($term instanceof TermInterface) {
      if ($is_child) {
        $data['taxonomy_term'][$tid]['name'] = $term->label();
        $data['taxonomy_term'][$tid]['tid'] = $term->id();
      }

      if ($is_title) {
        $data['taxonomy_term'][$tid]['name'] = $term->label();
      }

      $data['taxonomy_term'][$tid]['summary'] = $term->get('field_summary_catalog')->value;

      $tags += [
        'taxonomy_term:' . $tid,
      ];
    }
  }

  // Определяем шаблон, по которому будет формироваться каталог товаров.
  $theme_name = 'site_commerce_product_catalog_products';
  if (preg_match("/preview/i", $view->current_display)) {
    $theme_name = 'site_commerce_product_catalog_products_preview';
  }

  $variables['products'] = [
    '#theme' => $theme_name,
    '#data' => $data,
    '#attached' => [
      'library' => [
        'site_commerce_product/card',
        'site_commerce_product/catalog_products',
      ],
    ],
  ];

  if ($tags) {
    $variables['products']['#cache']['tags'] = $tags;
  }
}

/**
 * Implements template_preprocess_site_commerce_product_price_editor().
 */
function template_preprocess_site_commerce_product_price_editor(&$variables) {
  $entities = $variables['site_commerce_products'];

  $variables['products'] = [];
  foreach ($entities as $entity) {
    $prices = $entity->getPrices();
    $variables['products'][$entity->id()] = [
      'label' => $entity->label(),
      'number_from' => $prices['number_from'],
      'number' => $prices['number'],
      'number_sale' => $prices['number_sale'],
      'currency_code' => $prices['currency_code'],
      'quantity_unit' => $entity->getQuantityUnit(),
    ];
  }

  // Quantity units.
  $variables['units'] = getUnitMeasurement();
}
