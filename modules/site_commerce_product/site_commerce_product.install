<?php

/**
 * @file
 * Install file for the site_commerce_product.module.
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Implements hook_install().
 */
function site_commerce_product_install() {
  // Создает словарь для категорий товаров.
  $vid = 'site_commerce_catalog';
  $vocabularies = Vocabulary::loadMultiple();
  if (!isset($vocabularies[$vid])) {
    $vocabulary = Vocabulary::create([
      'vid' => $vid,
      'machine_name' => $vid,
      'name' => 'Каталог товаров',
    ]);
    $vocabulary->save();
  }

  // Создает тестовую категорию «Товары для дома».
  $term = Term::create([
    'name' => 'Товары для дома',
    'vid' => $vid,
  ]);
  $term->save();
  $tid = $term->id();

  // Создает тестовую категорию «Товары для дома» / «Посуда».
  $term = Term::create([
    'name' => 'Посуда',
    'vid' => $vid,
    'parent' => [$tid],
  ]);
  $term->save();

  // We create auxiliary directories.
  $directories = [
    'public://images/products',
    'public://images/categories',
    'public://import/excel',
    'public://import/images',
    'public://export/templates',
    'public://export/prices',
  ];
  foreach ($directories as $directory) {
    \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
  }
}

/**
 * Delete unused configuration.
 */
function site_commerce_product_update_8101(&$sandbox) {
  \Drupal::configFactory()->getEditable('site_commerce_product.import')->delete();
  return 'Update 8101 installed successfully.';
}

/**
 * Create new field in settings of products.
 */
function site_commerce_product_update_8102(&$sandbox) {
  // Добавляем определение поля для учета количества товара на складе.
  $field_type = 'site_commerce_product_settings';
  $column_name = 'quantity_stock_value';
  \Drupal::service('kvantstudio.helper')->fieldDefinitionAddColumn($field_type, $column_name);

  // Обновляем определение типа сущности.
  $manager = \Drupal::entityDefinitionUpdateManager();
  $type = $manager->getEntityType('site_commerce_product');
  $type->set('data_table', 'site_commerce_product_field_data');
  $type->set('permission_granularity', 'bundle');
  $manager->updateEntityType($type);

  return 'Update 8102 installed successfully.';
}

/**
 * Update products units quantity by ОКЕИ.
 */
function site_commerce_product_update_8103(&$sandbox) {
  $database = \Drupal::database();

  $database->update('site_commerce_product__field_settings')
    ->fields(['field_settings_quantity_unit' => 'шт'])
    ->condition('field_settings_quantity_unit', 'шт.')
    ->execute();

  $database->update('site_commerce_product__field_settings')
    ->fields(['field_settings_quantity_unit' => 'набор'])
    ->condition('field_settings_quantity_unit', 'комплект')
    ->execute();

  $database->update('site_commerce_product__field_settings')
    ->fields(['field_settings_quantity_unit' => 'кв. м'])
    ->condition('field_settings_quantity_unit', 'м<sup><small>2</small></sup>')
    ->execute();

  $database->update('site_commerce_product__field_settings')
    ->fields(['field_settings_quantity_unit' => 'упак'])
    ->condition('field_settings_quantity_unit', 'упак.')
    ->execute();

  return 'Update 8103 installed successfully.';
}

/**
 * Adds the attribute entity type.
 */
function site_commerce_product_update_8104(&$sandbox) {
  // Gets DatabaseSchema object for manipulating the schema.
  $schema = \Drupal::database()->schema();

  // Clears static and persistent plugin definition caches.
  \Drupal::entityTypeManager()->clearCachedDefinitions();

  $definition = \Drupal::entityTypeManager()->getDefinition('site_commerce_attribute_group');
  \Drupal::entityDefinitionUpdateManager()->installEntityType($definition);

  if (!$schema->tableExists('site_commerce_attribute')) {
    $definition = \Drupal::entityTypeManager()->getDefinition('site_commerce_attribute');
    \Drupal::entityDefinitionUpdateManager()->installEntityType($definition);
  }
}

/**
 * Adds new permissions to roles.
 */
function site_commerce_product_update_8105(&$sandbox) {
  $perms[] = 'site commerce view published products';

  $role = Role::load(AccountInterface::ANONYMOUS_ROLE);
  if ($role instanceof RoleInterface) {
    if (!$role->hasPermission('site commerce view published products')) {
      user_role_grant_permissions(AccountInterface::ANONYMOUS_ROLE, $perms);
    }
  }

  $role = Role::load(AccountInterface::AUTHENTICATED_ROLE);
  if ($role instanceof RoleInterface) {
    if (!$role->hasPermission('site commerce view published products')) {
      user_role_grant_permissions(AccountInterface::AUTHENTICATED_ROLE, $perms);
    }
  }

  return 'Update 8105 installed successfully.';
}

/**
 * Add 'category' field to 'site_commerce_attribute' entities.
 */
function site_commerce_product_update_8106() {
  // Clears static and persistent plugin definition caches.
  \Drupal::entityTypeManager()->clearCachedDefinitions();

  $storage_definition = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Attribute category'))
    ->setDescription(t('The category of this attribute.'))
    ->setRequired(FALSE)
    ->setSetting('target_type', 'taxonomy_term')
    ->setTranslatable(TRUE)
    ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

  \Drupal::entityDefinitionUpdateManager()->installFieldStorageDefinition('category', 'site_commerce_attribute', 'site_commerce_attribute', $storage_definition);
}

/**
 * Add 'root' field to 'site_commerce_attribute' entities.
 */
function site_commerce_product_update_8107() {
  // Clears static and persistent plugin definition caches.
  \Drupal::entityTypeManager()->clearCachedDefinitions();

  $storage_definition = BaseFieldDefinition::create('boolean')
  ->setLabel(t('The attribute is root within the group'))
  ->setRequired(FALSE)
  ->setDisplayOptions('form', [
    'type' => 'boolean_checkbox',
    'weight' => 7,
    'settings' => [
      'display_label' => TRUE,
    ],
  ])
  ->setDisplayConfigurable('form', TRUE)
  ->setDisplayConfigurable('view', FALSE);

  \Drupal::entityDefinitionUpdateManager()->installFieldStorageDefinition('root', 'site_commerce_attribute', 'site_commerce_attribute', $storage_definition);
}

/**
 * Create 'site_commerce_product' entity type base fields.
 */
function site_commerce_product_update_9501(&$sandbox) {
  $database = \Drupal::database();
  $transaction = $database->startTransaction();

  $entity_type_id = 'site_commerce_product';
  $field_name_delete = 'field_code';
  $field_name_create = 'code';
  \Drupal::service('kvantstudio.helper')->entityTypeBaseFieldCreate($entity_type_id, $field_name_create);

  $existing_values = $database->select('site_commerce_product__' . $field_name_delete, 'n')
    ->fields('n', ['entity_id', $field_name_delete . '_value'])
    ->execute()
    ->fetchAll();

  foreach ($existing_values as $value) {
    $database->update('site_commerce_product_field_data')
      ->fields([
        $field_name_create => $value->{$field_name_delete . '_value'}
      ])
      ->condition('product_id', $value->entity_id)
      ->execute();
  }

  \Drupal::service('kvantstudio.helper')->entityTypeFieldDelete($entity_type_id, $field_name_delete);

  unset($transaction);
}

/**
 * Create 'site_commerce_product' entity type base fields.
 */
function site_commerce_product_update_9502(&$sandbox) {
  $entity_type_id = 'site_commerce_product';
  $field_name_delete = 'field_code_manufacturer';
  $field_name_create = 'code_manufacturer';
  \Drupal::service('kvantstudio.helper')->entityTypeBaseFieldCreate($entity_type_id, $field_name_create);
  \Drupal::service('kvantstudio.helper')->entityTypeFieldDelete($entity_type_id, $field_name_delete);
}

/**
 * Create 'site_commerce_product' entity type base fields.
 */
function site_commerce_product_update_9503(&$sandbox) {
  $database = \Drupal::database();
  $transaction = $database->startTransaction();

  $entity_type_id = 'site_commerce_product';
  $field_name_delete = 'field_id_external';
  $field_name_create = 'code_external';
  \Drupal::service('kvantstudio.helper')->entityTypeBaseFieldCreate($entity_type_id, $field_name_create);

  $existing_values = $database->select('site_commerce_product__' . $field_name_delete, 'n')
    ->fields('n', ['entity_id', $field_name_delete . '_value'])
    ->execute()
    ->fetchAll();

  foreach ($existing_values as $value) {
    $database->update('site_commerce_product_field_data')
      ->fields([
        $field_name_create => $value->{$field_name_delete . '_value'}
      ])
      ->condition('product_id', $value->entity_id)
      ->execute();
  }

  \Drupal::service('kvantstudio.helper')->entityTypeFieldDelete($entity_type_id, $field_name_delete);

  unset($transaction);
}

/**
 * Create 'site_commerce_product' entity type base fields.
 */
function site_commerce_product_update_9504(&$sandbox) {
  $database = \Drupal::database();
  $transaction = $database->startTransaction();

  $entity_type_id = 'site_commerce_product';
  $field_name_delete = 'field_search_index';
  $field_name_create = 'search_index';
  \Drupal::service('kvantstudio.helper')->entityTypeBaseFieldCreate($entity_type_id, $field_name_create);

  $existing_values = $database->select('site_commerce_product__' . $field_name_delete, 'n')
    ->fields('n', ['entity_id', $field_name_delete . '_value'])
    ->execute()
    ->fetchAll();

  foreach ($existing_values as $value) {
    $database->update('site_commerce_product_field_data')
      ->fields([
        $field_name_create => $value->{$field_name_delete . '_value'}
      ])
      ->condition('product_id', $value->entity_id)
      ->execute();
  }

  \Drupal::service('kvantstudio.helper')->entityTypeFieldDelete($entity_type_id, $field_name_delete);

  unset($transaction);
}

/**
 * Create 'site_commerce_product' entity type base fields.
 */
function site_commerce_product_update_9505(&$sandbox) {
  $database = \Drupal::database();
  $transaction = $database->startTransaction();

  $entity_type_id = 'site_commerce_product';
  $field_name_delete = 'field_category';
  $field_name_create = 'category';
  \Drupal::service('kvantstudio.helper')->entityTypeBaseFieldCreate($entity_type_id, $field_name_create);

  $existing_values = $database->select('site_commerce_product__' . $field_name_delete, 'n')
    ->fields('n', ['entity_id', $field_name_delete . '_target_id'])
    ->execute()
    ->fetchAll();

  foreach ($existing_values as $value) {
    $database->update('site_commerce_product_field_data')
      ->fields([
        $field_name_create => $value->{$field_name_delete . '_target_id'}
      ])
      ->condition('product_id', $value->entity_id)
      ->execute();
  }

  \Drupal::service('kvantstudio.helper')->entityTypeFieldDelete($entity_type_id, $field_name_delete);

  unset($transaction);
}

/**
 * Create 'site_commerce_product' entity type base fields.
 */
function site_commerce_product_update_9506(&$sandbox) {
  $entity_type_id = 'site_commerce_product';
  $field_name_delete = 'field_categories';
  $field_name_create = 'additional_category';
  \Drupal::service('kvantstudio.helper')->entityTypeBaseFieldCreate($entity_type_id, $field_name_create);
  \Drupal::service('kvantstudio.helper')->entityTypeFieldDelete($entity_type_id, $field_name_delete);
}

/**
 * Create 'site_commerce_product' entity type base fields.
 */
function site_commerce_product_update_9507(&$sandbox) {
  $database = \Drupal::database();
  $transaction = $database->startTransaction();

  $entity_type_id = 'site_commerce_product';
  $field_name_delete = 'field_price_group';
  $field_name_create = 'price';
  \Drupal::service('kvantstudio.helper')->entityTypeBaseFieldCreate($entity_type_id, $field_name_create);

  $existing_values = $database->select('site_commerce_product__' . $field_name_delete, 'n')
    ->fields('n')
    ->execute()
    ->fetchAll();

  foreach ($existing_values as $value) {
    $database->insert("site_commerce_product__$field_name_create")
      ->fields([
        'bundle' => $value->bundle,
        'deleted' => $value->deleted,
        'entity_id' => $value->entity_id,
        'revision_id' => $value->revision_id,
        'langcode' => $value->langcode,
        'delta' => $value->delta,
        'price_group' => $value->{$field_name_delete . '_group'},
        'price_prefix' => $value->{$field_name_delete . '_prefix'},
        'price_suffix' => $value->{$field_name_delete . '_suffix'},
        'price_number_from' => $value->{$field_name_delete . '_number_from'},
        'price_number' => $value->{$field_name_delete . '_number'},
        'price_currency_code' => $value->{$field_name_delete . '_currency_code'},
        'price_number_sale' => $value->{$field_name_delete . '_number_sale'},
      ])
      ->execute();
  }

  \Drupal::service('kvantstudio.helper')->entityTypeFieldDelete($entity_type_id, $field_name_delete);

  unset($transaction);
}

/**
 * Create 'site_commerce_product' entity type base fields.
 */
function site_commerce_product_update_9508(&$sandbox) {
  $database = \Drupal::database();
  $transaction = $database->startTransaction();

  $entity_type_id = 'site_commerce_product';
  $field_name_delete = 'field_settings';
  $field_name_create = 'settings';
  \Drupal::service('kvantstudio.helper')->entityTypeBaseFieldCreate($entity_type_id, $field_name_create);

  $existing_values = $database->select('site_commerce_product__' . $field_name_delete, 'n')
    ->fields('n')
    ->execute()
    ->fetchAll();

  foreach ($existing_values as $value) {
    $database->insert("site_commerce_product__$field_name_create")
      ->fields([
        'bundle' => $value->bundle,
        'deleted' => $value->deleted,
        'entity_id' => $value->entity_id,
        'revision_id' => $value->revision_id,
        'langcode' => $value->langcode,
        'delta' => $value->delta,
        'settings_status_new' => $value->{$field_name_delete . '_status_new'},
        'settings_status_appearance' => $value->{$field_name_delete . '_status_appearance'},
        'settings_status_stock' => $value->{$field_name_delete . '_status_stock'},
        'settings_status_type_sale' => $value->{$field_name_delete . '_status_type_sale'},
        'settings_quantity_stock_allow' => $value->{$field_name_delete . '_quantity_stock_allow'},
        'settings_quantity_order_min' => $value->{$field_name_delete . '_quantity_order_min'},
        'settings_quantity_order_max' => $value->{$field_name_delete . '_quantity_order_max'},
        'settings_quantity_unit' => $value->{$field_name_delete . '_quantity_unit'},
        'settings_cart_form_allow' => $value->{$field_name_delete . '_cart_form_allow'},
        'settings_cart_quantity_allow' => $value->{$field_name_delete . '_cart_quantity_allow'},
        'settings_cart_after_adding_event' => $value->{$field_name_delete . '_cart_after_adding_event'},
        'settings_cart_label_button_add' => $value->{$field_name_delete . '_cart_label_button_add'},
        'settings_cart_label_button_click' => $value->{$field_name_delete . '_cart_label_button_click'},
        'settings_cart_description' => $value->{$field_name_delete . '_cart_description'},
        'settings_quantity_stock_value' => $value->{$field_name_delete . '_quantity_stock_value'},
      ])
      ->execute();
  }

  \Drupal::service('kvantstudio.helper')->entityTypeFieldDelete($entity_type_id, $field_name_delete);

  unset($transaction);
}

/**
 * Update 'site_commerce_attribute' entity type base fields.
 */
function site_commerce_product_update_9509(&$sandbox) {
  $entity_type_id = 'site_commerce_attribute';
  $field_name_delete = 'price';
  $field_name_create = 'price';
  \Drupal::service('kvantstudio.helper')->entityTypeBaseFieldDelete($entity_type_id, $field_name_delete);
  \Drupal::service('kvantstudio.helper')->entityTypeBaseFieldCreate($entity_type_id, $field_name_create);
}
