<?php

namespace Drupal\site_commerce_cart\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\kvantstudio\Formatter;
use Drupal\site_commerce_cart\CartInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Cart entity.
 *
 * @ingroup site_commerce_cart
 *
 * @ContentEntityType(
 *   id = "site_commerce_cart",
 *   label = @Translation("Cart item"),
 *   label_collection = @Translation("Cart items"),
 *   label_singular = @Translation("cart item"),
 *   label_plural = @Translation("cart items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count cart item",
 *     plural = "@count cart items",
 *   ),
 *   bundle_label = @Translation("Cart item type"),
 *   handlers = {
 *     "storage" = "Drupal\site_commerce_cart\CartStorage",
 *     "list_builder" = "Drupal\site_commerce_cart\CartListBuilder",
 *     "views_data" = "Drupal\site_commerce_cart\CartViewsData",
 *     "access" = "Drupal\site_commerce_cart\CartAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\site_commerce_cart\Form\CartForm",
 *       "edit" = "Drupal\site_commerce_cart\Form\CartForm",
 *       "delete" = "Drupal\site_commerce_cart\Form\CartDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_commerce_cart\CartHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "site_commerce_cart",
 *   admin_permission = "administer site_commerce_cart",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "cart_id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "label" = "title",
 *   },
 *   links = {
 *     "edit-form" = "/admin/site-commerce/shopping-list/{site_commerce_cart}/edit",
 *     "delete-form" = "/admin/site-commerce/shopping-list/{site_commerce_cart}/delete",
 *     "collection" = "/admin/site-commerce/shopping-list",
 *   },
 *   bundle_entity_type = "site_commerce_cart_type",
 *   field_ui_base_route = "entity.site_commerce_cart_type.edit_form"
 * )
 */
class Cart extends ContentEntityBase implements CartInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function getCartItemEntityTypeId(): string {
    return (string) $this->get('entity_type_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCartItemEntityId(): int {
    return (int) $this->get('entity_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return (string) $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title): CartInterface {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReceiptTitle(int $width = 128): string {
    $receipt_title = (string) $this->get('receipt_title')->value;
    $title = $receipt_title ?: $this->getTitle();
    return mb_strimwidth($title, 0, $width);
  }

  /**
   * {@inheritdoc}
   */
  public function setReceiptTitle($title): CartInterface {
    $this->set('receipt_title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuantity(): int {
    return (int) $this->get('quantity')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuantity(int $value): CartInterface {
    $this->set('quantity', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice(bool $format = FALSE): float {
    $number = $this->get('price')->number;
    if ($format) {
      $number = Formatter::removeTrailingZeros($number);
    }

    return (float) $number;
  }

  /**
   * {@inheritdoc}
   */
  public function getPriceСurrencyСode(): string {
    return (string) $this->get('price')->currency_code;
  }

  /**
   * {@inheritdoc}
   */
  public function setPrice(float $value, string $currency_code = NULL): CartInterface {
    $this->set('price', [
      'number' => $value,
      'currency_code' => $currency_code ?: $this->getPriceСurrencyСode()
    ]);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaid(bool $format = FALSE): float {
    $number = $this->get('paid')->number;
    if ($format) {
      $number = Formatter::removeTrailingZeros($number);
    }

    return (float) $number;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalPaid(bool $format = FALSE): float {
    $number = $this->get('paid')->number;
    $quantity = $this->getQuantity();
    $number = $number * $quantity;
    if ($format) {
      $number = Formatter::removeTrailingZeros($number);
    }

    return (float) $number;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaidСurrencyСode(): string {
    return (string) $this->get('paid')->currency_code;
  }

  /**
   * {@inheritdoc}
   */
  public function setPaid(float $value, string $currency_code = NULL): CartInterface {
    $this->set('paid', [
      'number' => $value,
      'currency_code' => $currency_code ?: $this->getPaidСurrencyСode()
    ]);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentObject(): string {
    $value = (string) $this->get('payment_object')->value;
    return $value ?: self::getDefaultPaymentObject();
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentObject(string $value): CartInterface {
    $this->set('payment_object', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getVatType(): string {
    $value = (string) $this->get('vat_type')->value;
    return $value ?: self::getDefaultVatType();
  }

  /**
   * {@inheritdoc}
   */
  public function setVatType(string $value): CartInterface {
    $this->set('vat_type', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMeasurementUnit(): string {
    $value = (string) $this->get('measurement_unit')->value;
    return $value ?: self::getDefaultMeasurementUnit();
  }

  /**
   * {@inheritdoc}
   */
  public function setMeasurementUnit(string $value): CartInterface {
    $this->set('measurement_unit', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentMode(): string {
    $value = (string) $this->get('payment_mode')->value;
    return $value ?: self::getDefaultPaymentMode();
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentMode(string $value): CartInterface {
    $this->set('payment_mode', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrderId(int $order_id): CartInterface {
    $this->set('order_id', $order_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return (int) $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): CartInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += self::ownerBaseFieldDefinitions($entity_type);

    $fields['uid']
      ->setLabel(t('Customer'))
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author'
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['drupal_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID of the unauthorized сustomer'));

    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order'))
      ->setSetting('target_type', 'site_commerce_order')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string'
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['entity_type_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type of product'))
      ->setRequired(TRUE);

    $fields['entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Product'))
      ->setRequired(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Product name'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['receipt_title'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Product name in the receipt'))
    ->setRequired(FALSE);

    $fields['quantity'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Quantity'))
      ->setRequired(TRUE)
      ->setDefaultValue(1)
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'number'
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer'
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['measurement_unit'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Unit of measurement'))
    ->setRequired(TRUE)
    ->setDefaultValueCallback(static::class . '::getDefaultMeasurementUnit');

    $fields['price'] = BaseFieldDefinition::create('site_commerce_price')
      ->setLabel(t('Price'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['paid'] = BaseFieldDefinition::create('site_commerce_price')
      ->setLabel(t('Total price'))
      ->setDescription(t('The final price to pay for the order.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['payment_object'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Payment subject attribute'))
    ->setRequired(TRUE)
    ->setDefaultValueCallback(static::class . '::getDefaultPaymentObject');

    $fields['payment_mode'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Calculation method'))
    ->setRequired(TRUE)
    ->setDefaultValueCallback(static::class . '::getDefaultPaymentMode');

    $fields['vat_type'] = BaseFieldDefinition::create('string')
    ->setLabel(t('VAT type'))
    ->setRequired(TRUE)
    ->setDefaultValueCallback(static::class . '::getDefaultVatType');

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Show in the cart'))
      ->setDescription(t('Status that allows displaying the item in the cart. When deleting a position, the status is set to 0 and is not taken into account in the cost calculations and the order.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['api'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Added by API'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('Date the item was created in the cart.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('Date the item was edited in the cart.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function getPaymentObjects(): array {
    $types = [
      'COMMODITY',
      'EXCISE',
      'JOB',
      'SERVICE',
      'PAYMENT',
      'ANOTHER',
      'COMMODITY_MARKING_NO_CODE',
      'COMMODITY_MARKING_WITH_CODE',
      'EXCISE_MARKING_NO_CODE',
      'EXCISE_MARKING_WITH_CODE'
    ];

    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaultPaymentObject(): string {
    return 'COMMODITY';
  }

  /**
   * {@inheritdoc}
   */
  public static function getVatTypes(): array {
    $types = [
      'NONE',
      'VAT0',
      'VAT10',
      'VAT110',
      'VAT20',
      'VAT120'
    ];

    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaultVatType(): string {
    return 'NONE';
  }

  /**
   * {@inheritdoc}
   */
  public static function getMeasurementUnits(): array {
    $types = [
      'OTHER',
      'PIECE',
      'GRAM',
      'KILOGRAM',
      'TON',
      'CENTIMETER',
      'DECIMETER',
      'METER',
      'SQUARE_CENTIMETER',
      'SQUARE_DECIMETER',
      'SQUARE_METER',
      'MILLILITER',
      'LITER',
      'CUBIC_METER',
      'KILOWATT_HOUR',
      'GIGACALORIE',
      'DAY',
      'HOUR',
      'MINUTE',
      'SECOND',
      'KILOBYTE',
      'MEGABYTE',
      'GIGABYTE',
      'TERABYTE'
    ];

    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaultMeasurementUnit(): string {
    return 'OTHER';
  }

  /**
   * {@inheritdoc}
   */
  public static function getPaymentModes(): array {
    $types = [
      'FULL_PREPAYMENT',
      'FULL_PAYMENT',
      'ADVANCE',
      'PREPAYMENT'
    ];

    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaultPaymentMode(): string {
    return 'FULL_PREPAYMENT';
  }
}
