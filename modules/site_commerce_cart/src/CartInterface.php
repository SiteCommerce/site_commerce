<?php

namespace Drupal\site_commerce_cart;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cart entities.
 *
 * @ingroup site_commerce_cart
 */
interface CartInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the cart item entity_type_id.
   *
   * @return string
   *   Name of the entity type.
   */
  public function getCartItemEntityTypeId(): string;

  /**
   * Gets the cart item entity_id.
   *
   * @return int
   *   ID of the entity cart item.
   */
  public function getCartItemEntityId(): int;

  /**
   * Gets the cart item name.
   *
   * @return string
   *   Name of the cart item.
   */
  public function getTitle(): string;

  /**
   * Sets the cart item name.
   *
   * @param string $title
   *   The cart item.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setTitle($title): CartInterface;

  /**
   * Gets the receipt title of cart item.
   *
   * @param int $width
   *   The width of the desired trim.
   *
   * @return string
   *   Name in the receipt.
   */
  public function getReceiptTitle(int $width = 128): string;

  /**
   * Sets the receipt title of cart item.
   *
   * @param string $title
   *   Name in the receipt.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setReceiptTitle($title): CartInterface;

  /**
   * Gets the quantity in cart.
   *
   * @return int
   *   Quantity in cart.
   */
  public function getQuantity(): int;

  /**
   * Sets the quantity in cart.
   *
   * @param int $value
   *   Quantity in cart.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.   *
   */
  public function setQuantity(int $value): CartInterface;

  /**
   * Gets the cart cost value.
   *
   * @param bool $format
   *   If TRUE will be remove trailing zeros.
   *
   * @return float
   */
  public function getPrice(bool $format = FALSE): float;

  /**
   * Gets the currency code of cart price.
   *
   * @return string
   */
  public function getPriceСurrencyСode(): string;

  /**
   * Set the cart price value.
   *
   * @param float $value
   *   The cart price value.
   * @param string $currency_code
   *   The currency code.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called entity.
   */
  public function setPrice(float $value, string $currency_code = NULL): CartInterface;

  /**
   * Gets the cart paid value.
   *
   * @param bool $format
   *   If TRUE will be remove trailing zeros.
   *
   * @return float
   */
  public function getPaid(bool $format = FALSE): float;

  /**
   * Gets the cart total paid value by quantity.
   *
   * @param bool $format
   *   If TRUE will be remove trailing zeros.
   *
   * @return float
   */
  public function getTotalPaid(bool $format = FALSE): float;

  /**
   * Gets the currency code of cart paid.
   *
   * @return string
   */
  public function getPaidСurrencyСode(): string;

  /**
   * Set the cart paid value.
   *
   * @param float $value
   *   The cart paid value.
   * @param string $currency_code
   *   The currency code.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called entity.
   */
  public function setPaid(float $value, string $currency_code = NULL): CartInterface;

  /**
   * Gets the payment object value.
   *
   * @return string
   *   The payment object value.
   */
  public function getPaymentObject(): string;

  /**
   * Sets the payment object value.
   *
   * @param string $value
   *   The payment object value.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setPaymentObject(string $value): CartInterface;

  /**
   * Gets the vat type value.
   *
   * @return string
   *   The vat type value.
   */
  public function getVatType(): string;

  /**
   * Sets the vat type value.
   *
   * @param string $value
   *   The vat type value.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setVatType(string $value): CartInterface;

  /**
   * Gets the measurement unit value.
   *
   * @return string
   *   The measurement unit value.
   */
  public function getMeasurementUnit(): string;

  /**
   * Sets the measurement unit value.
   *
   * @param string $value
   *   The measurement unit value.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setMeasurementUnit(string $value): CartInterface;

  /**
   * Gets the payment mode value.
   *
   * @return string
   *   The payment mode value.
   */
  public function getPaymentMode(): string;

  /**
   * Sets the payment mode value.
   *
   * @param string $value
   *   The payment mode value.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setPaymentMode(string $value): CartInterface;

  /**
   * Sets the order ID.
   *
   * @param int $order_id
   *   The ID of order entity.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setOrderId(int $order_id): CartInterface;

  /**
   * Gets the Cart creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cart.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the Cart creation timestamp.
   *
   * @param int $timestamp
   *   The Cart creation timestamp.
   *
   * @return \Drupal\site_commerce_cart\Entity\CartInterface
   *   The called Cart entity.
   */
  public function setCreatedTime(int $timestamp): CartInterface;

  /**
   * All values for 'payment_object' base field.
   *
   * @return mixed
   *   An array values for the 'payment_object' field.
   */
  public static function getPaymentObjects(): array;

  /**
   * Default value callback for 'payment_object' base field.
   *
   * @return mixed
   *   A default value for the 'payment_object' field.
   */
  public static function getDefaultPaymentObject(): string;

  /**
   * All values for 'vat_type' base field.
   *
   * @return mixed
   *   An array values for the 'vat_type' field.
   */
  public static function getVatTypes(): array;

  /**
   * Default value callback for 'vat_type' base field.
   *
   * @return mixed
   *   A default value for the 'vat_type' field.
   */
  public static function getDefaultVatType(): string;

  /**
   * All values for 'measurement_unit' base field.
   *
   * @return mixed
   *   An array values for the 'measurement_unit' field.
   */
  public static function getMeasurementUnits(): array;

  /**
   * Default value callback for 'measurement_unit' base field.
   *
   * @return mixed
   *   A default value for the 'measurement_unit' field.
   */
  public static function getDefaultMeasurementUnit(): string;

  /**
   * All values for 'payment_mode' base field.
   *
   * @return mixed
   *   An array values for the 'payment_mode' field.
   */
  public static function getPaymentModes(): array;

  /**
   * Default value callback for 'payment_mode' base field.
   *
   * @return mixed
   *   A default value for the 'payment_mode' field.
   */
  public static function getDefaultPaymentMode(): string;
}
