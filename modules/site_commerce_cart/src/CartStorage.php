<?php

namespace Drupal\site_commerce_cart;

use Drupal\site_commerce_product\ProductInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\kvantstudio\Formatter;
use Drupal\user\UserInterface;

/**
 * Defines a storage class for cart entity.
 */
class CartStorage extends SqlContentEntityStorage implements CartStorageInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Implements data formatting functions.
   *
   * @var \Drupal\kvantstudio\Formatter
   */
  protected $formatter;

  /**
   * Constructs a CommentStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_info
   *   An array of entity info for the entity type.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend instance to use.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\kvantstudio\Formatter $formatter
   *   Implements data formatting functions.
   */
  public function __construct(EntityTypeInterface $entity_info, Connection $database, EntityFieldManagerInterface $entity_field_manager, AccountInterface $current_user, CacheBackendInterface $cache, LanguageManagerInterface $language_manager, MemoryCacheInterface $memory_cache, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityTypeManagerInterface $entity_type_manager, Formatter $formatter) {
    parent::__construct($entity_info, $database, $entity_field_manager, $cache, $language_manager, $memory_cache, $entity_type_bundle_info, $entity_type_manager);
    $this->currentUser = $current_user;
    $this->formatter = $formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_info) {
    return new static(
      $entity_info,
      $container->get('database'),
      $container->get('entity_field.manager'),
      $container->get('current_user'),
      $container->get('cache.entity'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('kvantstudio.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function hasPositionInCart(string $entity_type_id, int $entity_id, ?UserInterface $account = NULL): bool|int {
    $query = $this->database->select('site_commerce_cart', 'n');
    $query->addField('n', 'cart_id');
    $query->isNull('n.order_id');
    $query->condition('n.entity_type_id', $entity_type_id);
    $query->condition('n.entity_id', $entity_id);

    if ($account) {
      $query->condition('n.uid', $account->id());
    } else {
      if ($this->currentUser->isAuthenticated()) {
        $query->condition('n.uid', $this->currentUser->id());
      } else {
        $query->condition('n.drupal_uuid', kvantstudio_user_hash());
      }
    }

    $count = (int) $query->execute()->fetchField();
    if ($count) {
      return $count;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasPositionsInCart(?UserInterface $account = NULL): bool {
    $query = $this->database->select('site_commerce_cart', 'n');
    $query->addExpression('COUNT(n.cart_id)');
    $query->isNull('n.order_id');
    $query->isNull('n.api');

    if ($account) {
      $query->condition('n.uid', $account->id());
    } else {
      if ($this->currentUser->isAuthenticated()) {
        $query->condition('n.uid', $this->currentUser->id());
      } else {
        $query->condition('n.drupal_uuid', kvantstudio_user_hash());
      }
    }

    return (bool) $query->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function addToCart(string $type, string $entity_type_id, int $entity_id, string $title, float $number, string $currency_code, int $quantity, ?bool $api = NULL, ?UserInterface $account = NULL): CartInterface {
    // Check if there is an item in the cart that has not been added to the order.
    /** @var \Drupal\site_commerce_cart\CartInterface $entity */
    $entity = NULL;

    if ($cart_id = $this->hasPositionInCart($entity_type_id, $entity_id, $account)) {
      $entity = $this->load($cart_id);
    } else {
      $entity = $this->create([
        'type' => $type,
        'entity_type_id' => $entity_type_id,
        'entity_id' => $entity_id,
        'uid' => $account ? $account->id() : $this->currentUser->id(),
        'drupal_uuid' => $account ? $account->uuid() : kvantstudio_user_hash(),
        'title' => $title,
        'quantity' => $quantity,
        'price' => [
          'number' => $number,
          'currency_code' => $currency_code,
        ],
        'paid' => [
          'number' => $number,
          'currency_code' => $currency_code,
        ],
        'status' => 1,
        'api' => $api,
      ]);

      $entity->enforceIsNew(TRUE);
      $entity->save();
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemsByOrderId(int $order_id): array {
    $query = $this->database->select('site_commerce_cart', 'n');
    $query->fields('n');
    $query->condition('n.order_id', $order_id);
    $result = $query->execute()->fetchAll();

    $data = [];
    foreach ($result as $row) {
      $data[] = (array) $row;
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function loadCart(?int $order_id = NULL): array {
    $query = $this->database->select('site_commerce_cart', 'n');
    $query->fields('n');

    // Определяем позиции добавленные текущим пользователем.
    if ($order_id) {
      $query->condition('n.order_id', $order_id);
    } else {
      $query->isNull('n.order_id');
      $query->isNull('n.api');
      if ($this->currentUser->isAuthenticated()) {
        $query->condition('n.uid', $this->currentUser->id());
      } else {
        $query->condition('n.drupal_uuid', kvantstudio_user_hash());
      }
    }

    $result = $query->execute();

    /** @var \Drupal\image\ImageStyleInterface $style */
    $style = $this->entityTypeManager->getStorage('image_style')->load('product_card_image');

    $data = [];
    foreach ($result as $row) {
      // TODO: Добавить поддержку изображения заглушки.
      $style_url = NULL;

      // Define the currency symbol.
      $currency_entities = $this->entityTypeManager->getStorage('site_commerce_currency')->loadByProperties([
        'letter_code' => $row->paid__currency_code
      ]);
      /** @var \Drupal\site_commerce_price\CurrencyInterface $currency */
      $currency = reset($currency_entities);

      // Product entity.
      $entity = NULL;
      $entity_type_exist = $this->entityTypeManager->hasDefinition($row->entity_type_id);
      if ($entity_type_exist) {
        $entity = $this->entityTypeManager->getStorage($row->entity_type_id)->load($row->entity_id);
      }

      // If the entity is a product of current online store.
      if ($entity instanceof ProductInterface) {
        // If image exist.
        if (!$entity->get('field_media_image')->isEmpty()) {
          $field_media_image = $entity->get('field_media_image')->first()->getValue();
          if (isset($field_media_image['target_id'])) {
            $media = Media::load($field_media_image['target_id']);
            $media_field = $media->get('field_media_image')->first()->getValue();
            $file = File::load($media_field['target_id']);
            if ($file) {
              $uri = $file->getFileUri();
              $style_url = $style->buildUrl($uri);
            }
          }
        }

        $data[$row->cart_id] = [
          'image_url' => $style_url,
          'entity_id' => $row->entity_id,
          'url' => $entity->toUrl()->toString(),
          'title' => $row->title,
          'receipt_title' => $row->receipt_title,
          'quantity_stock_allow' => $entity->getStockAllow(),
          'quantity_stock' => $entity->getStockAllow() ? $entity->getStockValue() : NULL,
          'quantity_cart' => (int) $row->quantity,
          'quantity_min' => $entity->getQuantityOrderMin(),
          'quantity_unit' => $entity->getQuantityUnit(),
          'price' => $this->formatter->price($row->paid__number),
          'currency_code' => $row->paid__currency_code,
          'symbol' => $currency->getSymbol(),
        ];
      } else {
        $data[$row->cart_id] = [
          'image_url' => $style_url,
          'entity_id' => $row->entity_id,
          'url' => NULL,
          'title' => $row->title,
          'receipt_title' => $row->receipt_title,
          'quantity_visible' => TRUE,
          'quantity_cart' => $row->quantity,
          'quantity_min' => 1,
          'quantity_unit' => NULL,
          'price' => $this->formatter->price($row->price__number),
          'currency_code' => $this->formatter->price($row->price__currency_code),
          'symbol' => $currency->getSymbol(),
        ];
      }

      // Удаление позиции из корзины если товар удалён на сайте и заказ еще не оформлен.
      if (!$entity && !$row->order_id) {
        $cart = $this->load($row->cart_id);
        $cart->delete();

        \Drupal::messenger()->addWarning(
          'The product <strong>@name</strong> is no longer available for purchase and has been removed from your shopping cart.',
          ['@name' => $cart->label()]
        );
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function lastAddedProducts(int $length, ?string $entity_type_id) {
    $query = $this->database->select('site_commerce_cart', 'n');
    $query->addField('n', 'entity_id');
    $query->orderBy('created', 'DESC');
    $query->range(0, $length);

    if ($entity_type_id) {
      $query->condition('n.entity_type_id', $entity_type_id);
    }

    $product_ids = $query->execute()->fetchCol();

    return $this->entityTypeManager->getStorage('site_commerce_product')->loadMultiple($product_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function cartItemsNotOrdered(int $period = 86400) {
    // Определяем максимальное время в течении которого товары могут находиться в корзине.
    // По умочанию 24 часа.
    $request_time = \Drupal::time()->getRequestTime();
    $timestamp = (int) ($request_time - $period);

    $query = $this->database->select('site_commerce_cart', 'n');
    $query->fields('n', ['cart_id']);
    $query->condition('n.order_id', 0);
    $query->condition('n.created', $timestamp, '<');
    $query->condition('n.status', 1);

    return $query->execute()->fetchAll();
  }
}
