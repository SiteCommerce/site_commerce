<?php

namespace Drupal\site_commerce_cart;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\user\UserInterface;

/**
 * Defines an interface for cart entity storage class.
 */
interface CartStorageInterface extends ContentEntityStorageInterface {

  /**
   * Check for current user if position by type and id in cart when order is not completed.
   *
   * @param string $entity_type_id
   *   'entity_type_id' of added item.
   * @param int $entity_id
   *   'entity_id' of added item.
   * @param UserInterface|null $account
   *   The user account for which the items have been added to the cart.
   *
   * @return bool|int
   *   FALSE or ID of cart item.
   */
  public function hasPositionInCart(string $entity_type_id, int $entity_id, ?UserInterface $account = NULL): bool|int;

  /**
   * Check for current user if positions in cart when order is not completed and added witout API.
   *
   * @param UserInterface|null $account
   *   The user account for which the items have been added to the cart.
   *
   * @return bool
   */
  public function hasPositionsInCart(?UserInterface $account = NULL): bool;

  /**
   * Create item in cart.
   * Item may be added by API from other site.
   *
   * @param string $type
   *   ID of 'site_commerce_cart_type' entity type.
   * @param string $entity_type_id
   *   'entity_type_id' of added item.
   * @param int $entity_id
   *   'entity_id' of added item.
   * @param string $title
   *   Title of added item.
   * @param float $number
   *   Price of added item.
   * @param string $currency_code
   *   Currency of added item.
   * @param int $quantity
   *   Quantity of added item.
   * @param bool|null $api
   *   TRUE if the item is added to the cart via API. Such positions are not displayed to the user and he cannot delete them.
   * @param UserInterface|null $account
   *   The user account for which the items have been added to the cart.
   *
   * @return \Drupal\site_commerce_cart\CartInterface
   *   Created or existed cart entity by parametrs.
   */
  public function addToCart(string $type, string $entity_type_id, int $entity_id, string $title, float $number, string $currency_code, int $quantity, ?bool $api = NULL, ?UserInterface $account = NULL): CartInterface;

  /**
   * Gets array items of the cart by order number.
   *
   * @param int $order_id
   *   The order ID for load cart items.
   *
   * @return array
   */
  public function getItemsByOrderId(int $order_id): array;

  /**
   * Loads the contents of the cart by order number.
   *
   * @param int|null $order_id
   *   The order ID for load cart items.
   *
   * @return array
   */
  public function loadCart(?int $order_id = NULL): array;

  /**
   * Gets last added to cart products.
   *
   * @param int $length
   *   Count of last added products.
   * @param string $entity_type_id
   *   Entity type of cart items.
   *
   * @return array
   *   An array of products entities.
   */
  public function lastAddedProducts(int $length, ?string $entity_type_id);

  /**
   * List of cart items not placed in order.
   *
   * @param int $period
   * @return array
   */
  public function cartItemsNotOrdered(int $period = 86400);

}
