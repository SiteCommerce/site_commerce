<?php

namespace Drupal\site_commerce_cart\TwigExtension;

use Drupal\site_commerce_cart\Form\CartProductAddForm;
use Drupal\site_commerce_product\ProductInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension that adds a custom function and a custom filter.
 */
class CartTwigExtension extends AbstractExtension {

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return [
      new TwigFunction('cart_product_add_form', $this->CartProductAddForm(...)),
      new TwigFunction('goto_order_checkout_form', $this->gotoOrderCheckoutForm(...)),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'site_commerce_cart.twig_extension';
  }

  /**
   * Формирует форму добавить в корзину.
   */
  public static function CartProductAddForm(ProductInterface $entity, bool $is_catalog = FALSE) {
    $form = new CartProductAddForm($entity, $is_catalog);
    $form = \Drupal::formBuilder()->getForm($form);
    return $form;
  }

  /**
   * Форма перехода к странице оформления заказа.
   */
  public static function gotoOrderCheckoutForm() {
    $form = \Drupal::formBuilder()->getForm('Drupal\site_commerce_cart\Form\CartGotoOrderCheckoutForm');
    return $form;
  }
}
