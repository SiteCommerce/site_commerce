<?php

namespace Drupal\site_commerce_cart\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\site_commerce_cart\CartInterface;
use Drupal\site_commerce_product\ProductInterface;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CartController extends ControllerBase {

  /**
   * Страница c корзиной.
   */
  public function cartPage() {
    $noindex_meta_tag = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'noindex, nofollow',
      ],
    ];

    return [
      '#theme' => 'site_commerce_cart_page',
      '#attached' => [
        'library' => [],
        'html_head' => [[$noindex_meta_tag, 'noindex']],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Обновляет значение количества в корзине с учётом остатков на складе.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param string $method
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function cartItemUpdateQuantity(Request $request, string $method) {
    $response = new AjaxResponse();

    // Проверка входных данных.
    $data = $request->request->get('data');
    if (empty($data) || $method != 'ajax') {
      throw new NotFoundHttpException();
    }
    $data = Json::decode($data);
    $cid = (int) $data['cid'];
    $quantity = (int) $data['quantity'];

    /** @var \Drupal\site_commerce_cart\CartInterface $cart */
    $cart = \Drupal::entityTypeManager()->getStorage('site_commerce_cart')->load($cid);
    $user_has_access = FALSE;
    if ($cart instanceof CartInterface) {
      $user_has_access = \Drupal::service('kvantstudio.validator')->isUserHasAccessOwnerEntity($cart);
    }

    if ($user_has_access) {
      // Текущее количество в корзине.
      $cart_quantity = (int) $cart->getQuantity();

      // Параметры товара.
      $entity_type_id = $cart->getCartItemEntityTypeId();
      $entity_id = $cart->getCartItemEntityId();

      $stock_value_new = 0;
      $operation = '';

      // Обновляем остатки на складе.
      /** @var \Drupal\site_commerce_product\ProductInterface $entity */
      $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
      if ($entity instanceof ProductInterface && $entity->getStockAllow()) {
        // Текущее количество на складе.
        $stock_value = (int) $entity->getStockValue();

        // Если количество увеличивается.
        if ($quantity > $cart_quantity) {
          // Количество товара, хотим взять со склада.
          $delta = $quantity - $cart_quantity;
          if ($delta > $stock_value) {
            $delta = $stock_value;
          }

          if (!$stock_value) {
            $delta = 0;
          }

          // Не можем увеличить больше чем доступно на складе.
          $quantity = $delta + $cart_quantity;

          // Уменьшаем количество товара на складе.
          $stock_value_new = $stock_value - $delta;

          $operation = 'plus';
        }

        // Если количество уменьшается.
        if ($quantity < $cart_quantity) {
          // Количество товара, хотим вернуть на склад.
          $delta = $cart_quantity - $quantity;

          // Увеличиваем количество товара на складе.
          $stock_value_new = $stock_value + $delta;

          $operation = 'minus';
        }

        // Если количество не изменяется.
        if ($quantity == $cart_quantity) {
          $stock_value_new = $stock_value;
        }

        // Сохраняем количество товара на складе.
        $entity->setStockValue($stock_value_new);
        $entity->save();
      }

      // Если разрешено учитывать количество на складе.
      // Запрещаем добавлять в корзину больше чем уже добавлено.
      if ($entity->getStockAllow() && !$stock_value && $stock_value_new == $stock_value && $operation == 'plus') {
        $response->addCommand(new AlertCommand('Превышено максимальное количество доступное для заказа.'));
      }

      // Обновляем количество в корзине.
      $cart->setQuantity($quantity);
      $cart->save();


      # Устанавливаем количество остаток на складе.
      $selector = "#cart__item-quantity-stock-value-$cid";
      $method = 'html';
      $arguments = [$stock_value_new];
      $response->addCommand(new InvokeCommand($selector, $method, $arguments));

      # Устанавливаем количество в корзине.
      $selector = "#cart__item-quantity-value-$cid";
      $method = 'attr';
      $arguments = ['value', $quantity];
      $response->addCommand(new InvokeCommand($selector, $method, $arguments));

      $method = 'val';
      $arguments = [$quantity];
      $response->addCommand(new InvokeCommand($selector, $method, $arguments));
    }

    // Обновляем информацию о стоимости заказа.
    $elements = [
      '#theme' => 'site_commerce_cart_order_information',
    ];
    $response->addCommand(new HtmlCommand('.cart__order-information', $elements));

    return $response;
  }

  /**
   * Удаляет позицию в корзине.
   *
   * @param [type] $method
   * @param [type] $cid
   * @param [type] $quantity
   * @return void
   */
  public function cartItemDelete(int $cart_id, string $method) {
    if ($method == 'ajax') {
      // Create AJAX Response object.
      $response = new AjaxResponse();

      $cart = \Drupal::entityTypeManager()->getStorage('site_commerce_cart')->load($cart_id);
      $value = 0;
      $stock_value = 0;
      $entity = NULL;
      if ($cart instanceof CartInterface) {
        // Текущее количество в корзине.
        $cart_quantity = (int) $cart->getQuantity();
        $entity_type_id = $cart->getCartItemEntityTypeId();
        $entity_id = $cart->getCartItemEntityId();

        // Обновляем остатки на складе.
        $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
        if ($entity instanceof ProductInterface && $entity->getStockAllow()) {
          // Текущее и новое количество на складе.
          $stock_value = (int) $entity->getStockValue();
          $value = $stock_value + $cart_quantity;
        }

        // Удаляем позицию из корзины.
        $cart->delete();

        // Обновляем количество товара на складе.
        if ($entity instanceof ProductInterface && $value > $stock_value) {
          $entity->setStockValue($value);
          $entity->save();
        }

        $response->addCommand(new RemoveCommand('#cart__item-' . $cart_id));
      }

      /** @var \Drupal\site_commerce_cart\CartStorageInterface $storage */
      $storage = $this->entityTypeManager()->getStorage('site_commerce_cart');
      if ($storage->hasPositionsInCart()) {
        // Пересчитываем информацию о заказе.
        $elements = [
          '#theme' => 'site_commerce_cart_order_information',
        ];
        $response->addCommand(new HtmlCommand('.cart__order-information', $elements));
      } else {
        $elements = [
          '#theme' => 'site_commerce_cart_empty',
        ];
        $response->addCommand(new HtmlCommand('#cart-wrapper', $elements));
      }

      // Return ajax response.
      return $response;
    }
  }
}
