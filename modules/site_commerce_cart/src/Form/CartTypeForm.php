<?php

namespace Drupal\site_commerce_cart\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the Cart type entities add and edit forms.
 */
class CartTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t("Short name of the cart type."),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\site_commerce_cart\Entity\CartType::load',
      ],
      '#disabled' => !$entity->isNew(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();
    $entity = $this->entity;
    $status = $entity->save();
    $message_params = [
      '%label' => $entity->label(),
      '%content_entity_id' => $entity->getEntityType()->getBundleOf(),
    ];

    // Provide a message for the user and redirect them back to the collection.
    switch ($status) {
      case SAVED_NEW:
        $messenger->addMessage($this->t('Created the %label cart type.', $message_params));
        break;

      default:
        $messenger->addMessage($this->t('Saved the %label cart type.', $message_params));
    }

    $form_state->setRedirectUrl($entity->toUrl('collection'));
  }
}
