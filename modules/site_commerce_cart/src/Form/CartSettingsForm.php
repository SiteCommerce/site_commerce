<?php

namespace Drupal\site_commerce_cart\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CartSettingsForm.
 */
class CartSettingsForm extends ConfigFormBase {

  /**
   * Config name.
   *
   * @var string
   */
  const SETTINGS = 'site_commerce_cart.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_commerce_cart_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config(static::SETTINGS);

    // Примечание для покупателей на странице корзины.
    $form['note_buyers_cart_page'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Note to buyers on the cart page'),
      '#default_value' => $config->get('note_buyers_cart_page'),
    ];

    // Количество секунд на которое резервируются товары в корзине.
    $form['reservation_time'] = [
      '#type' => 'number',
      '#title' => $this->t('The number of seconds for which products in the cart are reserved'),
      '#default_value' => $config->get('reservation_time'),
      '#min' => 0,
      '#size' => 10,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->configFactory->getEditable(static::SETTINGS);

    // Примечание для покупателей на странице корзины.
    $note_buyers_cart_page = trim($form_state->getValue('note_buyers_cart_page'));
    $config->set('note_buyers_cart_page', $note_buyers_cart_page);

    // Количество секунд на которое резервируются товары в корзине.
    $reservation_time = (int) $form_state->getValue('reservation_time');
    $config->set('reservation_time', $reservation_time);

    $config->save();
  }

}
