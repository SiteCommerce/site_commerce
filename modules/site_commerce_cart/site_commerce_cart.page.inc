<?php

/**
 * @file
 * Contains site_commerce_cart.page.inc.
 *
 * Page callback for Cart entities.
 */

use Drupal\kvantstudio\Formatter;

/**
 * Implements template_preprocess_site_commerce_cart_page().
 */
function template_preprocess_site_commerce_cart_page(&$variables) {
    // Шаблон блока с товарами в корзине.
    $elements = [
      '#theme' => 'site_commerce_cart_details',
    ];
    $variables['cart_details'] = $elements;
}

/**
 * Implements template_preprocess_site_commerce_cart_details().
 */
function template_preprocess_site_commerce_cart_details(&$variables) {
  /** @var \Drupal\site_commerce_cart\CartStorageInterface $storage_cart */
  $storage_cart = \Drupal::entityTypeManager()->getStorage('site_commerce_cart');

  $variables['positions_in_cart'] = $storage_cart->hasPositionsInCart();
  if ($variables['positions_in_cart']) {
    // Загружает массив с данными корзины.
    $variables['products'] = $storage_cart->loadCart();

    // Шаблон блока с информацией о стоимости заказа в корзине.
    $elements = [
      '#theme' => 'site_commerce_cart_order_information',
    ];
    $variables['order_information'] = $elements;
  } else {
    $elements = [
      '#theme' => 'site_commerce_cart_empty',
    ];
    $variables['cart_empty'] = $elements;
  }
}

/**
 * Implements template_preprocess_site_commerce_cart_order_information().
 */
function template_preprocess_site_commerce_cart_order_information(&$variables) {
  /** @var \Drupal\site_commerce_order\OrderStorageInterface $storage_order */
  $storage_order = \Drupal::entityTypeManager()->getStorage('site_commerce_order');

  // Loading the configuration.
  $config = \Drupal::config('site_commerce_cart.settings');
  $variables['note_buyers_cart_page'] = $config->get('note_buyers_cart_page');

  // Loading the configuration.
  $config = \Drupal::config('site_commerce_order.settings');

  // Определяем стоимость и валюту.
  $value = $storage_order->getPriceByCart();
  $variables['order_cost'] = Formatter::price($value);
  $variables['default_currency_code'] = $config->get('default_currency_code') ? $config->get('default_currency_code') : "RUB";

  // Define the currency symbol.
  $currency_entities = \Drupal::entityTypeManager()->getStorage('site_commerce_currency')->loadByProperties([
    'letter_code' => $variables['default_currency_code']
  ]);
  /** @var \Drupal\site_commerce_price\Entity\Currency $currency */
  $currency = reset($currency_entities);
  $variables['symbol'] = $currency->getSymbol();
}
