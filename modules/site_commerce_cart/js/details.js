/**
 * @file
 * Misc JQuery scripts in this file
 */
(function ($, Drupal, drupalSettings) {
  "use strict";

  /**
     * Регистрирует изменение кол-ва товаров в корзине при ручном вводе значения в поле ввода.
     */
  $(".cart__item-quantity-value").on("change", function () {
    var cid = parseInt($(this).attr("cid"));
    var min = parseInt($(this).attr("min"));
    var quantity = parseInt($(this).val().trim());
    if (isNaN(quantity) || quantity < min) {
      quantity = min;
      $("#cart__item-quantity-value-" + cid).val(quantity);
    }

    if (cid && quantity) {
      updateQuantity(cid, quantity);
    }
  });

  /**
     * Регистрирует изменение кол-ва товаров в корзине при нажатии на кнопку изменения количества.
     */
  $(".cart__item-quantity-actions-button").on("click", function () {
    var cid = parseInt($(this).attr("cid"));
    var min = parseInt($("#cart__item-quantity-value-" + cid).attr("min"));
    var quantity = parseInt($("#cart__item-quantity-value-" + cid).val().trim());
    if (isNaN(quantity) || quantity < min) {
      quantity = min;
      $("#cart__item-quantity-value-" + cid).val(quantity);
    }

    if (cid && quantity) {
      updateQuantity(cid, quantity);
    }
  });

	/**
	 * Update cart by ajax request.
	 * @param {number} cid
	 * @param {number} quantity
	 */
  function updateQuantity(cid, quantity) {
    // Устанавливаем прелоадер.
    if (typeof drupalSettings.ajaxLoader != "undefined") {
      let element = '<div class="site-orders-cart-ajax-preloader">' + drupalSettings.ajaxLoader.markup + "</div>";
      $(".order-information__preloader").html(element);
    }

		// Данные передаваемые в запросе.
		var data = {};
		data.cid = parseInt(cid);
		data.quantity = parseInt(quantity);

		// Формирует ajax запрос.
		var ajaxObject = Drupal.ajax({
			url: '/cart-item-update-quantity/nojs',
			submit: {
				data: JSON.stringify(data)
			}
		});

    ajaxObject.execute()
		.done(function (response) {
		})
		.fail(function (response) {
    });
  }

  // Удаляет позицию из корзины.
  $(".cart__item-delete").on("click", function () {
    var cid = parseInt($(this).attr("cid"));
    if (cid) {
      // Устанавливаем прелоадер.
      if (typeof drupalSettings.ajaxLoader != "undefined") {
        let element = '<div class="site-orders-cart-ajax-preloader">' + drupalSettings.ajaxLoader.markup + "</div>";
        $("#cart__item-actions-" + cid).html(element);
      }

      // Выполняет запрос ajax.
      var ajaxObject = Drupal.ajax({
        url: "/cart-item-delete/" + cid + "/nojs",
        base: false,
        element: false,
        progress: false
      });
      ajaxObject.execute();
    }
  });
})(jQuery, Drupal, drupalSettings);
