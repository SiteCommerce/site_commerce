# **Интернет-магазин для Drupal 10.2**

## Порядок установки ядра

**Выполнить установку Drupal через Drupal Composer Project Template**

Открыть в терминале папку с проектом (например, папка вашего проекта my-project)

```
cd my-project
```
Установить Drupal

```
composer create-project drupal/recommended-project .
```

**Удалить модуль drupal/core-project-message**

```
composer remove drupal/core-project-message
```

## Вносим изменения в файл composer.json в корне проекта в папке my-project

**В composer.json в разделе extra удалить блок drupal-core-project-message**

```
"drupal-core-project-message": {
....
}
```

**В composer.json добавить в раздел require после "drupal/core-recommended" дополнительные модули**

```
"require": {

    ......

    "drush/drush": "^12",
    "kvantstudio/exchange-rates": "^1.0",
    "kvantstudio/site_account": "^4.0",
    "kvantstudio/site_commerce": "^3.0@dev",
    "kvantstudio/site_media_gallery": "^2.0",
    "kvantstudio/site_payments": "^3.0",
    "kvantstudio/site_payments_sberbank": "^2.0",
    "kvantstudio/site_send_message": "^3.0",
    "zodiacmedia/drupal-libraries-installer": "^1.4"
}
```
Модуль zodiacmedia/drupal-libraries-installer - поможет установить сторонние js библиотеки в папку libraries.

**В раздел extra в конце добавить дополнительный раздел drupal-libraries**

```
"extra": {
    "drupal-libraries": {
      "glightbox": "https://github.com/biati-digital/glightbox/archive/refs/tags/3.1.0.zip",
      "flickity": "https://github.com/metafizzy/flickity/archive/refs/tags/v2.3.0.zip",
      "tooltipster": "https://github.com/calebjacob/tooltipster/archive/4.2.8.zip",
      "line-awesome": "https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/line-awesome-1.3.0.zip"
    },
}
```

Набор этих библиотек приведен в качестве примера и в реальных проектах может быть заменен на ваши бибилиотеки. В настоящее время обязательной является только flickity (используется для отображения слайдера фотографий в карточке товара).

**Выполнить команду обновления**

```
composer update --with-dependencies -o
```

**Выполнить установку Drupal через браузер**

Запустите установку в браузере из стандартного профиля.

**Выполнить команду установки модуля управления товарами**

```
drush en site_commerce_product
```
