<?php

namespace Drupal\site_commerce\Plugin\ajax_loader;

use Drupal\ajax_loader\ThrobberPluginBase;
use Drupal\Core\Extension\ModuleExtensionList;

/**
 * Class ThrobberPulse.
 *
 * @Throbber(
 *   id = "site_commerce_throbber_solid_snake",
 *   label = @Translation("Solid snake")
 * )
 */
class ThrobberSolidSnake extends ThrobberPluginBase {

  /**
   * ThrobberSolidSnake constructor.
   *
   * @param array $configuration
   *   Array with configuration.
   * @param string $plugin_id
   *   String with plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition value.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extensionList
   *   Provides a list of available modules.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleExtensionList $extensionList) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $extensionList);
    $this->path = '/' . $extensionList->getPath('site_commerce');
  }

  /**
   * @inheritdoc
   */
  protected function setMarkup() {
    return '<img class="site-commerce-throbber-solid-snake" src="/modules/contrib/site_commerce/images/throbber-solid-snake.svg"/>';
  }

  /**
   * @inheritdoc
   */
  protected function setCssFile() {
    return $this->path . '/css/throbber-solid-snake.css';
  }

}
